//! Manages a `Scrollable`'s pages, currently hovered item and more.

use std::cmp::Ordering;

use crate::{container::Container, window::Window};

use super::ScrollableInfo;

/// Holds the state of the pages and indexes in a `Scrollable`.
pub struct ScrollablePager {
	/// The index of the currently hovered item, if any.
	current_item_index: Option<usize>,

	/// The index of the last item at the last page, if any.
	last_item_index: Option<usize>,

	/// The index of the current page.
	current_page_index: usize,

	/// The index of the last page.
	last_page_index: usize,

	/// The amount of items which can be shown in a single page.
	items_per_page: usize,

	/// The indexes of selected items, in the order they were selected.
	selected_item_indexes: Vec<usize>,
}

impl ScrollablePager {
	pub fn new(window: &Window) -> Self {
		// The amount of rows which can be used to display items.
		// I leave 3 rows at the top of the `Window` (title + two empty lines
		// below) and 3 rows at the bottom (bottom text + 2 empty lines above
		// it).
		let available_height = window.height() - 3 - 3;
		let items_per_page = (available_height as f32 / 2.0).round() as usize;

		Self {
			current_item_index: None,
			last_item_index: None,

			current_page_index: 0,
			last_page_index: 0,

			items_per_page,

			selected_item_indexes: Vec::new(),
		}
	}

	fn has_items(&self) -> bool {
		self.current_item_index.is_some()
	}

	/// Returns the index of the currently hovered item, if any.
	pub fn hovered_index(&self) -> Option<usize> {
		self.current_item_index
	}

	pub fn set_hovered_index(&mut self, index: usize) {
		assert!(self.has_items() && index <= self.last_item_index.unwrap());

		self.current_item_index.replace(index);
		self.set_page(index / self.items_per_page);
	}

	pub fn change_items(&mut self, items_len: usize) {
		if items_len == 0 {
			self.current_item_index = None;
			self.last_item_index = None;
			self.current_page_index = 0;
			self.last_page_index = 0;
		} else {
			self.current_item_index.replace(0);
			self.last_item_index.replace(items_len - 1);
			self.last_page_index = self.last_item_index.unwrap() / self.items_per_page;
			self.set_page(0);
		}
	}

	pub fn add_item(&mut self) {
		if !self.has_items() {
			self.last_item_index = Some(0);
			self.current_item_index = Some(0);

			return;
		}

		*self.last_item_index.as_mut().unwrap() += 1;

		// Add a page if necessary.
		if (self.last_page_index + 1) * self.items_per_page == self.last_item_index.unwrap() {
			self.last_page_index += 1;
		}
	}

	pub fn current_page(&self) -> usize {
		self.current_page_index
	}

	pub fn set_page(&mut self, page_index: usize) {
		self.current_page_index = page_index;
	}

	pub fn has_more_than_one_page(&self) -> bool {
		self.last_page_index > 0
	}

	pub fn page_counter(&self) -> String {
		format!(
			"{}/{}",
			self.current_page_index + 1,
			self.last_page_index + 1
		)
	}

	pub fn go_to_next_item(&mut self) {
		if !self.has_items() {
			return;
		}

		if self.current_item_index == self.last_item_index {
			// If I'm at the end of the last page, go to the beginning.
			self.current_item_index.replace(0);
			self.set_page(0);
		} else if self.current_item_index.unwrap() + 1
			== (self.current_page_index + 1) * self.items_per_page
		{
			// If I'm at the end of any other page, go to the beginning of the
			// next one.
			*self.current_item_index.as_mut().unwrap() += 1;
			self.set_page(self.current_page_index + 1);
		} else {
			// Otherwise, go to the next item in the current page.
			*self.current_item_index.as_mut().unwrap() += 1;
		}
	}

	pub fn go_to_previous_item(&mut self) {
		if !self.has_items() {
			return;
		}

		if self.current_item_index.unwrap() == 0 {
			// If I'm at the beginning of the first page, go to the end of the
			// last one.
			self.current_item_index = self.last_item_index;
			self.set_page(self.last_page_index);
		} else if self.current_page_index > 0
			&& self.current_item_index.unwrap() == self.first_index_of_current_page()
		{
			// If I'm at the beginning of any other page, go to the end of the
			// previous one.
			*self.current_item_index.as_mut().unwrap() -= 1;
			self.set_page(self.current_page_index - 1);
		} else {
			// Otherwise, go to the previous item in the current page.
			*self.current_item_index.as_mut().unwrap() -= 1;
		}
	}

	pub fn go_to_next_page(&mut self) {
		if !self.has_more_than_one_page() {
			return;
		}

		match self.current_page_index.cmp(&self.last_page_index) {
			Ordering::Equal => {
				// If I'm at the last page, go to the first one.
				self.go_to_first_page();
				*self.current_item_index.as_mut().unwrap() -=
					self.last_page_index * self.items_per_page;
			}
			Ordering::Less => {
				// If I'm at any other page, go to the next one.
				self.set_page(self.current_page_index + 1);
				// That next page might have less items (ex: it's the last page
				// and it's not full).
				// If I can keep my "relative" index I'll do it, otherwise I'll
				// switch to the last available index.
				self.current_item_index.replace(usize::min(
					self.current_item_index.unwrap() + self.items_per_page,
					self.last_item_index.unwrap(),
				));
			}
			Ordering::Greater => unreachable!("Current page cannot be greater than last page"),
		}
	}

	pub fn go_to_previous_page(&mut self) {
		if !self.has_more_than_one_page() {
			return;
		}

		match self.current_page_index.cmp(&0) {
			Ordering::Equal => {
				// If I'm at the first page, go to the last one.
				self.go_to_last_page();
			}
			Ordering::Greater => {
				// If I'm at any other page, go to the previous one.
				self.set_page(self.current_page_index - 1);
				*self.current_item_index.as_mut().unwrap() -= self.items_per_page;
			}
			Ordering::Less => {
				unreachable!("self.current_page_index is usize and cannot be less than 0")
			}
		};
	}

	pub fn go_to_first_page(&mut self) {
		self.set_page(0);
	}

	pub fn go_to_last_page(&mut self) {
		if !self.has_more_than_one_page() {
			return;
		}

		// Example: if index points to the 3rd item in a page, this will be 2,
		// regardless of the
		// "absolute" value of index (that 3rd item could be at index 78).
		let current_index_relative_to_page =
			self.current_item_index.unwrap() - self.first_index_of_current_page();

		let last_page_first_index = self.last_page_index * self.items_per_page;

		// Try to go to the same "relative" index. If it isn't possible (because
		// the last page isn't full and that index does not exist in it), go to
		// the last available item instead.
		self.current_item_index.replace(usize::min(
			last_page_first_index + current_index_relative_to_page,
			self.last_item_index.unwrap(),
		));

		self.set_page(self.last_page_index);
	}

	pub fn delete_hovered(&mut self) {
		if !self.has_items() {
			panic!("Tried to delete the hovered item but there are no items.");
		}

		// If the deleted item was selected, deselect it.
		if self
			.selected_item_indexes
			.contains(&self.current_item_index.unwrap())
		{
			self.selected_item_indexes
				.retain(|&i| i != self.current_item_index.unwrap());
		}

		// The indexes of any selected items below the deleted item need to be
		// adjusted.
		self.selected_item_indexes
			.iter_mut()
			.filter(|index| **index > self.current_item_index.unwrap())
			.for_each(|index| *index -= 1);

		// If there is just one item, there won't be any items left.
		// We are already at the first page.
		if self.last_item_index.unwrap() == 0 {
			self.current_item_index = None;
			self.last_item_index = None;

			return;
		}

		// If the deleted item was the last one, adjust `self.current_item_index`.
		if self.current_item_index == self.last_item_index {
			*self.current_item_index.as_mut().unwrap() -= 1;
		}

		// There is one less item.
		*self.last_item_index.as_mut().unwrap() -= 1;

		// If the last page would be empty, delete it.
		if self.last_page_index * self.items_per_page > self.last_item_index.unwrap() {
			self.last_page_index -= 1;
		}

		// If I was at the last page and it no longer exists, go to the
		// previous page.
		if self.current_page_index > self.last_page_index {
			self.set_page(self.last_page_index);
		}
	}

	pub fn first_index_of_current_page(&self) -> usize {
		self.current_page_index * self.items_per_page
	}

	/// Returns the indexes of the selected items, in the order they were selected.
	pub fn selected_indexes(&self) -> &Vec<usize> {
		&self.selected_item_indexes
	}

	/// Select the currently hovered item.
	pub fn select_hovered_item(&mut self) {
		let Some(hovered_index) = self.hovered_index() else {
			return;
		};

		if self.selected_item_indexes.contains(&hovered_index) {
			self.selected_item_indexes.retain(|&x| x != hovered_index);
		} else {
			self.selected_item_indexes.push(hovered_index);
		}
	}

	pub fn set_selected_indexes(&mut self, selected_indexes: Vec<usize>) {
		self.selected_item_indexes = selected_indexes;
	}

	pub fn info(&self) -> ScrollableInfo {
		let item_count = match self.last_item_index {
			None => 0,
			Some(index) => index + 1,
		};

		ScrollableInfo {
			item_count,
			current_item_index: self.current_item_index,
			page_count: self.last_page_index + 1,
			current_page_index: self.current_page_index,
			items_per_page: self.items_per_page,
			selected_item_count: self.selected_item_indexes.len(),
		}
	}
}
