/// Holds display information about a `Scrollable`.
#[derive(Debug)]
pub struct ScrollableInfo {
	/// The amount of items.
	pub item_count: usize,

	/// The index of the currently hovered item, if any.
	pub current_item_index: Option<usize>,

	/// The amount of pages.
	pub page_count: usize,

	/// The index of the currently displayed page.
	pub current_page_index: usize,

	/// The maximum amount of items which can be displayed in every page.
	pub items_per_page: usize,

	/// The amount of selected items.
	pub selected_item_count: usize,
}
