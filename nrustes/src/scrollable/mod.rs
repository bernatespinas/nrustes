use std::collections::HashMap;
use std::fmt::Display;

use direction_simple::Direction;
use rgb::RGB8;

use crate::{
	container::Container, context::Context, darken::Darken, handle_input::HandleInput, key::Key,
	prelude::Draw, print::Print, window::Window,
};

mod scrollable_builder;
pub use scrollable_builder::ScrollableBuilder;

mod prefix;
use prefix::Prefix;

mod scrollable_pager;
use scrollable_pager::ScrollablePager;

mod scrollable_info;
pub use scrollable_info::ScrollableInfo;

#[cfg(test)]
pub mod tests;

#[derive(Draw)]
pub struct Scrollable<Item: Display> {
	/// The items to list.
	items: Vec<Item>,

	/// The inner `Window` where things are printed.
	#[draw]
	window: Window,

	pager: ScrollablePager,

	/// What to show before each item.
	prefix: Prefix,

	/// Maps indexes to the coordinates of their `DisplayItem`s in `self.win`.
	position_to_display_item_coords: HashMap<usize, (u16, u16)>,
}

impl<Item: Display> Scrollable<Item> {
	pub fn has_items(&self) -> bool {
		!self.items.is_empty()
	}

	pub fn change_items(&mut self, items: Vec<Item>) {
		self.items = items;

		self.pager.change_items(self.items.len());

		self.schedule_draw();
	}

	pub fn add_item(&mut self, item: Item) {
		self.items.push(item);
		self.pager.add_item();
	}

	/// Returns the currently selected item, if any.
	pub fn hovered(&self) -> Option<&Item> {
		match self.pager.hovered_index() {
			None => None,
			Some(index) => self.items.get(index),
		}
	}

	/// Returns the index of the currently selected item, if any.
	pub fn hovered_index(&self) -> Option<usize> {
		self.pager.hovered_index()
	}

	pub fn set_hovered_index(&mut self, index: usize) {
		self.pager.set_hovered_index(index);
	}

	/// Select the currently hovered item.
	pub fn select_hovered(&mut self) {
		self.pager.select_hovered_item();
	}

	/// Returns the indexes of the selected items, in the order they were selected.
	pub fn selected_indexes(&self) -> &[usize] {
		self.pager.selected_indexes()
	}

	/// Returns the amount of selected items.
	pub fn amount_selected(&self) -> usize {
		self.selected_indexes().len()
	}

	/// Returns `true` if the item at index `i` is selected; `false` otherwise.
	pub fn is_selected(&self, i: usize) -> bool {
		self.selected_indexes().contains(&i)
	}

	pub fn set_selected_indexes(&mut self, selected_indexes: Vec<usize>) {
		self.pager.set_selected_indexes(selected_indexes);
	}

	/// Deletes the currently hovered item and returns it; returns `None`
	/// otherwise.
	pub fn delete_hovered(&mut self) -> Option<Item> {
		let hovered_index = self.pager.hovered_index()?;

		let deleted = self.items.remove(hovered_index);
		self.pager.delete_hovered();

		self.schedule_draw();

		Some(deleted)
	}

	fn change_hovered_display_item_color(&mut self, color: RGB8) {
		if !self.has_items() {
			return;
		}

		let coords = self
			.position_to_display_item_coords
			.get(&self.hovered_index().unwrap())
			.unwrap();

		let hovered_display_item = self.window.display_item_mut(coords).unwrap();

		hovered_display_item.set_fg(color);
	}

	fn highlight_hovered_item(&mut self) {
		self.change_hovered_display_item_color(self.window.accent_color());
	}

	fn unhighlight_hovered_item(&mut self) {
		self.change_hovered_display_item_color(self.window.primary_color());
	}

	fn print_content_with_color(&mut self, color: RGB8) {
		// Reset the mapping between positions and their `DisplayItem`'s coordinates.
		self.position_to_display_item_coords.clear();

		if !self.has_items() {
			self.window.print_fg((3, 1), "Empty.", color);

			return;
		}

		// TODO Move into draw_decorations? Like, call Window's draw_decoration and then add this.
		if self.pager.has_more_than_one_page() {
			let info = self.pager.page_counter();
			self.window.header.change_additional_info(info);
		} else {
			self.window.header.change_additional_info("");
		}

		let mut y: u16 = 3;
		let mut i: usize = self.pager.first_index_of_current_page();

		while i < self.items.len() && y + 2 <= self.window.height() - 2 {
			let item = self.format_item_to_print(i);

			self.window.print_fg((y, 1), item, color);

			self.position_to_display_item_coords.insert(i, (y, 1));

			i += 1;
			y += 2;
		}
	}

	/// Returns a `String` with the item at `index` and all the necessary prefixes.
	fn format_item_to_print(&self, index: usize) -> String {
		let selected_identifier = if self.selected_indexes().contains(&index) {
			">> "
		} else {
			""
		};

		let item = self.items.get(index).unwrap();

		match self.prefix {
			Prefix::Number(separator) => {
				format!("{selected_identifier}{} {separator} {item}", index + 1)
			}
			Prefix::Char(ch) => format!("{selected_identifier}{ch} {item}"),
			Prefix::Nothing => format!("{selected_identifier}{item}"),
		}
	}

	/// Renders just the previously and currently selected items when possible to avoid having to redraw everything.
	fn render_previous_and_current_items_if_necessary(
		&mut self,
		previous_index: Option<usize>,
		context: &mut impl Context,
	) {
		if context.needs_to_redraw_buffer() {
			return;
		}

		let Some(previous_index) = previous_index else {
			return;
		};

		let mut render_display_item = |index: usize| {
			let coords = *self.position_to_display_item_coords.get(&index).unwrap();

			let display_item = self.window.display_item(coords).unwrap();

			context.render_text_container(
				self,
				coords.0,
				coords.1,
				&display_item.text,
				display_item.fg,
				display_item.bg,
			);
		};

		render_display_item(previous_index);
		render_display_item(self.hovered_index().unwrap());
	}

	pub fn info(&self) -> ScrollableInfo {
		self.pager.info()
	}
}

impl<T: Display> Container for Scrollable<T> {
	fn window(&self) -> &Window {
		&self.window
	}

	fn window_mut(&mut self) -> &mut Window {
		&mut self.window
	}

	fn update(&mut self) {
		self.window.delete_content();
		self.print_content_with_color(self.window.primary_color());
		self.highlight_hovered_item();
	}
}

impl<Item: Display> HandleInput for Scrollable<Item> {
	fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		let key = context.input_key().gamify();

		self.unhighlight_hovered_item();

		let page_before_handling_key = self.pager.current_page();
		let index_before_handling_key = self.hovered_index();

		match key {
			Key::Movement(Direction::Up) => self.pager.go_to_previous_item(),

			Key::Movement(Direction::Down) => self.pager.go_to_next_item(),

			Key::Movement(Direction::Right) => self.pager.go_to_next_page(),

			Key::Movement(Direction::Left) => self.pager.go_to_previous_page(),

			_ => {}
		};

		if self.pager.current_page() != page_before_handling_key || !matches!(key, Key::Movement(_))
		{
			self.schedule_draw();
		} else if matches!(key, Key::Movement(_)) {
			self.highlight_hovered_item();

			self.render_previous_and_current_items_if_necessary(index_before_handling_key, context);
		}

		Ok(key)
	}
}
