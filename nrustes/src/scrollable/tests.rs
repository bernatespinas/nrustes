use crate::{
	backends::VoidContext, container::Container, nrustes_config::NrustesConfig, prelude::Print,
	window::WindowBuilder,
};

use super::{Scrollable, ScrollableBuilder};

#[test]
fn enumerated_scrollable_uses_correct_format() {
	let (_context, scrollable_builder) = initialize(150, 200);

	let mut scrollable: Scrollable<String> = scrollable_builder.enumerated().build();

	scrollable.update();

	for display_item in scrollable.window.display_items().values() {
		let index: u16 = display_item
			.text
			.split(' ')
			.last()
			.unwrap()
			.parse()
			.unwrap();

		assert_eq!(display_item.text, format!("{} - Item {index}", index + 1));
	}
}

#[test]
fn scrollable_with_no_prefix_uses_correct_format() {
	let (_context, scrollable_builder) = initialize(150, 200);

	let mut scrollable: Scrollable<String> = scrollable_builder.with_no_prefix().build();

	scrollable.update();

	for display_item in scrollable.window.display_items().values() {
		let index: u16 = display_item
			.text
			.split(' ')
			.last()
			.unwrap()
			.parse()
			.unwrap();

		assert_eq!(display_item.text, format!("Item {index}"));
	}
}

#[test]
fn scrollable_with_char_prefix_uses_correct_format() {
	let (_context, scrollable_builder) = initialize(150, 200);

	let mut scrollable: Scrollable<String> = scrollable_builder.with_prefix('@').build();

	scrollable.update();

	for display_item in scrollable.window.display_items().values() {
		let index: u16 = display_item
			.text
			.split(' ')
			.last()
			.unwrap()
			.parse()
			.unwrap();

		assert_eq!(display_item.text, format!("@ Item {index}"));
	}

	scrollable.pager.go_to_next_page();
	scrollable.update();

	for display_item in scrollable.window.display_items().values() {
		let index: u16 = display_item
			.text
			.split(' ')
			.last()
			.unwrap()
			.parse()
			.unwrap();

		assert_eq!(display_item.text, format!("@ Item {index}"));
	}
}

#[test]
fn only_hovered_item_is_highlighted() {
	let (_context, scrollable_builder) = initialize(30, 10);

	let mut scrollable: Scrollable<String> = scrollable_builder.enumerated().build();

	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 0);
}

#[test]
fn scrolling_updates_highlighted_item() {
	let (_context, scrollable_builder) = initialize(30, 10);

	let mut scrollable: Scrollable<String> = scrollable_builder.enumerated().build();

	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 0);

	scrollable.pager.go_to_next_item();
	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 1);

	scrollable.pager.go_to_next_page();
	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 11);

	scrollable.pager.go_to_previous_item();
	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 10);

	scrollable.pager.go_to_previous_page();
	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 0);

	scrollable.pager.go_to_previous_page();
	scrollable.update();

	assert_only_hovered_item_is_highlighted(&scrollable, 20);
}

fn initialize(
	item_count: usize,
	items_per_page: usize,
) -> (VoidContext, ScrollableBuilder<String>) {
	let nrustes_config = NrustesConfig::default();
	let context = VoidContext::new(160, 200, nrustes_config).mock_drawing_is_slow();

	let items = (0..item_count).map(|i| format!("Item {i}")).collect();
	let height = 3 + items_per_page as u16 * 2 + 2;

	let scrollable_builder: ScrollableBuilder<String> =
		WindowBuilder::new(0, 0, height, 60, &context)
			.with_title("Scrollable")
			.scrollable_builder()
			.with_items(items);

	(context, scrollable_builder)
}

fn assert_only_hovered_item_is_highlighted(
	scrollable: &Scrollable<String>,
	expected_hovered_item_index: usize,
) {
	let hovered_index = scrollable.hovered_index().unwrap();

	assert_eq!(hovered_index, expected_hovered_item_index, "Expected hovered item index to be {expected_hovered_item_index}, but got {hovered_index} instead.");

	for (index, coords) in &scrollable.position_to_display_item_coords {
		let expected_fg = if *index == hovered_index {
			scrollable.window.accent_color()
		} else {
			scrollable.window.primary_color()
		};

		let display_item = scrollable.window.display_item(*coords).unwrap();

		assert_eq!(display_item.fg, expected_fg);
	}
}
