use std::collections::HashMap;
use std::fmt::Display;

use crate::container::{BuildContainer, Container};
use crate::scrollable::{Prefix, Scrollable};
use crate::shortcuts::Shortcuts;
use crate::window::Window;

use super::scrollable_pager::ScrollablePager;

pub struct ScrollableBuilder<Item: Display> {
	scrollable: Scrollable<Item>,
}

impl<Item: Display> ScrollableBuilder<Item> {
	pub fn new(window: Window) -> Self {
		let pager = ScrollablePager::new(&window);

		let scrollable = Scrollable {
			items: Vec::default(),

			window,

			pager,

			prefix: Prefix::Number('-'),

			position_to_display_item_coords: HashMap::default(),
		};

		ScrollableBuilder { scrollable }
	}

	/// Items will have their position displayed in front of them.
	pub fn enumerated(mut self) -> Self {
		self.scrollable.prefix = Prefix::Number('-');

		self
	}

	/// Items will not have anything displayed in front of them.
	pub fn with_no_prefix(mut self) -> Self {
		self.scrollable.prefix = Prefix::Nothing;

		self
	}

	/// Items will have a specific character displayed in front of them.
	pub fn with_prefix(mut self, prefix: char) -> Self {
		self.scrollable.prefix = Prefix::Char(prefix);

		self
	}

	pub fn with_items(mut self, items: Vec<Item>) -> Self {
		self.scrollable.change_items(items);

		self
	}

	pub fn at_item(mut self, item_index: usize) -> Self {
		debug_assert!(item_index + 1 < self.scrollable.items.len());

		self.scrollable.set_hovered_index(item_index);

		self
	}

	pub fn build<T>(self) -> T
	where
		T: Container,
		ScrollableBuilder<Item>: BuildContainer<T>,
	{
		self.build_container()
	}
}

impl<Item: Display> BuildContainer<Scrollable<Item>> for ScrollableBuilder<Item> {
	fn build_container(self) -> Scrollable<Item> {
		self.scrollable
	}
}

impl<Item: Display, Data: Clone> BuildContainer<Shortcuts<Scrollable<Item>, Data>>
	for ScrollableBuilder<Item>
{
	fn build_container(self) -> Shortcuts<Scrollable<Item>, Data> {
		self.scrollable.into()
	}
}
