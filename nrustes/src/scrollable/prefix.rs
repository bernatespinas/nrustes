/// What to show before each item in a `Scrollable`.
#[derive(Debug)]
pub enum Prefix {
	/// Show the item's positions, beginning at 1.
	Number(char),

	/// Show a specific character.
	Char(char),

	/// Show nothing.
	Nothing,
}
