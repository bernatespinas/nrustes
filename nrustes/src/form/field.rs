use super::field_type::FieldType;

#[derive(Clone)]
pub struct Field {
	/// The name which will be displayed to the user.
	pub name: String,

	/// The type of the value which this field accepts.
	pub field_type: FieldType,

	/// `true` if the field is optional and can be left empty; `false` otherwise.
	pub optional: bool,

	/// The current value of the field.
	pub value: String,
}
