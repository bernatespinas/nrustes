#[derive(Clone)]
pub enum FieldType {
	String,
	U16,
	I32,
	Choice(Vec<String>),
}
