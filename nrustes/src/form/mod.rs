use std::collections::HashMap;

use direction_simple::Direction;

use crate::{
	colors::LIGHT_GREY,
	container::Container,
	context::Context,
	darken::Darken,
	handle_input::HandleInput,
	prelude::{Draw, Key},
	print::Print,
	window::Window,
};

mod field_type;
pub use field_type::FieldType;

mod field;
use field::Field;

mod form_state;
use form_state::FormState;

#[derive(Draw)]
pub struct Form {
	/// The `Window` in which the form will be displayed.
	#[draw]
	window: Window,

	/// The currently hovered field.
	hovered_field_index: usize,

	form_state: FormState,
}

impl Container for Form {
	fn window(&self) -> &Window {
		&self.window
	}

	fn window_mut(&mut self) -> &mut Window {
		&mut self.window
	}

	fn update(&mut self) {
		self.window.delete_content();

		let mut y = 3;

		for (i, field) in self.form_state.iter().enumerate() {
			let fg = if i == self.hovered_field_index {
				self.window.accent_color()
			} else {
				self.window.primary_color()
			};

			self.window.print_fg((y, 2), &field.name, fg);

			y += 2;

			if field.value.is_empty() {
				let type_hint = match field.field_type {
					FieldType::String => "(Text)",
					FieldType::U16 => "(Positive natural number)",
					FieldType::I32 => "(Natural number)",
					FieldType::Choice(_) => "(Choice)",
				};

				let field_hint = if field.optional {
					format!("(Optional) {type_hint}")
				} else {
					type_hint.to_string()
				};

				self.window.print_fg((y, 6), field_hint, LIGHT_GREY);
			} else {
				self.window.print_fg((y, 6), &field.value, fg);
			}

			y += 2;
		}
	}
}

impl Form {
	pub fn new(window: Window) -> Self {
		Self {
			window,

			hovered_field_index: 0,

			form_state: FormState::default(),
		}
	}

	pub fn with_required_field(mut self, name: String, field_type: FieldType) -> Self {
		self.form_state.add_required_field(name, field_type);

		self
	}

	pub fn with_optional_field(mut self, name: String, field_type: FieldType) -> Self {
		self.form_state.add_optional_field(name, field_type);

		self
	}

	pub fn go_to_next_field_if_possible(&mut self) {
		if self.hovered_field_index + 1 < self.form_state.fields_amount() {
			self.hovered_field_index += 1;
		}
	}

	pub fn go_to_previous_field_if_possible(&mut self) {
		if self.hovered_field_index > 0 {
			self.hovered_field_index -= 1;
		}
	}

	pub fn hovered_field(&self) -> Option<&Field> {
		self.form_state.field(self.hovered_field_index)
	}

	pub fn set_hovered_field_value(&mut self, value: String) -> Result<(), &'static str> {
		match &self.hovered_field().unwrap().field_type {
			FieldType::String => {}
			FieldType::U16 => {
				if value.parse::<u16>().is_err() {
					return Err("Insert a u16 value.");
				}
			}
			FieldType::I32 => {
				if value.parse::<i32>().is_err() {
					return Err("Insert a i32 value.");
				}
			}
			FieldType::Choice(choices) => {
				if !choices.contains(&value) {
					return Err("Insert a valid choice.");
				}
			}
		};

		self.form_state
			.set_field_value(self.hovered_field_index, value);

		Ok(())
	}

	pub fn set_hovered_field_choice(&mut self, choice_index: usize) -> Result<(), &'static str> {
		match &self.hovered_field().unwrap().field_type {
			FieldType::Choice(choices) => match choices.get(choice_index) {
				Some(choice) => self.set_hovered_field_value(choice.clone()),
				None => Err("Insert a valid choice."),
			},
			_ => panic!("Tried to set the choice of a non-FieldType::Choice Field."),
		}
	}

	pub fn validate(&self) -> Result<(), String> {
		for field in self.form_state.iter() {
			if !field.optional && field.value.is_empty() {
				return Err("There are fields which are missing a value.".to_string());
			}
		}

		Ok(())
	}

	pub fn export(&self) -> Result<HashMap<String, String>, String> {
		self.validate()?;

		Ok(self
			.form_state
			.clone()
			.into_iter()
			.map(|field| (field.name, field.value))
			.collect())
	}

	pub fn clear_hovered_field(&mut self) {
		if self.hovered_field().is_some() {
			self.form_state.clear_field_value(self.hovered_field_index);
		}
	}
}

impl HandleInput for Form {
	fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		let key = context.input_key();

		match key {
			Key::Movement(Direction::Down) => {
				self.go_to_next_field_if_possible();

				self.schedule_draw();
			}
			Key::Movement(Direction::Up) => {
				self.go_to_previous_field_if_possible();

				self.schedule_draw();
			}
			Key::Backspace => {
				self.clear_hovered_field();

				self.schedule_draw();
			}
			_ => {}
		};

		self.update();

		Ok(key)
	}
}

impl From<Window> for Form {
	fn from(window: Window) -> Self {
		Form::new(window)
	}
}
