use super::{field::Field, field_type::FieldType};

#[derive(Default, Clone)]
pub struct FormState {
	fields: Vec<Field>,
}

impl FormState {
	pub fn add_required_field(&mut self, name: String, field_type: FieldType) {
		self.fields.push(Field {
			name,
			field_type,
			optional: false,
			value: String::new(),
		});
	}

	pub fn add_optional_field(&mut self, name: String, field_type: FieldType) {
		self.fields.push(Field {
			name,
			field_type,
			optional: true,
			value: String::new(),
		});
	}

	pub fn fields_amount(&self) -> usize {
		self.fields.len()
	}

	pub fn field(&self, index: usize) -> Option<&Field> {
		self.fields.get(index)
	}

	pub fn set_field_value(&mut self, index: usize, value: String) {
		self.fields.get_mut(index).unwrap().value = value;
	}

	pub fn clear_field_value(&mut self, index: usize) {
		self.fields.get_mut(index).unwrap().value.clear();
	}

	/// Returns an `Iterator` over the pairs of fields and their value.
	pub fn iter(&self) -> impl Iterator<Item = &Field> {
		self.fields.iter()
	}

	/// Returns an `Iterator` over the pairs of fields and their value.
	pub fn into_iter(self) -> impl Iterator<Item = Field> {
		self.fields.into_iter()
	}
}
