///! A popup is a small `Window` that is immediately displayed and quickly dismissed.
use std::time::Duration;

use crate::context::Context;
use crate::draw::Draw;

mod error_popup;
pub use error_popup::ErrorPopup;

mod progress_popup;
pub use progress_popup::ProgressPopup;

pub enum DismissMode {
	UserInput,
	AfterSeconds(u64),
}

pub trait Popup: Draw + Sized {
	fn dismiss_mode() -> DismissMode;

	fn dismiss(&mut self, context: &mut impl Context) {
		match Self::dismiss_mode() {
			DismissMode::UserInput => {
				context.input_key();
			}
			DismissMode::AfterSeconds(secs) => {
				std::thread::sleep(Duration::from_secs(secs));
			}
		};
	}
}
