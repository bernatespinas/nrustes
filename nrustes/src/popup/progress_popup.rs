use std::fmt::Display;

use crate::colors::{LIGHT_GREEN, LIGHT_RED};
use crate::container::Container;
use crate::context::Context;
use crate::darken::Darken;
use crate::prelude::Draw;
use crate::print::Print;
use crate::window::{Window, WindowBuilder};

use super::{DismissMode, Popup};

// enum ProgressStatus {
// 	Working,
// 	Done,
// 	Error,
// }

fn progress_popup_width(message: &str) -> u16 {
	(message.len() + " Error!".len() + 2 + 2) as u16
}

#[derive(Draw)]
pub struct ProgressPopup {
	#[draw]
	window: Window,
	done_or_error_x: u16,
	// status: ProgressStatus,
}

impl ProgressPopup {
	pub fn centered_in_screen(message: impl Display, context: &mut impl Context) -> ProgressPopup {
		let message = format!("{message}...");
		let width = progress_popup_width(&message);
		let window_builder = WindowBuilder::centered(5, width, context);

		ProgressPopup::new(message, window_builder)
	}

	pub fn centered_in_container(
		message: impl Display,
		container: &impl Container,
		context: &mut impl Context,
	) -> ProgressPopup {
		let message = format!("{message}...");
		let width = progress_popup_width(&message);
		let window_builder = WindowBuilder::centered_in(5, width, container, context);

		ProgressPopup::new(message, window_builder)
	}

	fn new(message: String, window_builder: WindowBuilder) -> ProgressPopup {
		let mut window: Window = window_builder.with_title("Progress").build();

		let done_or_error_x = 1 + 1 + message.len() as u16 + 1;

		window.print_accent((2, 2), message);

		ProgressPopup {
			window,
			done_or_error_x,
			// status: ProgressStatus::Working,
		}
	}

	pub fn process<OK, ERR>(
		mut self,
		mut process: impl FnMut() -> Result<OK, ERR>,
		view: &mut (impl Draw + Darken),
		context: &mut impl Context,
	) -> Result<OK, ERR> {
		view.darken();
		view.draw(context);

		self.draw(context);
		context.present();

		let result = process();

		match &result {
			Ok(_) => self.done(),
			Err(_) => self.error(),
		};

		if context.needs_to_redraw_buffer() {
			view.draw(context);
		}

		self.draw(context);
		context.present();

		self.dismiss(context);

		view.undarken();
		// view.draw(context);

		result
	}

	fn done(&mut self) {
		self.window
			.print_fg((2, self.done_or_error_x), " Done!", LIGHT_GREEN);
		// self.status = ProgressStatus::Done;
	}

	fn error(&mut self) {
		self.window
			.print_fg((2, self.done_or_error_x), "Error!", LIGHT_RED);
		// self.status = ProgressStatus::Error;
	}
}

impl Popup for ProgressPopup {
	fn dismiss_mode() -> DismissMode {
		DismissMode::AfterSeconds(1)
	}

	// fn can_dismiss(&self) -> bool {
	//     match self.status {
	//     	ProgressStatus::Working => false,
	//     	ProgressStatus::Done
	//     	| ProgressStatus::Error
	//     	=> true,
	//     }
	// }
}
