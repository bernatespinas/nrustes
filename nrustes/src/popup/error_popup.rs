use std::fmt::Display;

use crate::colors::{DARK_RED, LIGHT_RED};
use crate::container::Container;
use crate::context::Context;
use crate::darken::Darken;
use crate::prelude::Draw;
use crate::print::Print;
use crate::window::{Window, WindowBuilder};

use super::{DismissMode, Popup};

#[derive(Draw)]
pub struct ErrorPopup {
	#[draw]
	window: Window,
}

impl ErrorPopup {
	pub fn centered_in_screen(message: impl Display, context: &impl Context) -> ErrorPopup {
		let message = message.to_string();

		let width = error_popup_width(&message);
		let win_builder = WindowBuilder::centered(5, width, context);

		let window = build_error_popup_window(win_builder, &message);

		ErrorPopup { window }
	}

	pub fn centered_in_container(
		message: impl Display,
		container: &impl Container,
		context: &impl Context,
	) -> ErrorPopup {
		let message = message.to_string();

		let width = error_popup_width(&message);
		let win_builder = WindowBuilder::centered_in(5, width, container, context);

		let window = build_error_popup_window(win_builder, &message);

		ErrorPopup { window }
	}

	pub fn show(mut self, view: &mut (impl Draw + Darken), context: &mut impl Context) -> Self {
		view.darken();
		view.draw(context);

		self.draw(context);
		self.dismiss(context);

		view.undarken();

		self
	}
}

impl Popup for ErrorPopup {
	fn dismiss_mode() -> DismissMode {
		DismissMode::UserInput
	}

	// fn can_dismiss(&self) -> bool {
	//     true
	// }
}

/// Returns the width the Popup needs to display the message.
fn error_popup_width(message: &str) -> u16 {
	if message.len() + 4 < "Error".len() + 4 {
		"Error".len() as u16 + 4
	} else {
		message.len() as u16 + 4
	}
}

fn build_error_popup_window(win_builder: WindowBuilder, message: &str) -> Window {
	let mut window: Window = win_builder
		.with_title("Error")
		.with_colors(DARK_RED, LIGHT_RED)
		.build();

	window.print_fg((2, 2), message, LIGHT_RED);

	window
}
