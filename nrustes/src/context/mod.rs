use rgb::RGB8;

use crate::container::Container;
use crate::key::Key;
use crate::key_queue::QueueKey;
use crate::nrustes_config::NrustesConfig;

mod render_sprite;
pub use render_sprite::RenderSprite;

/// Provides a common API for all the available backends.
pub trait Context: RenderSprite + QueueKey {
	fn nrustes_config(&self) -> &NrustesConfig;

	/// Pushes changes to program's window.
	fn present(&mut self);

	/// Clears the program's window.
	fn clear(&mut self);

	/// Returns the height of the program's window.
	fn height(&self) -> u16;

	/// Returns the width of the program's window.
	fn width(&self) -> u16;

	/// Returns the default primary color which is defined in `NrustesConfig`.
	fn primary_color(&self) -> RGB8 {
		self.nrustes_config().primary_color
	}

	/// Returns the default accent color which is defined in `NrustesConfig`.
	fn accent_color(&self) -> RGB8 {
		self.nrustes_config().accent_color
	}

	/// Renders text directly to the program's window.
	fn render_text(&mut self, y: u16, x: u16, text: &str, fg: RGB8, bg: RGB8);

	/// Renders text directly to the program's window. Relative to `container`.
	fn render_text_container(
		&mut self,
		container: &impl Container,
		y: u16,
		x: u16,
		text: &str,
		fg: RGB8,
		bg: RGB8,
	) {
		self.render_text(container.y() + y, container.x() + x, text, fg, bg);
	}

	/// Renders a character directly to the program's window.
	fn render_char(&mut self, y: u16, x: u16, text: char, fg: RGB8, bg: RGB8);

	/// Renders a character directly to the program's window. Relative to `container`.
	fn render_char_container(
		&mut self,
		container: &impl Container,
		y: u16,
		x: u16,
		text: char,
		fg: RGB8,
		bg: RGB8,
	) {
		self.render_char(container.y() + y, container.x() + x, text, fg, bg);
	}

	/// Renders a vertical line which starts at (y, x).
	fn render_line_vertical(&mut self, y: u16, x: u16, length: u16, fg: RGB8, bg: RGB8) {
		let vertical = self.nrustes_config().vertical;

		for i in y..y + length {
			self.render_char(i, x, vertical, fg, bg);
		}
	}

	/// Renders a horizontal line which starts at (y, x).
	fn render_line_horizontal(&mut self, y: u16, x: u16, length: u16, fg: RGB8, bg: RGB8) {
		let horizontal = self.nrustes_config().horizontal;

		for i in x..x + length {
			self.render_char(y, i, horizontal, fg, bg);
		}
	}

	/// Renders a rectangle (just its borders) with its top left corner at (y, x).
	fn render_rect(&mut self, y: u16, x: u16, height: u16, width: u16, fg: RGB8, bg: RGB8) {
		let (vertical, horizontal, top_left, top_right, bottom_left, bottom_right) = {
			let config = self.nrustes_config();

			(
				config.vertical,
				config.horizontal,
				config.top_left,
				config.top_right,
				config.bottom_left,
				config.bottom_right,
			)
		};

		for i in y + 1..y + height - 1 {
			self.render_char(i, x, vertical, fg, bg);
			self.render_char(i, x + width - 1, vertical, fg, bg);
		}

		for i in x + 1..x + width - 1 {
			self.render_char(y, i, horizontal, fg, bg);
			self.render_char(y + height - 1, i, horizontal, fg, bg);
		}

		self.render_char(y, x, top_left, fg, bg);
		self.render_char(y, x + width - 1, top_right, fg, bg);
		self.render_char(y + height - 1, x, bottom_left, fg, bg);
		self.render_char(y + height - 1, x + width - 1, bottom_right, fg, bg);
	}

	/// Clears a rectangular zone of the program's window.
	fn clear_rect(&mut self, y: u16, x: u16, height: u16, width: u16);

	/// Clears a rectangular zone of the program's window. Relative to `container`.
	fn clear_rect_container(
		&mut self,
		container: &impl Container,
		y: u16,
		x: u16,
		height: u16,
		width: u16,
	) {
		self.clear_rect(container.y() + y, container.x() + x, height, width);
	}

	/// Clears the content of a line from `container` - its decorations are left intact.
	fn clear_line_container(&mut self, container: &impl Container, y: u16) {
		self.clear_rect_container(container, y, 1, 1, container.width() - 2);
	}

	/// Waits for user input, obtains a backend-specific key and returns the corresponding `Key`.
	fn input_key_bypass_queue(&mut self) -> Key;

	/// Adds a `Key` to the queue. It will be returned the next time `input_key`
	/// is called instead of getting input from the user.
	fn queue_key(&mut self, key: Key) {
		self.key_queue_mut().add_key(key);
	}

	/// If there's a `Key` in the queue, it will be removed and returned.
	/// Otherwise, calls `input_key_bypass_queue`.
	fn input_key(&mut self) -> Key {
		if let Some(key) = self.key_queue_mut().pop_front() {
			self.present();

			std::thread::sleep(std::time::Duration::from_millis(
				self.nrustes_config().automated_input_delay_millis,
			));

			return key;
		}

		self.input_key_bypass_queue()
	}

	/// Discards all the events that are waiting to be processed in a queue?
	fn flush_events(&mut self);

	/// Whether drawing operations could be defined as slow. Backends which render to a terminal window are slower than those which render to an accelerated canvas.
	fn drawing_is_slow(&self) -> bool;

	/// Whether the buffer of the program's window needs to be drawn so that all content is correctly rendered.
	fn needs_to_redraw_buffer(&self) -> bool;
}
