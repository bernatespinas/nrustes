use rgb::RGB8;

pub trait RenderSprite {
	fn can_render_graphics(&self) -> bool;

	/// Returns the program's window height and width in the smallest unit possible. For example, `ContextSdl2` returns its size in pixels whereas `ContextTermion` and `ContextCrossterm` return their size in terminal cells.
	fn size_in_smallest_units(&self) -> (u16, u16);

	/// Returns the size of font sprites in the smallest unit possible.
	fn font_sprite_size(&self) -> (u16, u16);

	/// Returns the size of graphics sprites in the smallest unit possible.
	fn graphics_sprite_size(&self) -> (u16, u16);

	fn render_font_sprite(&mut self, key: &(char, RGB8, RGB8), y: u16, x: u16);

	/// Panics if it doesn't support rendering graphic sprites.
	fn render_graphics_sprite(&mut self, _key: &String, _y: u16, _x: u16) {
		panic!("Current Context does not support rendering graphics.");
	}

	// TODO Rename.
	fn render_string_with_font_sprites(&mut self, text: &str, fg: RGB8, bg: RGB8, y: u16, x: u16) {
		// TODO Should I use graphemes...? If I do, I won't be able to use `render_sprite_char` because I will be iteratin `&str`s instead of `char`s. Perhaps I should always use `&str`s?
		// for (i, sprite_char) in text.graphemes(true).enumerate() {
		for (i, font_char) in text.chars().enumerate() {
			self.render_font_sprite(&(font_char, fg, bg), y, x + i as u16);
		}
	}

	/// Panics if it doesn't support rendering graphic sprites.
	fn set_current_graphics_texture(&mut self, _name: String) {
		panic!("Current Context does not support rendering graphics.");
	}
}
