use std::fmt::Display;

use rgb::RGB8;

use crate::colors::BLACK;

/// A trait to expose functionality about storing text which will be displayed at some point.
pub trait Print {
	/// The coordinates which are used to decide where to print text.
	type Coordinates;

	/// Returns the primary color.
	fn primary_color(&self) -> RGB8;

	/// Returns the accent color.
	fn accent_color(&self) -> RGB8;

	/// Stores some text to be displayed with foreground and background colors.
	fn print(&mut self, coords: Self::Coordinates, text: impl Display, fg: RGB8, bg: RGB8);

	/// Stores some text to be displayed with a foreground color.
	fn print_fg(&mut self, coords: Self::Coordinates, text: impl Display, fg: RGB8) {
		self.print(coords, text, fg, BLACK);
	}

	/// Stores some text to be displayed with the primary color.
	fn print_primary(&mut self, coords: Self::Coordinates, text: impl Display) {
		self.print_fg(coords, text, self.primary_color());
	}

	/// Stores some text to be displayed with the accent color.
	fn print_accent(&mut self, coords: Self::Coordinates, text: impl Display) {
		self.print_fg(coords, text, self.accent_color());
	}
}
