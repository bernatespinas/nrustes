use std::fmt::Display;

use crate::context::Context;
use crate::darken::Darken;
use crate::draw::Draw;
use crate::window::Window;

mod build_container;
pub use build_container::BuildContainer;

/// Common methods for `Window`-like structs.
pub trait Container: Draw + Darken {
	fn window(&self) -> &Window;
	fn window_mut(&mut self) -> &mut Window;

	fn y(&self) -> u16 {
		self.window().y()
	}

	fn x(&self) -> u16 {
		self.window().x()
	}

	fn height(&self) -> u16 {
		self.window().height()
	}

	fn width(&self) -> u16 {
		self.window().width()
	}

	fn change_title(&mut self, title: impl Display) {
		self.window_mut().change_title(title);
	}

	fn change_bottom_text(&mut self, bottom_text: impl Display) {
		self.window_mut().change_bottom_text(bottom_text);
	}

	/// Updates the content and decorations so that everything is ready to be drawn.
	fn update(&mut self);

	fn draw_decorations(&self, context: &mut impl Context) {
		self.window().draw_decorations(context);
	}

	fn draw_content(&mut self, context: &mut impl Context) {
		self.window_mut().draw_content(context);
	}

	fn erase_all(&self, context: &mut impl Context) {
		self.window().erase_all(context);
	}

	fn erase_content(&self, context: &mut impl Context) {
		self.window().erase_content(context);
	}

	fn delete_content(&mut self) {
		self.window_mut().delete_content();
	}

	// TODO discard_content or something.
	fn erase_and_delete_drawn_content(&mut self, context: &mut impl Context) {
		self.window_mut().erase_and_delete_drawn_content(context);
	}
}
