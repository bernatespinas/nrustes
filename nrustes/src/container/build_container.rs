use crate::container::Container;

pub trait BuildContainer<C: Container> {
	fn build_container(self) -> C;
}
