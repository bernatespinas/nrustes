//! A wrapper around other libraries to facilitate the creation of "terminal" programs.
//!
//! Choose between [crossterm](https://crates.io/crates/crossterm), [termion](https://crates.io/crates/termion) and [SDL2](https://crates.io/crates/sdl2), use the same API and switch between them effortlessly.
//!
//! First, add `nrustes` to your `Cargo.toml`:
//!
//! ```toml
//! [dependencies.nrustes]
//! git = "https://github.com/bernatespinas/nrustes.git"
//! features = ["crossterm_backend"]
//! ```
//! These are the main available features:
//! - "termion_backend"
//! - "crossterm_backend"
//! - "sdl2_backend"
//! - By default, all of them.
//!
//! Check the examples to see how everything works!
//! For more advanced examples check [dastan](https://www.gitlab.com/bernatespinas/dastan), the roguelike I'm also creating using nrustes.
//! `ContextSdl2` is more complicated to create, for example.

pub mod backends;

pub mod colors;

pub mod container;

pub mod context;

pub mod darken;

pub mod draw;

pub mod form;

pub mod handle_input;

pub mod key;

pub mod key_queue;

pub mod modal;

pub mod nrustes_config;

pub mod pad;

pub mod popup;

pub mod print;

pub mod scrollable;

pub mod scrollbar;

pub mod shortcuts;

pub mod texture_atlas;

pub mod views;

pub mod window;

pub mod prelude;

#[cfg(feature = "terminal_backend_tag")]
/// Returns the dimensions ((lines, columns)) of the terminal.
fn get_terminal_height_width() -> (u16, u16) {
	use terminal_size::{terminal_size, Height, Width};

	match terminal_size() {
		Some((Width(w), Height(h))) => (h, w),
		None => panic!("Unable to get terminal size"),
	}
}
