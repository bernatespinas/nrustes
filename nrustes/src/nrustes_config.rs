use std::{error::Error, path::PathBuf};

use rgb::RGB8;
use serde::{Deserialize, Serialize};

use crate::colors::{DARK_GREY, LIGHT_GREY};

#[derive(Debug, Serialize, Deserialize)]
pub struct NrustesConfig {
	#[serde(deserialize_with = "rgb_tuple_to_rgb8")]
	/// The color which will be used as the primary color by default.
	pub primary_color: RGB8,

	#[serde(deserialize_with = "rgb_tuple_to_rgb8")]
	/// The color which will be used as the accent color by default.
	pub accent_color: RGB8,

	/// The character which represents a vertical line.
	pub vertical: char,

	/// The character which represents a horizontal line.
	pub horizontal: char,

	/// The character which represents a top left corner.
	pub top_left: char,

	/// The character which represents a top right corner.
	pub top_right: char,

	/// The character which represents a bottom right corner.
	pub bottom_right: char,

	/// The character which represents a bottom left corner.
	pub bottom_left: char,

	pub ttf_font_file_path: Option<PathBuf>,

	/// Font size when outputted to the screen.
	pub font_size: (u16, u16),

	/// Graphics size when outputted to the screen.
	pub graphics_size: (u16, u16),

	pub placeholder_graphics_sprite: Option<String>,

	/// The delay in milliseconds before automated user input is returned.
	pub automated_input_delay_millis: u64,
}

impl NrustesConfig {
	pub fn load(path: PathBuf) -> Result<Self, Box<dyn Error>> {
		let config_content = std::fs::read_to_string(path)?;

		let nrustes_config = ron::de::from_str(&config_content)?;

		Ok(nrustes_config)
	}
}

impl Default for NrustesConfig {
	fn default() -> Self {
		Self {
			primary_color: DARK_GREY,
			accent_color: LIGHT_GREY,
			vertical: '|',
			horizontal: '-',
			top_left: '+',
			top_right: '+',
			bottom_right: '+',
			bottom_left: '+',
			ttf_font_file_path: None,
			font_size: (1, 1),
			graphics_size: (1, 1),
			placeholder_graphics_sprite: None,
			automated_input_delay_millis: 150,
		}
	}
}

fn rgb_tuple_to_rgb8<'de, D>(deserializer: D) -> Result<RGB8, D::Error>
where
	D: serde::Deserializer<'de>,
{
	let rgb_tuple: [u8; 3] = serde::Deserialize::deserialize(deserializer)?;

	Ok(RGB8::new(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
}
