use rgb::RGB8;

use crate::colors::ToNativeColor;

impl ToNativeColor<crossterm::style::Color> for RGB8 {
	fn to_native_color(self) -> crossterm::style::Color {
		crossterm::style::Color::Rgb {
			r: self.r,
			g: self.g,
			b: self.b,
		}
	}
}
