use std::io::{Stdout, Write};

use rgb::RGB8;

use crossterm::{cursor, event, queue, style, terminal};

use crate::{
	colors::ToNativeColor,
	context::Context,
	get_terminal_height_width,
	key::Key,
	key_queue::{KeyQueue, QueueKey},
	nrustes_config::NrustesConfig,
};

mod key;

mod color;

mod render_sprite;

pub struct ContextCrossterm {
	height: u16,
	width: u16,

	stdout: Stdout,

	key_queue: KeyQueue,

	nrustes_config: NrustesConfig,
}

impl ContextCrossterm {
	pub fn new(nrustes_config: NrustesConfig) -> ContextCrossterm {
		let (height, width) = get_terminal_height_width();

		terminal::enable_raw_mode().unwrap();

		ContextCrossterm {
			height,
			width,

			stdout: std::io::stdout(),

			key_queue: KeyQueue::default(),

			nrustes_config,
		}
	}
}

impl Context for ContextCrossterm {
	fn nrustes_config(&self) -> &NrustesConfig {
		&self.nrustes_config
	}

	fn present(&mut self) {
		self.stdout.flush().unwrap();
	}

	fn clear(&mut self) {
		queue!(
			self.stdout,
			cursor::MoveTo(0, 0),
			terminal::Clear(terminal::ClearType::All)
		)
		.unwrap();
	}

	fn height(&self) -> u16 {
		self.height
	}

	fn width(&self) -> u16 {
		self.width
	}

	fn render_text(&mut self, y: u16, x: u16, text: &str, fg: RGB8, bg: RGB8) {
		queue!(
			self.stdout,
			cursor::MoveTo(x, y),
			style::SetForegroundColor(fg.to_native_color()),
			style::SetBackgroundColor(bg.to_native_color()),
			style::Print(text),
			cursor::Hide,
			style::ResetColor
		)
		.unwrap();
	}

	fn render_char(&mut self, y: u16, x: u16, text: char, fg: RGB8, bg: RGB8) {
		queue!(
			self.stdout,
			cursor::MoveTo(x, y),
			style::SetForegroundColor(fg.to_native_color()),
			style::SetBackgroundColor(bg.to_native_color()),
			style::Print(text),
			cursor::Hide,
			style::ResetColor
		)
		.unwrap();
	}

	fn clear_rect(&mut self, y: u16, x: u16, height: u16, width: u16) {
		let erased_line = " ".repeat(width as usize);
		for i in 0..height {
			queue!(
				self.stdout,
				cursor::MoveTo(x, y + i),
				style::Print(&erased_line),
				cursor::Hide,
			)
			.unwrap();
		}
	}

	fn input_key_bypass_queue(&mut self) -> Key {
		self.present();

		loop {
			match event::read().unwrap() {
				event::Event::Key(event::KeyEvent { code, .. }) => {
					return Key::from(code);
				}
				_ => continue,
			};
		}
	}

	fn flush_events(&mut self) {}

	fn drawing_is_slow(&self) -> bool {
		true
	}

	fn needs_to_redraw_buffer(&self) -> bool {
		false
	}
}

impl QueueKey for ContextCrossterm {
	fn key_queue_mut(&mut self) -> &mut KeyQueue {
		&mut self.key_queue
	}
}
