use crossterm::event::KeyCode;

use direction_simple::Direction;

use crate::key::Key;

impl From<KeyCode> for Key {
	fn from(key_code: KeyCode) -> Key {
		match key_code {
			KeyCode::Up => Key::Movement(Direction::Up),
			KeyCode::Down => Key::Movement(Direction::Down),
			KeyCode::Right => Key::Movement(Direction::Right),
			KeyCode::Left => Key::Movement(Direction::Left),

			KeyCode::Enter => Key::Select,
			KeyCode::Tab => Key::Tab,
			KeyCode::Backspace => Key::Backspace,
			KeyCode::Esc => Key::Back,

			KeyCode::Char(ch) => Key::Char(ch),
			_ => Key::Unknown,
		}
	}
}
