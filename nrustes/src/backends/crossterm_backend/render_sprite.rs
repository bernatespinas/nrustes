use rgb::RGB8;

use crate::context::{Context, RenderSprite};

use super::ContextCrossterm;

impl RenderSprite for ContextCrossterm {
	fn can_render_graphics(&self) -> bool {
		false
	}

	fn size_in_smallest_units(&self) -> (u16, u16) {
		(self.height, self.width)
	}

	fn font_sprite_size(&self) -> (u16, u16) {
		(1, 1)
	}

	fn graphics_sprite_size(&self) -> (u16, u16) {
		(1, 1)
	}

	fn render_font_sprite(&mut self, key: &(char, RGB8, RGB8), y: u16, x: u16) {
		let (text, fg, bg) = key;

		self.render_char(y, x, *text, *fg, *bg);
	}
}
