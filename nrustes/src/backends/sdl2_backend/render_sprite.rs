use rgb::RGB8;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::render::Texture;

use crate::context::{Context, RenderSprite};

use super::ContextSdl2;

impl<'ctx> RenderSprite for ContextSdl2<'ctx> {
	fn can_render_graphics(&self) -> bool {
		true
	}

	fn size_in_smallest_units(&self) -> (u16, u16) {
		(self.pixel_height, self.pixel_width)
	}

	fn font_sprite_size(&self) -> (u16, u16) {
		self.nrustes_config.font_size
	}

	fn graphics_sprite_size(&self) -> (u16, u16) {
		self.nrustes_config.graphics_size
	}

	fn render_font_sprite(&mut self, key: &(char, RGB8, RGB8), y: u16, x: u16) {
		let texture = self.texture_atlas_font.current_texture();

		let Some(sprite_coords) = self.texture_atlas_font.get(key) else {
			log::error!(
				"Can't find font sprite with key {key:?}, creating texture..."
			);

			let (text, fg, bg) = *key;

			self.render_texture_text(y, x, &text.to_string(), fg, bg);

			return;
		};

		let sprite_size = self.texture_atlas_font.current_source_sprite_size();

		copy_to_canvas(
			texture,
			sprite_coords,
			sprite_size,
			&mut self.canvas,
			y,
			x,
			self.nrustes_config.font_size,
		);
	}

	fn render_graphics_sprite(&mut self, key: &String, y: u16, x: u16) {
		let texture = self.texture_atlas_graphics.current_texture();

		let sprite_coords = match self.texture_atlas_graphics.get(key) {
			Some(coords) => coords,
			None => match &self.nrustes_config().placeholder_graphics_sprite {
				None => {
					log::error!("Can't find graphics sprite {key}. Not printing anything because a placeholder sprite has not been defined.");
					return;
				}
				Some(sprite) => {
					log::error!(
						"Can't find graphics sprite {key}. Rendering placeholder sprite {sprite}."
					);

					self.texture_atlas_graphics.get(sprite).unwrap()
				}
			},
		};

		let src_size = self.texture_atlas_graphics.current_source_sprite_size();

		copy_to_canvas(
			texture,
			sprite_coords,
			src_size,
			&mut self.canvas,
			y,
			x,
			self.nrustes_config.graphics_size,
		);
	}

	fn set_current_graphics_texture(&mut self, name: String) {
		self.texture_atlas_graphics.set_current_texture(name);
	}
}

fn copy_to_canvas(
	texture: &Texture<'_>,
	sprite_coords: &(usize, usize),
	sprite_size_source: (u16, u16),

	canvas: &mut Canvas<sdl2::video::Window>,
	y: u16,
	x: u16,
	sprite_size_destination: (u16, u16),
) {
	let (src_height, src_width) = sprite_size_source;
	let (dst_height, dst_width) = sprite_size_destination;

	let dst_y = (y * dst_height) as i32;
	let dst_x = (x * dst_width) as i32;

	let (src_y, src_x) = *sprite_coords;

	canvas
		.copy(
			texture,
			// Source Rect.
			Rect::new(
				src_x as i32,
				src_y as i32,
				src_width as u32,
				src_height as u32,
			),
			// Destination Rect.
			Rect::new(dst_x, dst_y, dst_width as u32, dst_height as u32),
		)
		.unwrap();
}
