use sdl2::{image::LoadTexture, render::TextureCreator, video::WindowContext};

use crate::texture_atlas::{LoadTextureAtlas, TextureAtlasLoader};

use super::ContextSdl2;

impl<'ctx> LoadTextureAtlas<'ctx, ContextSdl2<'ctx>, TextureCreator<WindowContext>>
	for TextureAtlasLoader
{
	fn load_font(
		&self,
		tag: &str,
		texture_file_name: &str,
		atlas_file_name: &str,
		sprite_size: (u16, u16),
		context: &mut ContextSdl2<'ctx>,
		texture_creator: &'ctx TextureCreator<WindowContext>,
	) -> Result<(), String> {
		let texture_file_path = self.resource_path(texture_file_name);
		let texture = texture_creator.load_texture(texture_file_path)?;

		let atlas = self.load_font_atlas(atlas_file_name).map_err(|error| {
			format!("Error while deserializing atlas {atlas_file_name}. Error: {error:?}.")
		})?;

		context
			.texture_atlas_font
			.add_texture(tag, texture, atlas, sprite_size)
	}

	fn load_graphics(
		&self,
		tag: &str,
		texture_file_name: &str,
		atlas_file_name: &str,
		sprite_size: (u16, u16),
		context: &mut ContextSdl2<'ctx>,
		texture_creator: &'ctx TextureCreator<WindowContext>,
	) -> Result<(), String> {
		let texture_file_path = self.resource_path(texture_file_name);
		let texture = texture_creator.load_texture(texture_file_path)?;

		let atlas = self.load_graphics_atlas(atlas_file_name).map_err(|error| {
			format!("Error while deserializing atlas {atlas_file_name}. Error: {error:?}.")
		})?;

		context
			.texture_atlas_graphics
			.add_texture(tag, texture, atlas, sprite_size)
	}
}
