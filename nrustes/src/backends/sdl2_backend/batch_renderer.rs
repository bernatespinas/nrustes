use std::collections::HashMap;

use sdl2::render::Texture;

use super::{ContextSdl2, TextureAtlas, TextureAtlasRenderer, TextureAtlasSprite};

pub trait BatchRenderer {
	type Key: Eq + std::hash::Hash;

	fn hashmap(&mut self) -> &mut HashMap<Self::Key, Vec<(i32, i32)>>;

	fn add(&mut self, key: Self::Key, pos: (i32, i32)) {
		let hashmap = self.hashmap();

		if hashmap.contains_key(&key) {
			hashmap.get_mut(&key).unwrap().push(pos);
		} else {
			hashmap.insert(key, vec!(pos));
		}
	}

	fn render(
		&mut self,
		texture: &Texture<'_>,
		context_sdl2: &mut ContextSdl2
	);
}

type asd<'a> = <TextureAtlasSprite<'a> as TextureAtlas<'a>>::Key;

#[derive(Default)]
pub struct SpriteBatchRenderer<'a> {
	hashmap: HashMap<asd<'a>, Vec<(i32, i32)>>,
}

impl<'a> BatchRenderer for SpriteBatchRenderer<'a> {
	type Key = asd<'a>;

	fn hashmap(&mut self) -> &mut HashMap<Self::Key, Vec<(i32, i32)>> {
		&mut self.hashmap
	}

	fn render(&mut self, texture: &Texture<'_>, context_sdl2: &mut ContextSdl2) {
		for (key, value) in &self.hashmap {
			for (y, x) in value {
				TextureAtlasSprite::render_once(
					context_sdl2,
					key,
					// TODO
					*y as u16,
					*x as u16
				)
			}
		}
	}
}

// pub struct CharBatchRenderer {

// }