use sdl2::keyboard::{Keycode, Mod};

use direction_simple::Direction;

use crate::key::Key;

impl From<Keycode> for Key {
	fn from(key_code: Keycode) -> Key {
		match key_code {
			Keycode::Up => Key::Movement(Direction::Up),
			Keycode::Down => Key::Movement(Direction::Down),
			Keycode::Right => Key::Movement(Direction::Right),
			Keycode::Left => Key::Movement(Direction::Left),

			Keycode::Return | Keycode::KpEnter => Key::Select,
			Keycode::Tab => Key::Tab,

			Keycode::Backspace => Key::Backspace,

			Keycode::Num0 | Keycode::Kp0 => Key::Char('0'),
			Keycode::Num1 | Keycode::Kp1 => Key::Char('1'),
			Keycode::Num2 | Keycode::Kp2 => Key::Char('2'),
			Keycode::Num3 | Keycode::Kp3 => Key::Char('3'),
			Keycode::Num4 | Keycode::Kp4 => Key::Char('4'),
			Keycode::Num5 | Keycode::Kp5 => Key::Char('5'),
			Keycode::Num6 | Keycode::Kp6 => Key::Char('6'),
			Keycode::Num7 | Keycode::Kp7 => Key::Char('7'),
			Keycode::Num8 | Keycode::Kp8 => Key::Char('8'),
			Keycode::Num9 | Keycode::Kp9 => Key::Char('9'),

			// Lowercase by default. If any "uppercase mods" are present when processing the Event I turn these to uppercase.
			Keycode::A => Key::Char('a'),
			Keycode::B => Key::Char('b'),
			Keycode::C => Key::Char('c'),
			Keycode::D => Key::Char('d'),
			Keycode::E => Key::Char('e'),
			Keycode::F => Key::Char('f'),
			Keycode::G => Key::Char('g'),
			Keycode::H => Key::Char('h'),
			Keycode::I => Key::Char('i'),
			Keycode::J => Key::Char('j'),
			Keycode::K => Key::Char('k'),
			Keycode::L => Key::Char('l'),
			Keycode::M => Key::Char('m'),
			Keycode::N => Key::Char('n'),
			Keycode::O => Key::Char('o'),
			Keycode::P => Key::Char('p'),
			Keycode::Q => Key::Char('q'),
			Keycode::R => Key::Char('r'),
			Keycode::S => Key::Char('s'),
			Keycode::T => Key::Char('t'),
			Keycode::U => Key::Char('u'),
			Keycode::V => Key::Char('v'),
			Keycode::W => Key::Char('w'),
			Keycode::X => Key::Char('x'),
			Keycode::Y => Key::Char('y'),
			Keycode::Z => Key::Char('z'),

			Keycode::Period | Keycode::KpPeriod => Key::Char('.'),
			Keycode::Comma | Keycode::KpComma => Key::Char(','),
			Keycode::Exclaim => Key::Char('!'),
			Keycode::Question => Key::Char('?'),

			Keycode::LeftParen | Keycode::KpLeftParen => Key::Char('('),
			Keycode::RightParen | Keycode::KpRightParen => Key::Char(')'),
			Keycode::LeftBracket => Key::Char('['),
			Keycode::RightBracket => Key::Char(']'),
			Keycode::KpLeftBrace => Key::Char('{'),
			Keycode::KpRightBrace => Key::Char('}'),

			Keycode::Plus | Keycode::KpPlus => Key::Char('+'),
			Keycode::Minus | Keycode::KpMinus => Key::Char('-'),
			Keycode::Asterisk | Keycode::KpMultiply => Key::Char('*'),
			Keycode::Slash | Keycode::KpDivide => Key::Char('/'),

			Keycode::Greater => Key::Char('>'),
			Keycode::Less => Key::Char('<'),

			Keycode::Space => Key::Char(' '),

			Keycode::Escape => Key::Back,

			_ => Key::Unknown,
		}
	}
}

impl From<sdl2::event::Event> for Key {
	fn from(sdl2_event: sdl2::event::Event) -> Key {
		match sdl2_event {
			sdl2::event::Event::KeyDown {
				keycode: Some(keycode_val),
				keymod,
				..
			} => Key::from((keycode_val, keymod)),
			_ => Key::Unknown,
		}
	}
}

impl From<(Keycode, Mod)> for Key {
	fn from((keycode, keymod): (Keycode, Mod)) -> Key {
		let mut to_text_key = Key::from(keycode);

		if let Key::Char(ch) = to_text_key {
			if keymod.contains(sdl2::keyboard::Mod::CAPSMOD)
				|| keymod.contains(sdl2::keyboard::Mod::LSHIFTMOD)
				|| keymod.contains(sdl2::keyboard::Mod::RSHIFTMOD)
			{
				to_text_key = Key::Char(ch.to_uppercase().next().unwrap());
			} // else if keymod.contains(sdl2::keyboard::Mod::RALTMOD)
		}

		to_text_key
	}
}
