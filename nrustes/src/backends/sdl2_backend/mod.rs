pub use sdl2;
use sdl2::render::Texture;

use rgb::RGB8;
use unicode_segmentation::UnicodeSegmentation;

use crate::{
	colors::ToNativeColor,
	context::{Context, RenderSprite},
	key::Key,
	key_queue::{KeyQueue, QueueKey},
	nrustes_config::NrustesConfig,
	texture_atlas::TextureAtlas,
};

mod key;

mod color;

mod render_sprite;

mod load_texture_atlas;

// mod batch_renderer;

// const BORDER_WEIGHT: u8 = 3;

pub struct ContextSdl2<'a> {
	pixel_height: u16,
	pixel_width: u16,

	height: u16,
	width: u16,

	event_pump: sdl2::EventPump,
	event_subsystem: sdl2::EventSubsystem,
	canvas: sdl2::render::Canvas<sdl2::video::Window>,

	font: sdl2::ttf::Font<'a, 'a>,

	pub texture_atlas_font: TextureAtlas<(char, RGB8, RGB8), Texture<'a>>,
	pub texture_atlas_graphics: TextureAtlas<String, Texture<'a>>,

	key_queue: KeyQueue,

	nrustes_config: NrustesConfig,
}

impl<'a> ContextSdl2<'a> {
	pub fn new(
		font: sdl2::ttf::Font<'a, 'a>,
		event_pump: sdl2::EventPump,
		event_subsystem: sdl2::EventSubsystem,
		canvas: sdl2::render::Canvas<sdl2::video::Window>,
		nrustes_config: NrustesConfig,
	) -> ContextSdl2<'a> {
		let (font_height, font_width) = nrustes_config.font_size;

		let texture_atlas_font = TextureAtlas::default();
		let texture_atlas_graphics = TextureAtlas::default();

		let display_mode = canvas.window().display_mode().unwrap();
		// TODO Using font size. Nothing wrong with that, but I think I should make it clear somewhere.
		let height = display_mode.h as u16 / font_height;
		let width = display_mode.w as u16 / font_width;

		ContextSdl2 {
			pixel_height: display_mode.h as u16,
			pixel_width: display_mode.w as u16,

			height,
			width,

			event_pump,
			event_subsystem,

			canvas,

			font,

			texture_atlas_font,
			texture_atlas_graphics,

			key_queue: KeyQueue::default(),

			nrustes_config,
		}
	}

	pub fn sdl2_ttf_context() -> sdl2::ttf::Sdl2TtfContext {
		sdl2::ttf::init().expect("SDL2 TTF context creation failed.")
	}

	pub fn sdl2_context() -> Result<sdl2::Sdl, String> {
		sdl2::init()
	}

	fn render_texture_text(&mut self, y: u16, x: u16, text: &str, fg: RGB8, bg: RGB8) {
		let surface = self
			.font
			.render(text)
			.shaded::<sdl2::pixels::Color>(fg.to_native_color(), bg.to_native_color())
			.unwrap();

		let texture_creator = self.canvas.texture_creator();

		let texture = texture_creator
			.create_texture_from_surface(surface)
			.unwrap();

		let (src_height, src_width) = self.texture_atlas_font.current_source_sprite_size();

		let y = (y * src_height) as i32;
		let x = (x * src_width) as i32;
		let height = src_height as u32;
		let width = text.graphemes(true).count() as u32 * src_width as u32;

		self.canvas
			.copy(
				&texture,
				None,
				Some(sdl2::rect::Rect::new(x, y, width, height)),
			)
			.unwrap();
	}
}

impl<'a> Context for ContextSdl2<'a> {
	fn nrustes_config(&self) -> &NrustesConfig {
		&self.nrustes_config
	}

	fn present(&mut self) {
		self.canvas.present();
	}

	fn clear(&mut self) {
		self.canvas
			.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
		self.canvas.clear();
	}

	fn height(&self) -> u16 {
		self.height
	}

	fn width(&self) -> u16 {
		self.width
	}

	fn render_text(&mut self, y: u16, x: u16, text: &str, fg: RGB8, bg: RGB8) {
		if text_has_only_ascii_chars(text) {
			self.render_string_with_font_sprites(text, fg, bg, y, x);
		} else {
			self.render_texture_text(y, x, text, fg, bg);
		}
	}

	fn render_char(&mut self, y: u16, x: u16, text: char, fg: RGB8, bg: RGB8) {
		let key = (text, fg, bg);

		if self.texture_atlas_font.get(&key).is_some() {
			self.render_font_sprite(&key, y, x);
		} else {
			log::error!("Sprite not found for char: {text}, {fg:?}, {bg:?}. Rendering it as text.");
			self.render_texture_text(y, x, &text.to_string(), fg, bg);
		}
	}

	fn render_rect(&mut self, y: u16, x: u16, height: u16, width: u16, fg: RGB8, bg: RGB8) {
		let (vertical, horizontal, top_left, top_right, bottom_left, bottom_right) = {
			let config = self.nrustes_config();

			(
				config.vertical,
				config.horizontal,
				config.top_left,
				config.top_right,
				config.bottom_left,
				config.bottom_right,
			)
		};

		for i in y..y + height - 1 {
			self.render_font_sprite(&(vertical, fg, bg), i, x);
			self.render_font_sprite(&(vertical, fg, bg), i, x + width - 1);
		}

		for i in x..x + width - 1 {
			self.render_font_sprite(&(horizontal, fg, bg), y, i);
			self.render_font_sprite(&(horizontal, fg, bg), y + height - 1, i);
		}

		self.render_font_sprite(&(top_left, fg, bg), y, x);
		self.render_font_sprite(&(top_right, fg, bg), y, x + width - 1);
		self.render_font_sprite(&(bottom_left, fg, bg), y + height - 1, x);
		self.render_font_sprite(&(bottom_right, fg, bg), y + height - 1, x + width - 1);
	}

	// TODO Only works as expected when clearing text. // TODO Then make the fn's name say so.
	fn clear_rect(&mut self, y: u16, x: u16, height: u16, width: u16) {
		// self.canvas.set_draw_color(*BLACK);
		self.canvas
			.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));

		let (font_height, font_width) = self.nrustes_config.font_size;

		self.canvas
			.fill_rect(sdl2::rect::Rect::new(
				(x * font_width) as i32,
				(y * font_height) as i32,
				(width * font_width) as u32,
				(height * font_height) as u32,
			))
			.unwrap();
	}

	fn input_key_bypass_queue(&mut self) -> Key {
		self.present();

		// Wait for event and only return when a key is pressed (ignoring mouse, resize...).
		loop {
			match self.event_pump.wait_event() {
				sdl2::event::Event::KeyDown {
					keycode: Some(keycode_val),
					keymod,
					..
				} => return Key::from((keycode_val, keymod)),
				// Ignore all events that are not keyboard key presses.
				// TODO Can I tell SDL2 to just not fire them?
				_ => continue,
			};
		}
	}

	fn flush_events(&mut self) {
		// "Pumps the event loop", which I think that means that puts the events from somewhere to somewhere else where I can remove them with the next method call. I really don't care that much. Should I?
		// Without this, I think the next method call does something that hasn't got any impact, and then, at the next input_key, the events that weren't pumped get pumped and processed.
		self.event_pump.pump_events();

		// Flushes (removes?) all the input events that are key presses.
		self.event_subsystem
			.flush_event(sdl2::event::EventType::KeyDown);
	}

	fn drawing_is_slow(&self) -> bool {
		false
	}

	fn needs_to_redraw_buffer(&self) -> bool {
		true
	}
}

impl QueueKey for ContextSdl2<'_> {
	fn key_queue_mut(&mut self) -> &mut KeyQueue {
		&mut self.key_queue
	}
}

fn text_has_only_ascii_chars(text: &str) -> bool {
	text.graphemes(true).count() == text.len()
}
