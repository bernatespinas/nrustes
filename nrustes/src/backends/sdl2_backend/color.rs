use rgb::RGB8;

use crate::colors::ToNativeColor;

impl ToNativeColor<sdl2::pixels::Color> for RGB8 {
	fn to_native_color(self) -> sdl2::pixels::Color {
		sdl2::pixels::Color::RGB(self.r, self.g, self.b)
	}
}
