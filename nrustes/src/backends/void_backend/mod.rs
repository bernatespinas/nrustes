use rgb::RGB8;

use crate::{
	context::{Context, RenderSprite},
	key::Key,
	key_queue::{KeyQueue, QueueKey},
	nrustes_config::NrustesConfig,
};

/// A `Context` which displays nothing.
/// Useful for tests and other headless needs.
pub struct VoidContext {
	height: u16,
	width: u16,

	/// Whether drawing operations are slow. Mocked value.
	drawing_is_slow: bool,

	/// Whether full draws have to be performed in order to display everything
	/// correctly. Mocked value.
	needs_to_redraw_buffer: bool,

	/// Whether graphics can be rendered. Mocked value.
	can_render_graphics: bool,

	key_queue: KeyQueue,

	nrustes_config: NrustesConfig,
}

impl VoidContext {
	pub fn new(height: u16, width: u16, nrustes_config: NrustesConfig) -> Self {
		Self {
			height,
			width,

			// TODO If all these attributes are `false`, then `Window` content
			//  doesn't get printed. Decorations do, though. Hmm, It should be
			//  possible for a `Context` which is fast, doesn't need to redraw
			//  the buffer and doesn't support graphics to exist...
			drawing_is_slow: false,
			needs_to_redraw_buffer: false,
			can_render_graphics: false,

			key_queue: KeyQueue::default(),

			nrustes_config,
		}
	}

	pub fn mock_drawing_is_slow(mut self) -> Self {
		self.drawing_is_slow = true;

		self
	}

	pub fn mock_needs_to_redraw_buffer(mut self) -> Self {
		self.needs_to_redraw_buffer = true;

		self
	}

	pub fn mock_can_render_graphics(mut self) -> Self {
		self.can_render_graphics = true;

		self
	}
}

impl Context for VoidContext {
	fn nrustes_config(&self) -> &NrustesConfig {
		&self.nrustes_config
	}

	fn present(&mut self) {}

	fn clear(&mut self) {}

	fn height(&self) -> u16 {
		self.height
	}

	fn width(&self) -> u16 {
		self.width
	}

	fn render_text(&mut self, _y: u16, _x: u16, _text: &str, _fg: RGB8, _bg: RGB8) {}

	fn render_char(&mut self, _y: u16, _x: u16, _text: char, _fg: RGB8, _bg: RGB8) {}

	fn clear_rect(&mut self, _y: u16, _x: u16, _height: u16, _width: u16) {}

	fn input_key_bypass_queue(&mut self) -> Key {
		unimplemented!(
			"VoidBackend does not have any logic to read user input - it has to be mocked."
		)
	}

	fn flush_events(&mut self) {}

	fn drawing_is_slow(&self) -> bool {
		self.drawing_is_slow
	}

	fn needs_to_redraw_buffer(&self) -> bool {
		self.needs_to_redraw_buffer
	}
}

impl QueueKey for VoidContext {
	fn key_queue_mut(&mut self) -> &mut KeyQueue {
		&mut self.key_queue
	}
}

impl RenderSprite for VoidContext {
	fn can_render_graphics(&self) -> bool {
		self.can_render_graphics
	}

	fn size_in_smallest_units(&self) -> (u16, u16) {
		if self.can_render_graphics {
			(
				self.height / self.nrustes_config.font_size.0,
				self.width / self.nrustes_config.font_size.1,
			)
		} else {
			(self.height, self.width)
		}
	}

	fn font_sprite_size(&self) -> (u16, u16) {
		if self.can_render_graphics {
			self.nrustes_config.font_size
		} else {
			(1, 1)
		}
	}

	fn graphics_sprite_size(&self) -> (u16, u16) {
		if self.can_render_graphics {
			self.nrustes_config.graphics_size
		} else {
			(1, 1)
		}
	}

	fn render_font_sprite(&mut self, _key: &(char, RGB8, RGB8), _y: u16, _x: u16) {}

	fn render_graphics_sprite(&mut self, _key: &String, _y: u16, _x: u16) {}
}
