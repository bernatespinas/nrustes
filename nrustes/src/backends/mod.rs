#[cfg(feature = "crossterm_backend")]
mod crossterm_backend;
#[cfg(feature = "crossterm_backend")]
pub use crossterm_backend::ContextCrossterm;

#[cfg(feature = "termion_backend")]
mod termion_backend;
#[cfg(feature = "termion_backend")]
pub use termion_backend::ContextTermion;

#[cfg(feature = "sdl2_backend")]
mod sdl2_backend;
#[cfg(feature = "sdl2_backend")]
pub use sdl2_backend::ContextSdl2;

mod void_backend;
pub use void_backend::VoidContext;
