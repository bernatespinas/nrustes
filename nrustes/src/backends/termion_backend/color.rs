use rgb::RGB8;

use crate::colors::ToNativeColor;

impl ToNativeColor<termion::color::Rgb> for RGB8 {
	fn to_native_color(self) -> termion::color::Rgb {
		termion::color::Rgb(self.r, self.g, self.b)
	}
}
