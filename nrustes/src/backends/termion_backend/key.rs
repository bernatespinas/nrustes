use termion::event;

use direction_simple::Direction;

use crate::key::Key;

impl From<event::Key> for Key {
	fn from(event_key: event::Key) -> Key {
		match event_key {
			event::Key::Up => Key::Movement(Direction::Up),
			event::Key::Down => Key::Movement(Direction::Down),
			event::Key::Right => Key::Movement(Direction::Right),
			event::Key::Left => Key::Movement(Direction::Left),

			event::Key::Char('\n') => Key::Select,
			event::Key::Char('\r') => Key::Select,
			event::Key::Char('\t') => Key::Tab,
			event::Key::Backspace => Key::Backspace,

			event::Key::Char(ch) => Key::Char(ch),
			event::Key::Esc => Key::Back,
			_ => Key::Unknown,
		}
	}
}
