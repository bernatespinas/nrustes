use std::io::Write;

use rgb::RGB8;

use termion::{
	input::TermRead,
	raw::{IntoRawMode, RawTerminal},
	style,
};

use crate::{
	colors::ToNativeColor,
	context::Context,
	get_terminal_height_width,
	key::Key,
	key_queue::{KeyQueue, QueueKey},
	nrustes_config::NrustesConfig,
};

mod key;

mod color;

mod render_sprite;

pub struct ContextTermion {
	height: u16,
	width: u16,

	stdout: RawTerminal<std::io::Stdout>,

	key_queue: KeyQueue,

	nrustes_config: NrustesConfig,
}

impl ContextTermion {
	pub fn new(nrustes_config: NrustesConfig) -> ContextTermion {
		// TODO Get them every time the game is run... The terminal might have been resized or whatever. I don't like this.
		let (height, width) = get_terminal_height_width();

		ContextTermion {
			height,
			width,

			stdout: std::io::stdout().into_raw_mode().unwrap(),

			key_queue: KeyQueue::default(),

			nrustes_config,
		}
	}
}

// I had to +1 x and y because ANSI thingies are one-based (they begin at 1, not at 0).
impl Context for ContextTermion {
	fn nrustes_config(&self) -> &NrustesConfig {
		&self.nrustes_config
	}

	fn present(&mut self) {
		self.stdout.flush().unwrap();
	}

	fn clear(&mut self) {
		write!(
			self.stdout,
			"{goto}{clear}",
			goto = termion::cursor::Goto(1, 1),
			clear = termion::clear::All
		)
		.unwrap();
	}

	fn height(&self) -> u16 {
		self.height
	}

	fn width(&self) -> u16 {
		self.width
	}

	fn render_text(&mut self, y: u16, x: u16, text: &str, fg: RGB8, bg: RGB8) {
		write!(
			self.stdout,
			"{goto}{fg}{bg}{text}{hide}{reset}",
			goto = termion::cursor::Goto(x + 1, y + 1),
			fg = termion::color::Fg::<termion::color::Rgb>(fg.to_native_color()),
			bg = termion::color::Bg::<termion::color::Rgb>(bg.to_native_color()),
			text = text,
			hide = termion::cursor::Hide,
			reset = style::Reset,
		)
		.unwrap();
	}

	fn render_char(&mut self, y: u16, x: u16, text: char, fg: RGB8, bg: RGB8) {
		write!(
			self.stdout,
			"{goto}{fg}{bg}{text}{hide}{reset}",
			goto = termion::cursor::Goto(x + 1, y + 1),
			fg = termion::color::Fg::<termion::color::Rgb>(fg.to_native_color()),
			bg = termion::color::Bg::<termion::color::Rgb>(bg.to_native_color()),
			text = text,
			hide = termion::cursor::Hide,
			reset = style::Reset,
		)
		.unwrap();
	}

	fn clear_rect(&mut self, y: u16, x: u16, height: u16, width: u16) {
		let erased_line = " ".repeat(width as usize);
		for i in 0..height {
			write!(
				self.stdout,
				"{goto}{text}{hide}",
				goto = termion::cursor::Goto(x + 1, y + i + 1),
				text = &erased_line,
				hide = termion::cursor::Hide,
			)
			.unwrap();
		}
	}

	fn input_key_bypass_queue(&mut self) -> Key {
		self.present();

		// let _stdout = std::io::stdout().into_raw_mode().unwrap();
		let stdin = std::io::stdin();

		let native_key = stdin.keys().next().unwrap().unwrap();

		Key::from(native_key)
	}

	fn flush_events(&mut self) {}

	fn drawing_is_slow(&self) -> bool {
		true
	}

	fn needs_to_redraw_buffer(&self) -> bool {
		false
	}
}

impl QueueKey for ContextTermion {
	fn key_queue_mut(&mut self) -> &mut KeyQueue {
		&mut self.key_queue
	}
}
