use crate::{
	container::Container, context::Context, darken::Darken, draw::Draw, handle_input::HandleInput,
	popup::ErrorPopup, prelude::Key, shortcuts::BindShortcut,
};

mod mono_view_holder;
pub use mono_view_holder::MonoViewHolder;

pub trait MonoView: MonoViewHolder + Draw + Darken + Sized {
	/// Represents the data which can be outputted by this view.
	type Output;

	/// Initializes the view so that everything is ready to be displayed right after being created.
	fn initialize(&mut self);

	fn draw_if_necessary(&mut self, context: &mut impl Context) {
		log::trace!(
			"{} needs_draw: {}",
			self.main_container().window().title(),
			self.main_container().needs_draw()
		);

		if self.needs_draw() {
			self.main_container_mut().update();

			self.draw(context);
		}
	}

	/// Processes a `Key`, handles any `Action` mapped to it and returns the `Output` if any.
	fn handle_key(&mut self, key: &Key, context: &mut impl Context) -> Option<Self::Output> {
		let shortcut_data: Option<&Self::Action> = self.main_container().shortcut_data(key);

		let action = shortcut_data.cloned()?;

		match self.handle_action(&action, context) {
			Some(Err(message)) => {
				ErrorPopup::centered_in_screen(message, context).show(self, context);

				None
			}
			Some(Ok(output)) => Some(output),
			None => None,
		}
	}

	/// Processes an `Action`. It might return the `Output`.
	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>>;

	fn handle_user_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		// TODO handle_input_and_redraw or just handle_input?
		self.main_container_mut().handle_input(context)
	}

	fn input_loop(&mut self, context: &mut impl Context) -> Option<Self::Output> {
		self.initialize();

		loop {
			self.draw_if_necessary(context);

			let key = match self.handle_user_input(context) {
				Ok(k) => k,
				Err(message) => {
					ErrorPopup::centered_in_screen(message, context).show(self, context);

					continue;
				}
			};

			match key.gamify() {
				Key::Back => return None,
				k => match self.handle_key(&k, context) {
					None => {}
					Some(output) => return Some(output),
				},
			}
		}
	}
}
