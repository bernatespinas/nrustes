use crate::{container::Container, handle_input::HandleInput, shortcuts::Shortcuts};

/// This trait contains the functionality needed by `DoubleView` which can be
/// derived with a macro.
pub trait MonoViewHolder {
	type Container1: Container + HandleInput;

	/// Represents the actions the user can trigger by pressing `Key`s assigned to shortcuts. Typically, an `enum`.
	type Action: Clone;

	fn main_container(&self) -> &Shortcuts<Self::Container1, Self::Action>;
	fn main_container_mut(&mut self) -> &mut Shortcuts<Self::Container1, Self::Action>;
}
