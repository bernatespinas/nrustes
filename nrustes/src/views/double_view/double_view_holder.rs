use crate::{container::Container, handle_input::HandleInput, shortcuts::Shortcuts};

use super::DoubleViewState;

/// This trait contains the functionality needed by `DoubleView` which can be
/// derived with a macro.
pub trait DoubleViewHolder {
	type Container1: Container + HandleInput + Into<Shortcuts<Self::Container1, Self::Action>>;
	type Container2: Container + HandleInput + Into<Shortcuts<Self::Container2, Self::Action>>;

	/// Represents the actions the player can trigger by pressing `Key`s assigned to shortcuts. Typically, an `enum`.
	type Action: Clone;

	fn view_state(&self) -> &DoubleViewState<Self::Container1, Self::Container2, Self::Action>;

	fn view_state_mut(
		&mut self,
	) -> &mut DoubleViewState<Self::Container1, Self::Container2, Self::Action>;

	fn main_container(&self) -> &Shortcuts<Self::Container1, Self::Action> {
		&self.view_state().main_container
	}

	fn main_container_mut(&mut self) -> &mut Shortcuts<Self::Container1, Self::Action> {
		&mut self.view_state_mut().main_container
	}

	fn secondary_container(&self) -> &Shortcuts<Self::Container2, Self::Action> {
		&self.view_state().secondary_container
	}

	fn secondary_container_mut(&mut self) -> &mut Shortcuts<Self::Container2, Self::Action> {
		&mut self.view_state_mut().secondary_container
	}
}
