use crate::{
	container::Container, context::Context, darken::Darken, draw::Draw, key::Key, popup::ErrorPopup,
};

mod double_view_holder;
pub use double_view_holder::DoubleViewHolder;

mod double_view_state;
pub use double_view_state::DoubleViewState;

pub mod view_presets;

pub trait DoubleView: DoubleViewHolder + Draw + Darken + Sized {
	/// Represents the data which can be outputted by this view.
	type Output;

	/// Initializes the view so that everything is ready to be displayed right after being created.
	fn initialize(&mut self);

	fn draw_if_necessary(&mut self, context: &mut impl Context) {
		log::trace!(
			"{} needs_draw: {}",
			self.main_container().window().title(),
			self.main_container().needs_draw()
		);

		log::trace!(
			"{} needs_draw: {}",
			self.secondary_container().window().title(),
			self.secondary_container().needs_draw()
		);

		if context.needs_to_redraw_buffer() {
			self.main_container_mut().update();
			self.secondary_container_mut().update();

			self.draw(context);
		} else {
			if self.main_container().needs_draw() {
				self.main_container_mut().update();

				self.main_container_mut().draw(context);
			}

			if self.secondary_container().needs_draw() {
				self.secondary_container_mut().update();

				self.secondary_container_mut().draw(context);
			}
		}
	}

	/// Processes a `Key`, handles any `Action` mapped to it and returns the `Output` if any.
	fn handle_key(&mut self, key: &Key, context: &mut impl Context) -> Option<Self::Output> {
		let shortcut_data: Option<&Self::Action> = self.view_state().shortcut_data(key);

		let action = shortcut_data.cloned()?;

		match self.handle_action(&action, context) {
			Some(Err(message)) => {
				ErrorPopup::centered_in_screen(message, context).show(self, context);

				None
			}
			Some(Ok(output)) => Some(output),
			None => None,
		}
	}

	/// Processes an `Action`. It might return the `Output`.
	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>>;

	fn input_loop(&mut self, context: &mut impl Context) -> Option<Self::Output> {
		self.initialize();

		loop {
			self.draw_if_necessary(context);

			let key = match self.view_state_mut().handle_input(context) {
				Ok(k) => k,
				Err(message) => {
					ErrorPopup::centered_in_screen(message, context).show(self, context);

					continue;
				}
			};

			match key.gamify() {
				Key::Back => return None,
				Key::Tab => {
					self.view_state_mut().change_current_container();

					self.schedule_draw();
				}
				k => match self.handle_key(&k, context) {
					None => {}
					Some(output) => return Some(output),
				},
			};
		}
	}
}
