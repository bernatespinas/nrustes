//! There are some types of views which are common: a `Scrollable` to the left and a `Window` to the right; or a `Scrollbar` instead of a `Window`...
//! These traits provide some helpful methods to make obtaining these `Container`s easier and clearer.
//! That way, instead of having to use `self.main_container()` or `self.secondary_container()`, which may look a bit opaque, you can use `self.scrollable()` and `self.window()`, for example.
//! These traits are implemented for the relevant types of `DoubleViewState` (the type which stores the state necessary for a view) and `DoubleViewHolder` (the type which represents the view itself). Depending on which type you call these methods on, you might get borrow-checker errors about lifetimes which might not make much sense. You can try calling these methods on the other type or add "redundant" `let`s.

mod scrollable_window_view;
pub use scrollable_window_view::ScrollableWindowView;

mod scrollable_scrollbar_view;
pub use scrollable_scrollbar_view::ScrollableScrollbarView;

mod form_window_view;
pub use form_window_view::FormWindowView;
