use std::fmt::Display;

use crate::{
	prelude::DoubleViewHolder, scrollable::Scrollable, scrollbar::Scrollbar, shortcuts::Shortcuts,
	views::double_view::DoubleViewState,
};

pub trait ScrollableScrollbarView<T: Display, U: Clone> {
	fn scrollable(&self) -> &Shortcuts<Scrollable<T>, U>;
	fn scrollable_mut(&mut self) -> &mut Shortcuts<Scrollable<T>, U>;

	fn scrollbar(&self) -> &Shortcuts<Scrollbar, U>;
	fn scrollbar_mut(&mut self) -> &mut Shortcuts<Scrollbar, U>;
}

impl<T: Display, U: Clone> ScrollableScrollbarView<T, U>
	for DoubleViewState<Scrollable<T>, Scrollbar, U>
{
	fn scrollable(&self) -> &Shortcuts<Scrollable<T>, U> {
		&self.main_container
	}

	fn scrollable_mut(&mut self) -> &mut Shortcuts<Scrollable<T>, U> {
		&mut self.main_container
	}

	fn scrollbar(&self) -> &Shortcuts<Scrollbar, U> {
		&self.secondary_container
	}

	fn scrollbar_mut(&mut self) -> &mut Shortcuts<Scrollbar, U> {
		&mut self.secondary_container
	}
}

impl<
		T: Display,
		U: Clone,
		V: DoubleViewHolder<Container1 = Scrollable<T>, Container2 = Scrollbar, Action = U>,
	> ScrollableScrollbarView<T, U> for V
{
	fn scrollable(&self) -> &Shortcuts<Scrollable<T>, U> {
		self.main_container()
	}

	fn scrollable_mut(&mut self) -> &mut Shortcuts<Scrollable<T>, U> {
		self.main_container_mut()
	}

	fn scrollbar(&self) -> &Shortcuts<Scrollbar, U> {
		self.secondary_container()
	}

	fn scrollbar_mut(&mut self) -> &mut Shortcuts<Scrollbar, U> {
		self.secondary_container_mut()
	}
}
