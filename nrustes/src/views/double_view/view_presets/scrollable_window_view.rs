use std::fmt::Display;

use crate::{
	prelude::{DoubleViewHolder, DoubleViewState},
	scrollable::Scrollable,
	shortcuts::Shortcuts,
	window::Window,
};

pub trait ScrollableWindowView<T: Display, U: Clone> {
	fn scrollable(&self) -> &Shortcuts<Scrollable<T>, U>;
	fn scrollable_mut(&mut self) -> &mut Shortcuts<Scrollable<T>, U>;

	fn window(&self) -> &Shortcuts<Window, U>;
	fn window_mut(&mut self) -> &mut Shortcuts<Window, U>;
}

impl<T: Display, U: Clone> ScrollableWindowView<T, U>
	for DoubleViewState<Scrollable<T>, Window, U>
{
	fn scrollable(&self) -> &Shortcuts<Scrollable<T>, U> {
		&self.main_container
	}

	fn scrollable_mut(&mut self) -> &mut Shortcuts<Scrollable<T>, U> {
		&mut self.main_container
	}

	fn window(&self) -> &Shortcuts<Window, U> {
		&self.secondary_container
	}

	fn window_mut(&mut self) -> &mut Shortcuts<Window, U> {
		&mut self.secondary_container
	}
}

impl<
		T: Display,
		U: Clone,
		V: DoubleViewHolder<Container1 = Scrollable<T>, Container2 = Window, Action = U>,
	> ScrollableWindowView<T, U> for V
{
	fn scrollable(&self) -> &Shortcuts<Scrollable<T>, U> {
		self.main_container()
	}

	fn scrollable_mut(&mut self) -> &mut Shortcuts<Scrollable<T>, U> {
		self.main_container_mut()
	}

	fn window(&self) -> &Shortcuts<Window, U> {
		self.secondary_container()
	}

	fn window_mut(&mut self) -> &mut Shortcuts<Window, U> {
		self.secondary_container_mut()
	}
}
