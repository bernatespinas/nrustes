use crate::{
	form::Form,
	prelude::{DoubleViewHolder, DoubleViewState},
	shortcuts::Shortcuts,
	window::Window,
};

pub trait FormWindowView<T: Clone> {
	fn form(&self) -> &Shortcuts<Form, T>;
	fn form_mut(&mut self) -> &mut Shortcuts<Form, T>;

	fn window(&self) -> &Shortcuts<Window, T>;
	fn window_mut(&mut self) -> &mut Shortcuts<Window, T>;
}

impl<T: Clone> FormWindowView<T> for DoubleViewState<Form, Window, T> {
	fn form(&self) -> &Shortcuts<Form, T> {
		&self.main_container
	}

	fn form_mut(&mut self) -> &mut Shortcuts<Form, T> {
		&mut self.main_container
	}

	fn window(&self) -> &Shortcuts<Window, T> {
		&self.secondary_container
	}

	fn window_mut(&mut self) -> &mut Shortcuts<Window, T> {
		&mut self.secondary_container
	}
}

impl<T: Clone, V: DoubleViewHolder<Container1 = Form, Container2 = Window, Action = T>>
	FormWindowView<T> for V
{
	fn form(&self) -> &Shortcuts<Form, T> {
		self.main_container()
	}

	fn form_mut(&mut self) -> &mut Shortcuts<Form, T> {
		self.main_container_mut()
	}

	fn window(&self) -> &Shortcuts<Window, T> {
		self.secondary_container()
	}

	fn window_mut(&mut self) -> &mut Shortcuts<Window, T> {
		self.secondary_container_mut()
	}
}
