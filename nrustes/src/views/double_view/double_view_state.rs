use crate::{
	container::Container,
	context::Context,
	darken::Darken,
	handle_input::HandleInput,
	key::Key,
	prelude::Draw,
	shortcuts::{BindShortcut, Shortcuts},
};

/// Holds the two `Container`s of a `DoubleView` and allows changing which one is the current one.
#[derive(Draw)]
pub struct DoubleViewState<C1: Container, C2: Container, A: Clone> {
	#[draw]
	pub main_container: Shortcuts<C1, A>,

	#[draw]
	pub secondary_container: Shortcuts<C2, A>,

	current_container: u8,
}

impl<C1, C2, A: Clone> DoubleViewState<C1, C2, A>
where
	C1: Container + HandleInput,
	C1: Into<Shortcuts<C1, A>>,
	C2: Container + HandleInput,
	C2: Into<Shortcuts<C2, A>>,
{
	pub fn new(mut main_container: C1, secondary_container: C2) -> Self {
		mark_container_as_current(&mut main_container);

		Self {
			main_container: main_container.into(),
			secondary_container: secondary_container.into(),

			current_container: 0,
		}
	}

	pub(crate) fn change_current_container(&mut self) {
		self.current_container += 1;

		if self.current_container > 1 {
			self.current_container = 0;
		}

		if self.current_container == 0 {
			mark_container_as_non_current(&mut self.secondary_container);
			mark_container_as_current(&mut self.main_container);
		} else {
			mark_container_as_non_current(&mut self.main_container);
			mark_container_as_current(&mut self.secondary_container);
		}
	}

	pub(crate) fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		if self.current_container == 0 {
			self.main_container.handle_input(context)
		} else {
			self.secondary_container.handle_input(context)
		}
	}

	/// Returns an `Option` value with the shortcut data associated to a key in the current container; `None` if there is none.
	pub(crate) fn shortcut_data(&self, key: &Key) -> Option<&A> {
		if self.current_container == 0 {
			self.main_container
				.shortcut_data(key)
				.or_else(|| self.secondary_container.unfocused_shortcut_data(key))
		} else {
			self.secondary_container
				.shortcut_data(key)
				.or_else(|| self.main_container.unfocused_shortcut_data(key))
		}
	}
}

fn mark_container_as_current(container: &mut impl Container) {
	let new_title = format!(">> {}", container.window().title());
	container.change_title(new_title);
}

fn mark_container_as_non_current(container: &mut impl Container) {
	let new_title = container.window().title()[3..].to_string();
	container.change_title(new_title);
}
