//! A `Container` which knows how to draw a partial or complete view of a grid-like structure.

use std::cmp::min;
use std::fmt::Debug;

use direction_simple::Direction;

use crate::container::Container;
use crate::context::Context;
use crate::darken::Darken;
use crate::prelude::Draw;
use crate::window::Window;

mod scroll_grid;
pub use scroll_grid::ScrollGrid;

mod draw_grid_cell;
pub use draw_grid_cell::DrawGridCell;

#[derive(Draw)]
pub struct Pad<T> {
	#[draw]
	window: Window,

	/// The coordinates of the top left corner.
	top_left: T,

	/// The coordinates of the bottom right corner.
	bottom_right: T,
}

impl<T> Container for Pad<T> {
	fn window(&self) -> &Window {
		&self.window
	}

	fn window_mut(&mut self) -> &mut Window {
		&mut self.window
	}

	fn update(&mut self) {}
}

impl<T: Debug + Copy + PartialEq> Pad<T> {
	pub fn new<S>(window: Window, top_left: T, scroll_grid: &S) -> Self
	where
		S: ScrollGrid<Position = T>,
	{
		let bottom_right = calculate_bot_right(&window, &top_left, scroll_grid);

		Self {
			window,
			top_left,
			bottom_right,
		}
	}

	pub fn top_left(&self) -> &T {
		&self.top_left
	}

	pub fn bottom_right(&self) -> &T {
		&self.bottom_right
	}

	/// Returns `true` if any of the `scroll_grid`'s dimensions (height or with) is smaller than the `Pad`'s.
	pub fn scroll_grid_is_smaller_than_view<S>(&self, scroll_grid: &S) -> bool
	where
		S: ScrollGrid<Position = T>,
	{
		scroll_grid.rows() < self.window.height() || scroll_grid.columns() < self.window.width()
	}

	pub fn set_top_left<S>(&mut self, top_left: T, scroll_grid: &S)
	where
		S: ScrollGrid<Position = T>,
	{
		self.top_left = top_left;

		self.bottom_right = calculate_bot_right(&self.window, &top_left, scroll_grid);
	}

	/// - `scroll_grid`: the grid-like structure which will be completely or partially drawn.
	/// - `cell_item_drawer`: knows how to render `scroll_grid` cells.
	/// - `aux_data`: auxiliary data which is necessary when drawing.
	pub fn draw<S, D, C, A>(
		&self,
		scroll_grid: &S,
		cell_item_drawer: &D,
		aux_data: &A,
		context: &mut impl Context,
	) where
		D: DrawGridCell<CellItem = C, AuxData = A>,
		S: ScrollGrid<Position = T, CellItem = C>,
	{
		let mut cursor = self.top_left;
		let mut cursor_first_column_in_row = self.top_left;

		for y in 0..min(self.window.height(), scroll_grid.rows()) {
			for x in 0..min(self.window.width(), scroll_grid.columns()) {
				self.draw_cell_item(
					scroll_grid,
					&cursor,
					cell_item_drawer,
					y,
					x,
					aux_data,
					context,
				);

				cursor = scroll_grid
					.apply_direction(&cursor, Direction::Right)
					.unwrap_or(cursor);
			}

			cursor_first_column_in_row = scroll_grid
				.apply_direction(&cursor_first_column_in_row, Direction::Down)
				.unwrap_or(cursor_first_column_in_row);

			cursor = cursor_first_column_in_row;
		}
	}

	pub fn draw_cell_item<S, D, C, A>(
		&self,
		scroll_grid: &S,
		position: &T,
		cell_item_drawer: &D,
		y: u16,
		x: u16,
		aux_data: &A,
		context: &mut impl Context,
	) where
		D: DrawGridCell<CellItem = C, AuxData = A>,
		S: ScrollGrid<Position = T, CellItem = C>,
	{
		let cell_item = scroll_grid.cell_item(position);

		cell_item_drawer.draw(self, y, x, cell_item, aux_data, context);
	}
}

fn calculate_bot_right<T, S>(window: &Window, top_left: &T, scroll_grid: &S) -> T
where
	T: Copy,
	S: ScrollGrid<Position = T>,
{
	let height = min(window.height(), scroll_grid.rows());
	let width = min(window.width(), scroll_grid.columns());

	let mut bot_right = *top_left;

	for _ in 0..height - 1 {
		bot_right = scroll_grid
			.apply_direction(&bot_right, Direction::Down)
			.unwrap();
	}

	for _ in 0..width - 1 {
		bot_right = scroll_grid
			.apply_direction(&bot_right, Direction::Right)
			.unwrap();
	}

	bot_right
}
