use crate::{container::Container, context::Context};

pub trait DrawGridCell {
	/// The items which will be drawn.
	type CellItem;

	/// Auxiliary data necessary when drawing.
	type AuxData;

	fn draw(
		&self,
		container: &impl Container,
		y: u16,
		x: u16,
		cell_item: &Self::CellItem,
		aux_data: &Self::AuxData,
		context: &mut impl Context,
	);
}
