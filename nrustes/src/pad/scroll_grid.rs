use direction_simple::Direction;

pub trait ScrollGrid {
	/// The type used to reference `CellItem`s in the grid.
	type Position;

	/// The items which will be drawn.
	type CellItem;

	/// Returns the amount of rows in the grid.
	fn rows(&self) -> u16;

	/// Returns the amount of columns in the grid.
	fn columns(&self) -> u16;

	/// Tries to move `position` in a `Direction` and returns the resulting
	/// `ZonePosition` if it was possible.
	fn apply_direction(
		&self,
		position: &Self::Position,
		direction: Direction,
	) -> Option<Self::Position>;

	fn position_to_screen_coordinates(
		&self,
		position: &Self::Position,
		top_left: &Self::Position,
	) -> Option<(u16, u16)>;

	/// Returns the item in the cell in the specified position.
	fn cell_item(&self, position: &Self::Position) -> &Self::CellItem;
}
