use direction_simple::Direction;

// TODO: enum Event {Key(Key), Alt(Key), Ctrl(Key), Mouse(??)}
/// A common set of keys for all the available backends.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Key {
	Movement(Direction),
	Alt(Direction),
	Select,
	Back,
	Backspace,
	Tab,
	Char(char),
	Unknown,
}

impl std::fmt::Display for Key {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Key::Movement(direction) => write!(f, "{direction:?}"),
			Key::Select => write!(f, "Select"),
			Key::Back => write!(f, "Back"),
			Key::Char(ch) => write!(f, "{ch}"),
			Key::Backspace => write!(f, "Backspace"),
			Key::Tab => write!(f, "Tab"),
			Key::Alt(_) => write!(f, "{self:?}"),
			Key::Unknown => write!(f, "Unknown"),
		}
	}
}

impl Key {
	pub fn gamify(self) -> Key {
		match self {
			Key::Char('8') => Key::Movement(Direction::Up),
			Key::Char('2') => Key::Movement(Direction::Down),
			Key::Char('6') => Key::Movement(Direction::Right),
			Key::Char('4') => Key::Movement(Direction::Left),
			Key::Char('1') => Key::Movement(Direction::DownLeft),
			Key::Char('3') => Key::Movement(Direction::DownRight),
			Key::Char('7') => Key::Movement(Direction::UpLeft),
			Key::Char('9') => Key::Movement(Direction::UpRight),

			Key::Char('5') => Key::Select,
			Key::Char('0') => Key::Back,

			key => key,
		}
	}
}
