use crate::{container::Container, context::Context, key::Key};

pub trait HandleInput: Container {
	/// Handles the user's input and returns a `Result` with the pressed `Key` if successful, or an error message otherwise.
	fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String>;

	/// Handles the user's input, performs a draw if necessary, and returns a `Result` with the pressed `Key` if successful, or an error message otherwise.
	fn handle_input_and_redraw(&mut self, context: &mut impl Context) -> Result<Key, String> {
		if self.needs_draw() || context.needs_to_redraw_buffer() {
			self.update();
			self.draw(context);
		} else {
			// TODO This is wrong, right? termion, crossterm -> unnecessary redraws?
			self.draw_content(context);
		}

		let key_result = self.handle_input(context);

		key_result
	}
}
