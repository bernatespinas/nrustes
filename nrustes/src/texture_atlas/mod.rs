use std::{collections::HashMap, hash::Hash};

mod texture_atlas_data;
use texture_atlas_data::{TextureAtlasData, TextureCoordinates};

mod texture_atlas_loader;
pub use texture_atlas_loader::{LoadTextureAtlas, TextureAtlasLoader};

pub struct TextureAtlas<Key: Eq + Hash, Texture> {
	data: HashMap<String, TextureAtlasData<Key, Texture>>,
	current_texture: String,
}

impl<Key: Eq + Hash, Texture> Default for TextureAtlas<Key, Texture> {
	fn default() -> Self {
		Self {
			current_texture: "".to_string(),
			data: HashMap::default(),
		}
	}
}

impl<Key: Eq + Hash, Texture> TextureAtlas<Key, Texture> {
	pub fn current_texture(&self) -> &Texture {
		&self.data.get(&self.current_texture).unwrap().texture
	}

	/// The size of each sprite.
	pub fn current_source_sprite_size(&self) -> (u16, u16) {
		self.data
			.get(&self.current_texture)
			.unwrap()
			.source_sprite_size
	}

	pub fn set_current_texture(&mut self, current: String) {
		self.current_texture = current;
	}

	pub fn get(&self, key: &Key) -> Option<&TextureCoordinates> {
		self.data
			.get(&self.current_texture)
			.and_then(|data| data.atlas.get(key))
	}

	pub fn add_texture(
		&mut self,
		texture_id: &str,
		texture: Texture,
		atlas: HashMap<Key, TextureCoordinates>,
		source_sprite_size: (u16, u16),
	) -> Result<(), String> {
		let texture_atlas_data = TextureAtlasData {
			texture,
			atlas,
			source_sprite_size,
		};

		self.data.insert(texture_id.to_string(), texture_atlas_data);

		Ok(())
	}
}
