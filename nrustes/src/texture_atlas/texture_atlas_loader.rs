use std::{collections::HashMap, error::Error, path::PathBuf};

use rgb::RGB8;

use crate::context::Context;

pub struct TextureAtlasLoader {
	resources_dir_path: PathBuf,
}

impl TextureAtlasLoader {
	pub fn new(resources_dir_path: PathBuf) -> Self {
		Self { resources_dir_path }
	}

	pub(crate) fn resource_path(&self, resource_file_name: &str) -> PathBuf {
		self.resources_dir_path.join(resource_file_name)
	}
}

pub trait LoadTextureAtlas<'ctx, CTX: Context, TextureCreator> {
	fn load_font(
		&self,
		tag: &str,
		texture_file_name: &str,
		atlas_file_name: &str,
		sprite_size: (u16, u16),
		context: &mut CTX,
		texture_creator: &'ctx TextureCreator,
	) -> Result<(), String>;

	fn load_graphics(
		&self,
		tag: &str,
		texture_file_name: &str,
		atlas_file_name: &str,
		sprite_size: (u16, u16),
		context: &mut CTX,
		texture_creator: &'ctx TextureCreator,
	) -> Result<(), String>;
}

impl TextureAtlasLoader {
	pub(crate) fn load_font_atlas(
		&self,
		atlas_file_name: &str,
	) -> Result<HashMap<(char, RGB8, RGB8), (usize, usize)>, Box<dyn Error>> {
		let atlas_path = self.resource_path(atlas_file_name);
		let atlas_content = std::fs::read_to_string(atlas_path)?;

		let atlas: HashMap<(char, (u8, u8, u8), (u8, u8, u8)), (usize, usize)> =
			ron::de::from_str(&atlas_content)?;

		// Turn `(u8, u8, u8)` into `RGB8`.
		let rgb8_atlas = atlas
			.into_iter()
			.map(|(key, value)| {
				(
					(
						key.0,
						RGB8::new(key.1 .0, key.1 .1, key.1 .2),
						RGB8::new(key.2 .0, key.2 .1, key.2 .2),
					),
					value,
				)
			})
			.collect::<HashMap<(char, RGB8, RGB8), (usize, usize)>>();

		Ok(rgb8_atlas)
	}

	pub(crate) fn load_graphics_atlas(
		&self,
		atlas_file_name: &str,
	) -> Result<HashMap<String, (usize, usize)>, Box<dyn Error>> {
		let atlas_path = self.resource_path(atlas_file_name);
		let atlas_content = std::fs::read_to_string(atlas_path)?;

		ron::de::from_str(&atlas_content).map_err(|error| error.into())
	}
}
