use std::collections::HashMap;
use std::hash::Hash;

pub type TextureCoordinates = (usize, usize);

pub struct TextureAtlasData<Key: Eq + Hash, Texture> {
	pub texture: Texture,
	pub atlas: HashMap<Key, TextureCoordinates>,

	/// The size of each sprite.
	pub source_sprite_size: (u16, u16),
}
