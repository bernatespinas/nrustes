use std::collections::HashMap;

mod code_and_web_texture_packer;
pub use code_and_web_texture_packer::CodeAndWebTexturePackerJson;

type TextureAtlas = HashMap<String, (usize, usize)>;