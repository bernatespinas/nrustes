use super::TextureAtlas;

/// Struct that represents the JSON data outputted by CodeAndWeb's TexturePacker.
#[derive(Serialize, Deserialize)]
pub struct CodeAndWebTexturePackerJson {
	frames: Vec<Sprite>,
}

impl From<CodeAndWebTexturePackerJson> for TextureAtlas {
	fn from(json: CodeAndWebTexturePackerJson) -> Self {
		let mut atlas = TextureAtlas::default();

		for sprite in json.frames {
			// TODO Use `File` to get the file's name without its extension.
			let mut filename = sprite.filename;
			filename.truncate(filename.len() - 4);

			log::trace!("truncated to {filename}");
			atlas.insert(filename, (sprite.frame.y as usize, sprite.frame.x as usize));
		}

		atlas
	}
}

#[derive(Serialize, Deserialize)]
struct Sprite {
	filename: String,
	frame: Rect,
	// spriteSourceSize: Rect,
	// sourceSize: Size,
}

#[derive(Serialize, Deserialize)]
struct Rect {
	x: u16,
	y: u16,
	w: u16,
	h: u16,
}

// #[derive(Serialize, Deserialize)]
// struct Size {
// 	w: u16,
// 	h: u16,
// }
