pub use rgb::RGB8;

pub trait ToNativeColor<T> {
	fn to_native_color(self) -> T;
}

// pub trait ColorPalette {
// 	fn color(&self, color: Color) -> RGB;
// }

pub const LIGHT_GREEN: RGB8 = RGB8 {
	r: 25,
	g: 255,
	b: 25,
};

pub const DARK_GREEN: RGB8 = RGB8 { r: 8, g: 82, b: 8 };

pub const LIGHT_BLUE: RGB8 = RGB8 {
	r: 25,
	g: 255,
	b: 255,
};

pub const DARK_BLUE: RGB8 = RGB8 {
	r: 25,
	g: 25,
	b: 255,
};

pub const LIGHT_GREY: RGB8 = RGB8 {
	r: 186,
	g: 186,
	b: 186,
};

pub const DARK_GREY: RGB8 = RGB8 {
	r: 115,
	g: 115,
	b: 115,
};

pub const BLACK: RGB8 = RGB8 { r: 0, g: 0, b: 0 };

pub const LIGHT_RED: RGB8 = RGB8 {
	r: 255,
	g: 153,
	b: 153,
};

pub const DARK_RED: RGB8 = RGB8 { r: 153, g: 0, b: 0 };
