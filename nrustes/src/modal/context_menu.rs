use std::fmt::Display;

use crate::{
	context::Context,
	draw::Draw,
	handle_input::HandleInput,
	key::Key,
	scrollable::Scrollable,
	window::{Window, WindowBuilder},
};

use super::Modal;

pub struct ContextMenu<Item: Display> {
	scrollable: Scrollable<Item>,
}

impl<Item: Display> ContextMenu<Item> {
	pub fn new(
		title: impl Display,
		items: Vec<Item>,
		on_window: &Window,
		context: &impl Context,
	) -> Self {
		let scrollable = WindowBuilder::modal_list_bottom(items.len(), on_window, context)
			.with_title(title.to_string())
			.scrollable_builder()
			.with_items(items)
			.build();

		ContextMenu { scrollable }
	}
}

impl<Item: Display> Modal for ContextMenu<Item> {
	type Output = usize;

	fn get_output(&mut self, view: &mut impl Draw, context: &mut impl Context) -> Option<usize> {
		loop {
			if context.needs_to_redraw_buffer() {
				view.draw(context);
				self.scrollable.draw(context);
			}

			match self.scrollable.handle_input_and_redraw(context).unwrap() {
				Key::Select => return self.scrollable.hovered_index(),
				Key::Back => return None,
				_ => {}
			};
		}
	}
}
