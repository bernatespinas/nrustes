use crate::{
	container::Container,
	context::Context,
	draw::Draw,
	key::Key,
	print::Print,
	window::{Window, WindowBuilder},
};

use super::Modal;

pub struct TextInputModal {
	window: Window,
}

impl TextInputModal {
	pub fn new(title: impl ToString, on_window: &Window, context: &impl Context) -> TextInputModal {
		let window = WindowBuilder::modal_bottom(5, on_window, context)
			.with_title(title.to_string())
			.build();

		TextInputModal { window }
	}
}

impl Modal for TextInputModal {
	type Output = String;

	fn get_output(&mut self, view: &mut impl Draw, context: &mut impl Context) -> Option<String> {
		let mut text = "".to_string();

		// The amount of characters from `text` which will not displayed.
		let mut offset = 0;

		if !context.needs_to_redraw_buffer() {
			self.window.draw_decorations(context);
		}

		loop {
			if context.needs_to_redraw_buffer() {
				view.draw(context);
				self.window.draw(context);
			} else {
				self.window.draw_content(context);
			}

			match context.input_key() {
				Key::Char(c) => {
					text.push(c);

					// If the text doesn't fit the window, skip displaying the first currently displayed character.
					if text.len() > self.window.width() as usize - 4 {
						offset += 1;
					}

					self.window.print_accent((2, 2), &text[offset..]);
				}
				Key::Backspace => {
					if !text.is_empty() {
						text.pop();

						// There is room to displayed one skipped character, if any exists.
						if offset > 0 {
							offset -= 1;
						}

						self.window.print_accent((2, 2), &text[offset..]);
					}
				}
				Key::Select => {
					if text.is_empty() {
						return None;
					} else {
						return Some(text);
					}
				}
				Key::Back => return None,
				_ => {}
			}
		}
	}
}
