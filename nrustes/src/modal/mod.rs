use crate::context::Context;
use crate::darken::Darken;
use crate::draw::Draw;

mod context_menu;
pub use context_menu::ContextMenu;

mod text_input_modal;
pub use text_input_modal::TextInputModal;

pub trait Modal {
	type Output;

	fn get_output(
		&mut self,
		view: &mut impl Draw,
		context: &mut impl Context,
	) -> Option<Self::Output>;

	fn show(
		&mut self,
		view: &mut (impl Draw + Darken),
		context: &mut impl Context,
	) -> Option<Self::Output> {
		view.darken();
		view.draw(context);

		let output = self.get_output(view, context);

		view.undarken();
		view.schedule_draw();

		output
	}
}
