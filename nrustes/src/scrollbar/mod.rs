use std::collections::HashMap;
use std::fmt::Display;

use direction_simple::Direction;
use rgb::RGB8;

use crate::{
	container::Container,
	context::Context,
	darken::Darken,
	handle_input::HandleInput,
	key::Key,
	prelude::Draw,
	print::Print,
	window::{DisplayItem, Window},
};

mod line_splitter;
use line_splitter::LineSplitter;

#[derive(Draw)]
pub struct Scrollbar {
	#[draw]
	window: Window,

	/// The maximum amount of lines which can be displayed on a page.
	lines_per_page: u16,

	/// The page which is visible. Its contents are in `self.window`.
	visible_page: u8,

	/// The page on which text is being printed. When that page is full, text will start being printed on the next page, and so on.
	current_page: u8,

	/// The line on which the next text will be printed.
	current_line: u16,

	/// The maximum length a line can have before being split.
	max_line_length: usize,

	// Pages and their content.
	pages: HashMap<u8, HashMap<(u16, u16), DisplayItem>>,
}

impl Scrollbar {
	pub fn new(window: Window) -> Scrollbar {
		// / 2 because I want to leave an empty line between each line.
		// - 5 to leave enough empty lines at the top and bottom to not have them to close to the `Window`'s title and bottom text.
		let lines_per_page = (window.height() - 5) / 2;

		let max_line_length = window.width() as usize - 4;

		let mut pages: HashMap<u8, HashMap<(u16, u16), DisplayItem>> = HashMap::with_capacity(1);
		pages.insert(0, HashMap::new());

		Scrollbar {
			window,
			lines_per_page,
			visible_page: 0,
			current_page: 0,
			current_line: 3,
			max_line_length,
			pages,
		}
	}

	/// Moves to the next available line, where the next printed text will be.
	/// If the current page is full, it creates a new one and switches to it.
	fn next_line(&mut self) {
		self.current_line += 2;
	}

	/// Adds a new page if the current one is full.
	fn add_new_page_if_necessary(&mut self) {
		if self.current_page_is_full() {
			self.current_line = 3;
			self.current_page += 1;

			self.pages.insert(self.current_page, HashMap::new());
		}
	}

	/// Returns `true` if the current page doesn't have space for more lines; `false` otherwise.
	fn current_page_is_full(&self) -> bool {
		let current_page = self.pages.get(&self.current_page).unwrap();

		current_page.len() >= self.lines_per_page as usize
	}

	/// Goes to the next page, if available.
	fn next_page(&mut self) {
		if self.pages.contains_key(&(self.visible_page + 1)) {
			self.visible_page += 1;

			self.schedule_draw();
		}
	}

	/// Goes to the previous page, if available.
	fn prev_page(&mut self) {
		if self.visible_page == 0 {
			return;
		}

		if self.pages.contains_key(&(self.visible_page - 1)) {
			self.visible_page -= 1;

			self.schedule_draw();
		}
	}

	/// Prints text in the current page.
	fn print_in_current_page(&mut self, x: u16, text: impl Display, fg: RGB8, bg: RGB8) {
		let current_page = self.pages.get_mut(&self.current_page).unwrap();
		let display_item = DisplayItem::new(text.to_string(), fg, bg);

		current_page.insert((self.current_line, x), display_item);
	}

	/// Adds an empty line.
	pub fn add_empty_line(&mut self) {
		self.next_line();
	}

	/// Prints many texts in a single line. Each text can have different
	/// colors. They are printed consecutively, with no spaces inserted
	/// between them, and they are not split into multiple lines if one line
	/// isn't enough.
	pub fn print_compound_text(&mut self, x: u16, compound_text: &[(impl Display, RGB8, RGB8)]) {
		self.add_new_page_if_necessary();

		let mut current_x = x;

		for (text, fg, bg) in compound_text {
			let text = text.to_string();

			log::trace!(
				"Adding {text} at ({}, {current_x}) to current page.",
				self.current_line
			);

			self.print_in_current_page(current_x, &text, *fg, *bg);

			current_x += text.len() as u16;
		}

		self.next_line();
	}
}

impl Container for Scrollbar {
	fn window(&self) -> &Window {
		&self.window
	}

	fn window_mut(&mut self) -> &mut Window {
		&mut self.window
	}

	fn update(&mut self) {
		self.window.delete_content();

		let visible_page = self.pages.get(&self.visible_page).unwrap();

		for ((y, x), display_item) in visible_page.iter() {
			self.window.print(
				(*y, *x),
				&display_item.text,
				display_item.fg,
				display_item.bg,
			);
		}

		if self.current_page > 0 {
			let additional_info = format!("{}/{}", self.visible_page + 1, self.pages.len());

			self.window.header.change_additional_info(additional_info);
		} else {
			self.window.header.change_additional_info("");
		}
	}

	fn delete_content(&mut self) {
		self.window.delete_content();

		self.current_page = 0;
		self.visible_page = 0;
		self.current_line = 3;

		self.pages.clear();
		self.pages.insert(0, HashMap::new());
	}
}

impl Print for Scrollbar {
	type Coordinates = u16;

	fn primary_color(&self) -> RGB8 {
		self.window.primary_color()
	}

	fn accent_color(&self) -> RGB8 {
		self.window.accent_color()
	}

	fn print(&mut self, x: u16, text: impl Display, fg: RGB8, bg: RGB8) {
		let text = text.to_string();

		if text.len() <= self.max_line_length {
			self.add_new_page_if_necessary();

			self.print_in_current_page(x, text, fg, bg);

			self.next_line();
		} else {
			let lines = LineSplitter::new(&text, self.max_line_length).split_text_in_lines();

			for line in lines {
				self.add_new_page_if_necessary();

				self.print_in_current_page(x, line, fg, bg);

				self.next_line();
			}
		}
	}
}

impl HandleInput for Scrollbar {
	fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		let key = context.input_key();

		match key {
			Key::Movement(Direction::Left) => {
				self.prev_page();
			}
			Key::Movement(Direction::Right) => {
				self.next_page();
			}
			_ => {}
		};

		Ok(key)
	}
}

impl From<Window> for Scrollbar {
	fn from(window: Window) -> Self {
		Self::new(window)
	}
}
