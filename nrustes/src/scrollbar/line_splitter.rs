use std::cmp::min;

pub struct LineSplitter<'a> {
	/// The line which might be split.
	text: &'a str,

	/// The maximum length a line can have.
	max_line_length: usize,

	/// The current start index.
	start: usize,

	/// The current end index.
	end: usize,

	/// The (start, end) index pairs which allow `line` to be printed in `max_line_length` chunks while taking some requirements into account (avoid splitting words, trim spaces at the start of each line...).
	start_end_pairs: Vec<(usize, usize)>,
}

impl<'a> LineSplitter<'a> {
	pub fn new(text: &'a str, max_line_length: usize) -> Self {
		Self {
			text,
			max_line_length,

			start: 0,
			end: max_line_length,

			start_end_pairs: Vec::new(),
		}
	}

	/// Returns an `Iterator` which visits all the lines the original text can be split into.
	/// Words are not split if possible - if a word would get cut between two lines, it will be fully printed in the second line (unless it's the only word in that line because it's a really long word).
	/// Trailing spaces at the start are omitted.
	pub fn split_text_in_lines(mut self) -> impl Iterator<Item = &'a str> {
		while !self.reached_end() {
			self.avoid_truncating_words_if_possible();

			self.skip_spaces_beginning();

			self.add_pair();

			self.update_cursor();
		}

		self.line_chunks()
	}

	/// Returns `true` if there is no more text in the current line to be processed; `false` otherwise.
	fn reached_end(&self) -> bool {
		self.start >= self.text.len()
	}

	/// Returns the line's chunk which is visible with the current start and end indexes.
	fn current_line_chunk(&self) -> &str {
		&self.text[self.start..self.end]
	}

	fn skip_spaces_beginning(&mut self) {
		let trimmed_left_text = self.current_line_chunk().trim_start();
		let trimmed_spaces_left = self.current_line_chunk().len() - trimmed_left_text.len();

		self.start += trimmed_spaces_left;

		if self.start >= self.end {
			self.end = min(self.start + self.max_line_length, self.text.len());

			self.skip_spaces_beginning();
		}
	}

	/// Commits the current pair of (start, end) indexes.
	fn add_pair(&mut self) {
		log::trace!("Final chunk:");
		log::trace!("\t{}", self.current_line_chunk());
		log::trace!("\t{}..{}", self.start, self.end);

		self.start_end_pairs.push((self.start, self.end));
	}

	/// Updates the current pair of (start, end) indexes to start working with the remaining line's text.
	fn update_cursor(&mut self) {
		self.start = self.end;

		self.end = min(self.end + self.max_line_length, self.text.len());
	}

	/// Returns `true` if the last character of the current line chunk is a space; `false` otherwise.
	fn last_char_is_space(&self) -> bool {
		let last_char = &self.text[self.end - 1..self.end];

		let last_char_is_space = last_char == " ";

		log::trace!("last_char_is_space: {last_char_is_space} ({last_char})");

		last_char_is_space
	}

	/// Returns `true` if the character after the last one of the current line chunk is a space; `false` otherwise.
	fn next_char_is_space(&self) -> bool {
		let next_char = &self.text[self.end..self.end + 1];

		let next_char_is_space = next_char == " ";

		log::trace!("next_char_is_space: {next_char_is_space} ({next_char})");

		next_char_is_space
	}

	/// Returns `true` if the current line chunk contains spaces; `false` otherwise.
	fn contains_spaces(&self) -> bool {
		self.current_line_chunk().contains(' ')
	}

	fn truncate_line_to_avoid_splitting_word(&mut self) {
		// A word has been split. No!
		let new_end = self.current_line_chunk().rfind(' ').unwrap();

		self.end = self.start + new_end;
	}

	fn avoid_truncating_words_if_possible(&mut self) {
		log::trace!("Potential chunk:");
		log::trace!("\t{}", self.current_line_chunk());
		log::trace!("\t{}..{}", self.start, self.end);

		let last_char_is_space = self.last_char_is_space();

		if !last_char_is_space && self.text.len() > self.end + 1 {
			let next_char_is_space = self.next_char_is_space();

			if !next_char_is_space {
				if self.contains_spaces() {
					self.truncate_line_to_avoid_splitting_word();
				}
			}
		}
	}

	/// Returns the line chunks obtained by splitting the line with the commited (start, end) index pairs.
	fn line_chunks(self) -> impl Iterator<Item = &'a str> {
		self.start_end_pairs
			.into_iter()
			.map(|(start, end)| &self.text[start..end])
	}
}
