use crate::context::Context;

pub trait Draw {
	fn draw(&mut self, context: &mut impl Context);

	/// Whether a draw needs to be performed.
	fn needs_draw(&self) -> bool;

	/// Schedules a draw so that it will be performed the next time user input has to be obtained.
	fn schedule_draw(&mut self);
}
