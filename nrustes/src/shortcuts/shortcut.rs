pub struct Shortcut<Data: Clone> {
	/// The text which will be shown to describe this shortcut, if any.
	pub(super) label: Option<String>,

	/// The data associated with this shortcut.
	pub(super) data: Data,

	/// Whether this shortcut is permanent. Permanent `Shortcut`s shouldn't be deleted.
	pub(super) permanent: bool,

	/// Whether this `Shortcut` can only be triggered when its view is focused.
	pub(super) requires_focus: bool,
}

impl<Data: Clone> Shortcut<Data> {
	pub fn new(data: Data) -> Self {
		Self {
			label: None,
			data,
			permanent: false,
			requires_focus: false,
		}
	}

	pub fn with_label(mut self, label: impl Into<String>) -> Self {
		self.label.replace(label.into());

		self
	}

	pub fn permanent(mut self) -> Self {
		self.permanent = true;

		self
	}

	pub fn requires_focus(mut self) -> Self {
		self.requires_focus = true;

		self
	}
}
