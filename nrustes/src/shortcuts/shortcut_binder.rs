use std::collections::HashMap;

use crate::key::Key;

use super::{shortcut::Shortcut, BindShortcut};

pub struct ShortcutBinder<Data: Clone> {
	shortcuts: HashMap<Key, Shortcut<Data>>,
}

impl<Data: Clone> Default for ShortcutBinder<Data> {
	fn default() -> Self {
		ShortcutBinder {
			shortcuts: HashMap::default(),
		}
	}
}

impl<Data: Clone> BindShortcut<Data> for ShortcutBinder<Data> {
	fn shortcut_binder(&self) -> &ShortcutBinder<Data> {
		self
	}

	fn shortcut_binder_mut(&mut self) -> &mut ShortcutBinder<Data> {
		self
	}

	fn bind_shortcut(&mut self, key: Key, shortcut: Shortcut<Data>) {
		self.shortcuts.insert(key, shortcut);
	}

	fn unbind_shortcut(&mut self, key: &Key) {
		self.shortcuts.remove(key);
	}

	fn shortcut_data(&self, key: &Key) -> Option<&Data> {
		self.shortcuts
			.get(key)
			.as_ref()
			.map(|shortcut| &shortcut.data)
	}

	fn unfocused_shortcut_data(&self, key: &Key) -> Option<&Data> {
		self.shortcuts.get(key).and_then(|shortcut| {
			if shortcut.requires_focus {
				None
			} else {
				Some(&shortcut.data)
			}
		})

		// let shortcut = self.shortcuts.get(key)?;

		// if shortcut.requires_focus {
		// 	None
		// } else {
		// 	Some(&shortcut.data)
		// }
	}

	fn clear_shortcuts(&mut self) {
		self.shortcuts.retain(|_key, shortcut| shortcut.permanent);
	}

	fn shortcuts_label(&self) -> String {
		let mut label: String = self
			.shortcuts
			.iter()
			.filter(|(_key, shortcut)| shortcut.label.is_some())
			.map(|(key, shortcut)| format!("{key} - {}  ", shortcut.label.as_ref().unwrap()))
			.collect();

		// Remove the last 2 spaces if there's at least one shortcut.
		if !label.is_empty() {
			label.truncate(label.len() - 2);
		}

		label
	}
}
