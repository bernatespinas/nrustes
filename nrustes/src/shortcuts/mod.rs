//! Wrap a `Container` into a `Shortcuts` to be able to bind shortcuts to it by mapping a `Key` to:
//! - A label, which is a `String` that describes the shortcut.
//! - `Data`, whis is info needed to complete the action invoked by the shortcut.

use std::fmt::Display;

use direction_simple::Direction;

use crate::prelude::*;

mod shortcut;
pub use shortcut::Shortcut;

mod shortcut_binder;
pub use shortcut_binder::ShortcutBinder;

mod bind_shortcut;
pub use bind_shortcut::BindShortcut;

// `Data` needs to implement `Clone` because if I rely on borrows it's easy for "View"s to run into borrowing issues.
#[derive(Draw)]
pub struct Shortcuts<C: Container, Data: Clone> {
	#[draw]
	container: C,

	shortcut_binder: ShortcutBinder<Data>,
}

impl<C: Container, Data: Clone> Shortcuts<C, Data> {
	fn new(container: C) -> Self {
		Shortcuts {
			container,
			shortcut_binder: ShortcutBinder::default(),
		}
	}

	pub fn container(&self) -> &C {
		&self.container
	}

	pub fn container_mut(&mut self) -> &mut C {
		&mut self.container
	}

	/// Sets the label which described the available, visible `Shortcut`s as the bottom text of the inner `Container`.
	fn set_shortcuts_label_as_bottom_text(&mut self) {
		let bottom_text = self.shortcut_binder.shortcuts_label();

		self.container.change_bottom_text(bottom_text);
	}
}

impl<C: Container, Data: Clone> Container for Shortcuts<C, Data> {
	fn window(&self) -> &Window {
		self.container.window()
	}

	fn window_mut(&mut self) -> &mut Window {
		self.container.window_mut()
	}

	fn update(&mut self) {
		self.container.update();

		self.set_shortcuts_label_as_bottom_text();
	}

	fn delete_content(&mut self) {
		self.container.delete_content();
	}
}

impl<P: Container + Print, Data: Clone> Print for Shortcuts<P, Data> {
	type Coordinates = P::Coordinates;

	fn primary_color(&self) -> RGB8 {
		self.container.primary_color()
	}

	fn accent_color(&self) -> RGB8 {
		self.container.accent_color()
	}

	fn print(&mut self, coords: Self::Coordinates, text: impl Display, fg: RGB8, bg: RGB8) {
		self.container.print(coords, text, fg, bg);
	}
}

impl<C: Container + HandleInput, Data: Clone> HandleInput for Shortcuts<C, Data> {
	fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		self.container.handle_input(context)
	}
}

impl<C: Container, Data: Clone> BindShortcut<Data> for Shortcuts<C, Data> {
	fn shortcut_binder(&self) -> &ShortcutBinder<Data> {
		&self.shortcut_binder
	}

	fn shortcut_binder_mut(&mut self) -> &mut ShortcutBinder<Data> {
		&mut self.shortcut_binder
	}

	fn bind_shortcut(&mut self, key: Key, shortcut: Shortcut<Data>) {
		self.shortcut_binder.bind_shortcut(key, shortcut);

		self.set_shortcuts_label_as_bottom_text();
	}

	fn unbind_shortcut(&mut self, key: &Key) {
		self.shortcut_binder.unbind_shortcut(key);

		self.set_shortcuts_label_as_bottom_text();
	}

	fn clear_shortcuts(&mut self) {
		self.shortcut_binder.clear_shortcuts();

		self.set_shortcuts_label_as_bottom_text();
	}
}

impl<Data: Clone> From<Window> for Shortcuts<Window, Data> {
	fn from(window: Window) -> Self {
		Shortcuts::new(window)
	}
}

impl<T: Display, Data: Clone> From<Scrollable<T>> for Shortcuts<Scrollable<T>, Data> {
	fn from(scrollable: Scrollable<T>) -> Self {
		Shortcuts::new(scrollable)
	}
}

impl<Data: Clone> From<Scrollbar> for Shortcuts<Scrollbar, Data> {
	fn from(scrollbar: Scrollbar) -> Self {
		Shortcuts::new(scrollbar)
	}
}

impl<Data: Clone> From<Form> for Shortcuts<Form, Data> {
	fn from(form: Form) -> Self {
		Shortcuts::new(form)
	}
}

impl<Item: Display, Data: Clone> Shortcuts<Scrollable<Item>, Data> {
	pub fn bind_movement_shortcuts(&mut self, action: Data) {
		self.shortcut_binder.bind_shortcut(
			Key::Movement(Direction::Up),
			Shortcut::new(action.clone()).permanent().requires_focus(),
		);

		self.shortcut_binder.bind_shortcut(
			Key::Movement(Direction::Down),
			Shortcut::new(action.clone()).permanent().requires_focus(),
		);

		self.shortcut_binder.bind_shortcut(
			Key::Movement(Direction::Left),
			Shortcut::new(action.clone()).permanent().requires_focus(),
		);

		self.shortcut_binder.bind_shortcut(
			Key::Movement(Direction::Right),
			Shortcut::new(action).permanent().requires_focus(),
		);
	}
}

impl<Data: Clone> Shortcuts<Form, Data> {
	pub fn bind_movement_shortcuts(&mut self, action: Data) {
		self.shortcut_binder.bind_shortcut(
			Key::Movement(Direction::Up),
			Shortcut::new(action.clone()).permanent().requires_focus(),
		);

		self.shortcut_binder.bind_shortcut(
			Key::Movement(Direction::Down),
			Shortcut::new(action.clone()).permanent().requires_focus(),
		);
	}
}
