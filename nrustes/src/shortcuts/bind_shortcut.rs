use crate::key::Key;

use super::{shortcut::Shortcut, shortcut_binder::ShortcutBinder};

pub trait BindShortcut<Data: Clone> {
	fn shortcut_binder(&self) -> &ShortcutBinder<Data>;
	fn shortcut_binder_mut(&mut self) -> &mut ShortcutBinder<Data>;

	/// Binds a `Shortcut` to a `Key`.
	fn bind_shortcut(&mut self, key: Key, shortcut: Shortcut<Data>) {
		self.shortcut_binder_mut().bind_shortcut(key, shortcut);
	}

	/// Removes any `Shortcut` which is bound to `key`.
	fn unbind_shortcut(&mut self, key: &Key) {
		self.shortcut_binder_mut().unbind_shortcut(key);
	}

	/// Unbinds every non-permanent `Shortcut`.
	fn clear_shortcuts(&mut self) {
		self.shortcut_binder_mut().clear_shortcuts();
	}

	/// Returns the data associated to any `Shortcut` bound to `key`; `None` if there is none.
	fn shortcut_data(&self, key: &Key) -> Option<&Data> {
		self.shortcut_binder().shortcut_data(key)
	}

	/// Returns the data associated to any `Shortcut` bound to `key`; `None` if there is none. Only `Shortcut`s which don't require its `Container` to be focused are taken into account.
	fn unfocused_shortcut_data(&self, key: &Key) -> Option<&Data> {
		self.shortcut_binder().unfocused_shortcut_data(key)
	}

	/// Returns the text which describes the relevant, bound `Shortcut`s.
	fn shortcuts_label(&self) -> String {
		self.shortcut_binder().shortcuts_label()
	}
}
