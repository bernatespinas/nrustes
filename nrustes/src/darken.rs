use crate::prelude::Draw;

pub trait Darken: Draw {
	fn set_darkened(&mut self, darkened: bool);

	fn darken(&mut self) {
		self.set_darkened(true);

		self.schedule_draw();
	}

	fn undarken(&mut self) {
		self.set_darkened(false);

		self.schedule_draw();
	}
}
