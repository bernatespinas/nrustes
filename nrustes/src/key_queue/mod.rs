use std::collections::VecDeque;

use crate::key::Key;

mod queue_key;
pub use queue_key::QueueKey;

#[derive(Default)]
pub struct KeyQueue {
	queue: VecDeque<Key>,
}

impl KeyQueue {
	/// Adds a `Key` to the end of the queue.
	pub fn add_key(&mut self, key: Key) {
		self.queue.push_back(key);
	}

	/// Returns the first `Key` in the queue, if any.
	pub fn pop_front(&mut self) -> Option<Key> {
		self.queue.pop_front()
	}
}
