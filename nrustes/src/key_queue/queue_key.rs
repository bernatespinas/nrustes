use super::KeyQueue;

pub trait QueueKey {
	fn key_queue_mut(&mut self) -> &mut KeyQueue;
}
