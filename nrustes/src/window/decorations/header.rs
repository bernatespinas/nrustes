use std::fmt::Display;

use crate::colors::{BLACK, DARK_GREY, LIGHT_GREY};
use crate::context::Context;
use crate::window::Window;

use super::{draw_header_footer, DecorateWindow};

pub struct Header {
	/// The main title of the item which has this `Header`.
	title: String,

	/// Whether the title should be centered.
	centered: bool,

	/// Additional info which will be
	additional_info: String,
}

impl Header {
	pub fn new(title: impl Display) -> Header {
		Header {
			title: title.to_string(),
			centered: false,

			additional_info: "".to_string(),
		}
	}

	pub fn centered(&mut self) {
		self.centered = true;
	}

	pub fn title(&self) -> &str {
		&self.title
	}

	pub fn change_title(&mut self, title: impl Display) {
		self.title = title.to_string();
	}

	pub fn change_additional_info(&mut self, additional_info: impl Display) {
		self.additional_info = additional_info.to_string();
	}
}

impl DecorateWindow for Header {
	fn decorate(&self, window: &Window, context: &mut impl Context) {
		let (primary_color, accent_color) = if window.darkened {
			(DARK_GREY, LIGHT_GREY)
		} else {
			(window.primary_color, window.accent_color)
		};

		let title_x = if self.centered {
			window.width / 2 - self.title.len() as u16 / 2
		} else {
			2
		};

		let additional_info_x = window.width - 2 - self.additional_info.len() as u16;

		draw_header_footer(
			window,
			0,
			vec![
				(title_x, &self.title),
				(additional_info_x, &self.additional_info),
			],
			primary_color,
			accent_color,
			context,
		);

		let (top_left, top_right) = {
			let config = context.nrustes_config();

			(config.top_left, config.top_right)
		};

		context.render_char_container(window, 0, 0, top_left, primary_color, BLACK);

		context.render_char_container(window, 0, window.width - 1, top_right, primary_color, BLACK);
	}
}
