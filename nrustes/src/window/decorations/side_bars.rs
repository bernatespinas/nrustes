use crate::colors::{BLACK, DARK_GREY};
use crate::context::Context;
use crate::window::Window;

use super::DecorateWindow;

#[derive(Default)]
pub struct SideBars {}

impl DecorateWindow for SideBars {
	fn decorate(&self, window: &Window, context: &mut impl Context) {
		let primary_color = if window.darkened {
			DARK_GREY
		} else {
			window.primary_color
		};

		context.render_line_vertical(
			window.y + 1,
			window.x,
			window.height - 2,
			primary_color,
			BLACK,
		);

		context.render_line_vertical(
			window.y + 1,
			window.x + window.width - 1,
			window.height - 2,
			primary_color,
			BLACK,
		);
	}
}
