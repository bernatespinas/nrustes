use rgb::RGB8;

use crate::colors::BLACK;
use crate::context::Context;
use crate::window::Window;

use super::display_item_drawer::draw_empty_line_portion;

mod header;
pub use header::Header;

mod footer;
pub use footer::Footer;

mod side_bars;
pub use side_bars::SideBars;

pub trait DecorateWindow {
	fn decorate(&self, window: &Window, context: &mut impl Context);
}

fn draw_header_footer(
	window: &Window,
	line: u16,
	items: Vec<(u16, &str)>,
	primary_color: RGB8,
	accent_color: RGB8,
	context: &mut impl Context,
) {
	let mut x = 1;
	let erase_string = &window.header_footer_empty_line;

	for (text_x, text) in items {
		draw_empty_line_portion(
			window,
			line,
			x,
			text_x - x,
			erase_string,
			primary_color,
			context,
		);

		context.render_text_container(window, line, text_x, text, accent_color, BLACK);

		x = text_x + text.len() as u16;
	}

	draw_empty_line_portion(
		window,
		line,
		x,
		window.width - 1 - x,
		erase_string,
		primary_color,
		context,
	);
}
