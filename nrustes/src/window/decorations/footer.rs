use std::fmt::Display;

use crate::colors::{BLACK, DARK_GREY, LIGHT_GREY};
use crate::context::Context;
use crate::window::Window;

use super::{draw_header_footer, DecorateWindow};

pub struct Footer {
	bottom_text: String,

	/// Whether the bottom text should be centered.
	centered: bool,
}

impl Footer {
	pub fn new(bottom_text: impl Display) -> Footer {
		Footer {
			bottom_text: bottom_text.to_string(),
			centered: false,
		}
	}

	pub fn centered(&mut self) {
		self.centered = true;
	}

	pub fn change_bottom_text(&mut self, bottom_text: impl Display) {
		self.bottom_text = bottom_text.to_string();
	}
}

impl DecorateWindow for Footer {
	fn decorate(&self, window: &Window, context: &mut impl Context) {
		let (primary_color, accent_color) = if window.darkened {
			(DARK_GREY, LIGHT_GREY)
		} else {
			(window.primary_color, window.accent_color)
		};

		draw_header_footer(
			window,
			window.height - 1,
			vec![(2, &self.bottom_text)],
			primary_color,
			accent_color,
			context,
		);

		let bottom_left = context.nrustes_config().bottom_left;
		let bottom_right = context.nrustes_config().bottom_right;

		context.render_char_container(
			window,
			window.height - 1,
			0,
			bottom_left,
			primary_color,
			BLACK,
		);

		context.render_char_container(
			window,
			window.height - 1,
			window.width - 1,
			bottom_right,
			primary_color,
			BLACK,
		);
	}
}
