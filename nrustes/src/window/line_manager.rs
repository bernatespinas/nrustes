use std::collections::HashMap;

use rgb::RGB8;

use super::DisplayItem;

#[derive(Default)]
pub struct LineManager {
	lines: HashMap<u16, HashMap<u16, DisplayItem>>,

	sorted_line_coordinates: HashMap<u16, Vec<u16>>,
}

impl LineManager {
	pub fn insert_display_item(&mut self, line: u16, x: u16, text: String, fg: RGB8, bg: RGB8) {
		self.lines.entry(line).or_insert_with(HashMap::new);

		let display_item = DisplayItem::new(text, fg, bg);

		self.lines.get_mut(&line).unwrap().insert(x, display_item);

		self.sorted_line_coordinates.remove(&line);
	}

	pub fn display_item(&self, line: u16, x: u16) -> Option<&DisplayItem> {
		self.lines
			.get(&line)
			.and_then(|display_items| display_items.get(&x))
	}

	pub fn line_display_items(&self, line: u16) -> &HashMap<u16, DisplayItem> {
		self.lines.get(&line).unwrap()
	}

	pub fn display_item_mut(&mut self, line: u16, x: u16) -> Option<&mut DisplayItem> {
		self.lines
			.get_mut(&line)
			.and_then(|display_items| display_items.get_mut(&x))
	}

	pub fn lines(&self) -> impl Iterator<Item = u16> + '_ {
		self.lines.keys().copied()
	}

	pub fn lines_not_present_in<'a>(
		&'a self,
		other: &'a LineManager,
	) -> impl Iterator<Item = u16> + 'a {
		self.lines
			.keys()
			.filter(move |line| !other.lines.contains_key(line))
			.copied()
	}

	pub fn lines_also_present_in<'a>(
		&'a self,
		other: &'a LineManager,
	) -> impl Iterator<Item = u16> + 'a {
		self.lines
			.keys()
			.filter(move |line| other.lines.contains_key(line))
			.copied()
	}

	pub fn sorted_coordinates_in_line(&self, line: u16) -> Option<impl Iterator<Item = &u16>> {
		self.sorted_line_coordinates
			.get(&line)
			.map(|sorted_coordinates| sorted_coordinates.iter())
	}

	pub fn sort_display_items(&mut self) {
		for (line, display_items) in &self.lines {
			if self.sorted_line_coordinates.contains_key(line) {
				continue;
			}

			let mut sorted_coordinates_in_line: Vec<u16> = display_items.keys().copied().collect();
			sorted_coordinates_in_line.sort();

			self.sorted_line_coordinates
				.insert(*line, sorted_coordinates_in_line);
		}
	}

	#[cfg(test)]
	pub fn display_items(&self) -> HashMap<(u16, u16), &DisplayItem> {
		self.lines
			.iter()
			.flat_map(|(line, line_display_items)| {
				line_display_items
					.iter()
					.map(|(x, display_item)| ((*line, *x), display_item))
			})
			.collect()
	}
}
