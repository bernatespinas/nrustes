use std::fmt::Display;

use rgb::RGB8;

use crate::container::{BuildContainer, Container};
use crate::context::Context;
use crate::scrollable::ScrollableBuilder;
use crate::scrollbar::Scrollbar;
use crate::shortcuts::Shortcuts;
use crate::window::Window;

use super::decorations::{Footer, Header, SideBars};
use super::display_item_drawer::DELETED_LINE_ERASE_CHAR;
use super::line_manager::LineManager;

/// Relative values to give to a `Window`'s height or `y` position.
pub enum RelativeY {
	Value(u16),
	FromTop(u16),
	FromBot(u16),
	Fraction(u16),
	/// Maximum height or y = 0.
	ZeroOrMax,
}

/// Relative values to give to a `Window`'s width or `x` position.
pub enum RelativeX {
	Value(u16),
	FromLeft(u16),
	FromRight(u16),
	Fraction(u16),
	/// Maximum width or x = 0.
	ZeroOrMax,
}

pub struct WindowBuilder {
	window: Window,
}

impl WindowBuilder {
	pub fn new(y: u16, x: u16, height: u16, width: u16, context: &impl Context) -> WindowBuilder {
		let window = Window {
			y,
			x,
			height,
			width,

			header: Header::new(""),
			footer: Footer::new(""),
			side_bars: SideBars::default(),

			primary_color: context.primary_color(),
			accent_color: context.accent_color(),

			darkened: false,

			line_manager: LineManager::default(),
			deleted_content_line_manager: LineManager::default(),

			empty_line: DELETED_LINE_ERASE_CHAR
				.to_string()
				.repeat(width as usize - 2),

			header_footer_empty_line: context
				.nrustes_config()
				.horizontal
				.to_string()
				.repeat(width as usize - 2),

			needs_draw: true,
		};

		WindowBuilder { window }
	}

	pub fn with_title(mut self, title: impl Display) -> Self {
		self.window.header.change_title(title);

		self
	}

	pub fn with_centered_title(mut self, title: impl Display) -> Self {
		self.window.header.centered();

		self.with_title(title)
	}

	pub fn with_bottom_text(mut self, bottom_text: impl Display) -> Self {
		self.window.footer.change_bottom_text(bottom_text);

		self
	}

	pub fn with_centered_bottom_text(mut self, bottom_text: impl Display) -> Self {
		self.window.footer.centered();

		self.with_bottom_text(bottom_text)
	}

	pub fn with_colors(mut self, primary_color: RGB8, accent_color: RGB8) -> Self {
		self.window.change_colors(primary_color, accent_color);

		self
	}

	/// Creates a Window that takes up all the space in the program's window.
	pub fn full(title: impl Display, context: &impl Context) -> Self {
		WindowBuilder::new(0, 0, context.height(), context.width(), context)
			.with_title(title.to_string())
	}

	/// Creates a `Window` at the left half of the program's window.
	pub fn left(title: impl Display, context: &impl Context) -> Self {
		WindowBuilder::new(0, 0, context.height(), context.width() / 2, context)
			.with_title(title.to_string())
	}

	/// Creates a `Window` at the right half of the program's window.
	pub fn right(title: impl Display, context: &impl Context) -> Self {
		let width = if context.width() % 2 == 0 {
			context.width() / 2
		} else {
			context.width() / 2 + 1
		};

		WindowBuilder::new(0, context.width() / 2, context.height(), width, context)
			.with_title(title.to_string())
	}

	/// Creates a `Window` centered in the program's window.
	pub fn centered(height: u16, width: u16, context: &impl Context) -> Self {
		let y: u16 = (context.height() - height) / 2;
		let x: u16 = (context.width() - width) / 2;

		WindowBuilder::new(y, x, height, width, context)
	}

	/// Creates a `Window` centered in another `Container`.
	pub fn centered_in(
		height: u16,
		width: u16,
		container: &impl Container,
		context: &impl Context,
	) -> Self {
		let y: u16 = container.y() + (container.height() - height) / 2;
		let x: u16 = container.x() + (container.width() - width) / 2;

		WindowBuilder::new(y, x, height, width, context)
	}

	/// Creates a modal `Window` at the bottom of another `Container`.
	pub fn modal_bottom(height: u16, container: &impl Container, context: &impl Context) -> Self {
		WindowBuilder::new(
			container.height() - height,
			0,
			height,
			container.width(),
			context,
		)
	}

	/// Creates a modal `Window` at the bottom of another `Container`. The `Window` will have enough space to comfortably fit `$items_amount` lines.
	pub fn modal_list_bottom(
		items_amount: usize,
		container: &impl Container,
		context: &impl Context,
	) -> Self {
		let height = items_amount as u16 * 2 + 5;
		WindowBuilder::modal_bottom(height, container, context)
	}

	pub fn manual(
		ry: RelativeY,
		rx: RelativeX,
		rh: RelativeY,
		rw: RelativeX,
		context: &impl Context,
	) -> Self {
		let y = match ry {
			RelativeY::Value(yy) => yy,
			RelativeY::FromTop(yy) => yy,
			RelativeY::FromBot(yy) => context.height() - yy,
			RelativeY::Fraction(yy) => context.height() / yy,
			RelativeY::ZeroOrMax => 0,
		};

		let x = match rx {
			RelativeX::Value(xx) => xx,
			RelativeX::FromLeft(xx) => xx,
			RelativeX::FromRight(xx) => context.width() - xx,
			RelativeX::Fraction(xx) => context.width() / xx,
			RelativeX::ZeroOrMax => 0,
		};

		let height = match rh {
			RelativeY::Value(hh) => hh,
			RelativeY::FromTop(hh) => hh,
			RelativeY::FromBot(hh) => context.height() - hh - y,
			RelativeY::Fraction(hh) => context.height() / hh,
			RelativeY::ZeroOrMax => context.height(),
		};

		let width = match rw {
			RelativeX::Value(ww) => ww,
			RelativeX::FromLeft(ww) => ww,
			RelativeX::FromRight(ww) => context.width() - ww - x,
			RelativeX::Fraction(ww) => context.width() / ww,
			RelativeX::ZeroOrMax => context.width(),
		};

		WindowBuilder::new(y, x, height, width, context)
	}

	pub fn scrollable_builder<Item: Display>(self) -> ScrollableBuilder<Item> {
		ScrollableBuilder::new(self.window)
	}

	pub fn build<T>(self) -> T
	where
		T: Container,
		WindowBuilder: BuildContainer<T>,
	{
		self.build_container()
	}
}

impl BuildContainer<Window> for WindowBuilder {
	fn build_container(self) -> Window {
		self.window
	}
}

impl<C, T> BuildContainer<Shortcuts<C, T>> for WindowBuilder
where
	C: From<Window>,
	C: Container,
	C: Into<Shortcuts<C, T>>,
	T: Clone,
{
	fn build_container(self) -> Shortcuts<C, T> {
		C::from(self.window).into()
	}
}

impl BuildContainer<Scrollbar> for WindowBuilder {
	fn build_container(self) -> Scrollbar {
		Scrollbar::new(self.window)
	}
}
