use std::fmt::Display;

use rgb::RGB8;

use crate::{
	container::Container, context::Context, darken::Darken, draw::Draw, handle_input::HandleInput,
	key::Key, print::Print,
};

mod window_builder;
pub use window_builder::{RelativeX, RelativeY, WindowBuilder};

mod decorations;
use decorations::{DecorateWindow, Footer, Header, SideBars};

mod display_item;
pub use display_item::DisplayItem;

mod display_item_drawer;
use display_item_drawer::{DrawLines, WindowContentDrawer};

mod line_manager;
use line_manager::LineManager;

#[cfg(test)]
mod tests;

/// A delimited zone of the terminal that works like a window.
pub struct Window {
	/// The y coordinats of the top left corner of the `Window` in the program's window.
	y: u16,

	/// The x coordinats of the top left corner of the `Window` in the program's window.
	x: u16,

	height: u16,
	width: u16,

	pub header: Header,
	footer: Footer,
	side_bars: SideBars,

	primary_color: RGB8,
	accent_color: RGB8,

	/// Whether the elements of this `Window` are darkened.
	darkened: bool,

	line_manager: LineManager,
	deleted_content_line_manager: LineManager,

	/// A `String` which consists only of spaces and is as wide as the area in which text can be printed in the content's area.
	/// It's used to clear the contents of the `Window`, and it's stored here to avoid creating it frequently.
	empty_line: String,

	/// A `String` which consists only of horizontal lines and is as wide as the area in the header and footer in which text can be printed.
	/// It's used to clear the contents of the header and footer, and it's stored here to avoid creating it frequently.
	header_footer_empty_line: String,

	/// Whether the whole `Window` needs to be drawn.
	needs_draw: bool,
}

impl Window {
	pub fn title(&self) -> &str {
		self.header.title()
	}

	pub(crate) fn display_item(&self, (y, x): (u16, u16)) -> Option<&DisplayItem> {
		self.line_manager.display_item(y, x)
	}

	pub(crate) fn display_item_mut(&mut self, (y, x): &(u16, u16)) -> Option<&mut DisplayItem> {
		self.line_manager.display_item_mut(*y, *x)
	}

	fn change_colors(&mut self, primary_color: RGB8, accent_color: RGB8) {
		self.primary_color = primary_color;
		self.accent_color = accent_color;
	}

	fn crop_text_if_necessary(&self, x: u16, text: String) -> String {
		let available_width = self.width as usize - 2 - x as usize;

		if text.len() > available_width {
			format!("{}...", &text[0..available_width - 3])
		} else {
			text
		}
	}

	#[cfg(test)]
	pub fn display_items(&self) -> std::collections::HashMap<(u16, u16), &DisplayItem> {
		self.line_manager.display_items()
	}
}

impl Container for Window {
	fn window(&self) -> &Window {
		self
	}

	fn window_mut(&mut self) -> &mut Window {
		self
	}

	fn y(&self) -> u16 {
		self.y
	}

	fn x(&self) -> u16 {
		self.x
	}

	fn height(&self) -> u16 {
		self.height
	}

	fn width(&self) -> u16 {
		self.width
	}

	fn change_title(&mut self, title: impl Display) {
		self.header.change_title(title);
	}

	fn change_bottom_text(&mut self, bottom_text: impl Display) {
		self.footer.change_bottom_text(bottom_text);
	}

	fn update(&mut self) {}

	fn draw_decorations(&self, context: &mut impl Context) {
		self.header.decorate(self, context);
		self.footer.decorate(self, context);

		self.side_bars.decorate(self, context);
	}

	fn draw_content(&mut self, context: &mut impl Context) {
		if context.drawing_is_slow() {
			self.line_manager.sort_display_items();
		} else {
			self.erase_content(context);
		}

		WindowContentDrawer::draw_lines(self, context);

		if context.drawing_is_slow() {
			self.deleted_content_line_manager = LineManager::default();
		}
	}

	fn erase_all(&self, context: &mut impl Context) {
		context.clear_rect(self.y, self.x, self.height, self.width);
	}

	// TODO Rename: erase_empty_content, but it also redraws every DisplayItem.
	// I could rename it to that and make it skip drawing `DisplayItem`s, because that whould be what `fn draw` does?
	// Or call it "erase_erasable_content": with_sdl2 -> all, else -> just the emtpy part.
	fn erase_content(&self, context: &mut impl Context) {
		context.clear_rect(self.y + 1, self.x + 1, self.height - 2, self.width - 2);
	}

	/// Delete every text stored in the Window, but don't update the screen.
	fn delete_content(&mut self) {
		self.deleted_content_line_manager = std::mem::take(&mut self.line_manager);
	}

	fn erase_and_delete_drawn_content(&mut self, context: &mut impl Context) {
		self.delete_content();
		self.erase_content(context);
	}
}

impl Print for Window {
	type Coordinates = (u16, u16);

	fn primary_color(&self) -> RGB8 {
		self.primary_color
	}

	fn accent_color(&self) -> RGB8 {
		self.accent_color
	}

	fn print(&mut self, (y, x): (u16, u16), text: impl Display, fg: RGB8, bg: RGB8) {
		let text = self.crop_text_if_necessary(x, text.to_string());

		self.line_manager.insert_display_item(y, x, text, fg, bg);
	}
}

impl Draw for Window {
	fn draw(&mut self, context: &mut impl Context) {
		self.draw_decorations(context);
		self.draw_content(context);

		self.needs_draw = false;
	}

	fn needs_draw(&self) -> bool {
		self.needs_draw
	}

	fn schedule_draw(&mut self) {
		self.needs_draw = true;
	}
}

impl Darken for Window {
	fn set_darkened(&mut self, darkened: bool) {
		self.darkened = darkened;
	}
}

impl HandleInput for Window {
	fn handle_input(&mut self, context: &mut impl Context) -> Result<Key, String> {
		Ok(context.input_key())
	}
}
