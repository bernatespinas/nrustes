use rgb::RGB8;

use unicode_segmentation::UnicodeSegmentation;

use crate::colors::LIGHT_GREY;
use crate::context::Context;
use crate::window::Window;

/// An item that has been printed on a `Window`.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DisplayItem {
	pub(crate) text: String,
	pub(crate) fg: RGB8,
	pub(crate) bg: RGB8,
}

impl DisplayItem {
	pub fn new(text: String, fg: RGB8, bg: RGB8) -> Self {
		Self { text, fg, bg }
	}

	pub fn len(&self) -> usize {
		self.text.graphemes(true).count()
	}

	pub fn draw(&self, window: &Window, y: u16, x: u16, context: &mut impl Context) {
		context.render_text_container(window, y, x, &self.text, self.fg, self.bg);
	}

	pub fn draw_darkened(&self, window: &Window, y: u16, x: u16, context: &mut impl Context) {
		context.render_text_container(window, y, x, &self.text, LIGHT_GREY, self.bg);
	}

	pub fn set_fg(&mut self, fg: RGB8) {
		self.fg = fg;
	}
}
