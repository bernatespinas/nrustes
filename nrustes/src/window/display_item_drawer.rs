use std::collections::HashSet;

use rgb::RGB8;

use crate::colors::BLACK;
use crate::context::Context;
use crate::window::Window;

#[cfg(feature = "debug_chars")]
pub const DELETED_LINE_ERASE_CHAR: char = '*';

#[cfg(feature = "debug_chars")]
const DELETED_LINE_ERASE_FG: RGB8 = crate::colors::LIGHT_RED;

#[cfg(feature = "debug_chars")]
const UPDATED_LINE_ERASE_FG: RGB8 = crate::colors::LIGHT_GREEN;

#[cfg(not(feature = "debug_chars"))]
pub const DELETED_LINE_ERASE_CHAR: char = ' ';

#[cfg(not(feature = "debug_chars"))]
const DELETED_LINE_ERASE_FG: RGB8 = BLACK;

#[cfg(not(feature = "debug_chars"))]
const UPDATED_LINE_ERASE_FG: RGB8 = BLACK;

pub fn draw_empty_line_portion(
	window: &Window,
	line: u16,
	x: u16,
	length: u16,
	erase_string: &str,
	erase_string_fg: RGB8,
	context: &mut impl Context,
) {
	let empty_string = &erase_string[0..length as usize];

	// NOTE If `erase_string` contains extended / non-ASCII chars (such as à or Ź) I cannot use the code above to get a slice out of it. I'd have to do this instead. I'll leave it here in case I run into the same use case / issue again.
	// let empty_string: String = erase_string
	// 	.graphemes(true)
	// 	.take(length as usize)
	// 	.collect()
	// ;

	context.render_text_container(window, line, x, empty_string, erase_string_fg, BLACK);
}

pub trait DrawLines {
	fn draw_lines(window: &Window, context: &mut impl Context);

	fn draw_updated_line(window: &Window, line: u16, context: &mut impl Context) {
		let mut x = 1;

		// log::info!("\t\twindow.width = {}", window.width);

		let sorted_coordinates_in_line = window
			.line_manager
			.sorted_coordinates_in_line(line)
			.unwrap();

		for sorted_x in sorted_coordinates_in_line {
			draw_empty_line_portion(
				window,
				line,
				x,
				sorted_x - x,
				&window.empty_line,
				UPDATED_LINE_ERASE_FG,
				context,
			);

			let display_item = window.line_manager.display_item(line, *sorted_x).unwrap();

			if window.darkened {
				display_item.draw_darkened(window, line, *sorted_x, context);
			} else {
				display_item.draw(window, line, *sorted_x, context);
			}

			x = sorted_x + display_item.len() as u16;
		}

		// TODO Assuming I'll have to process lines longer that `window.width`.
		let empty_string_len = if x > window.width {
			0
		} else {
			window.width - 1 - x
		};

		draw_empty_line_portion(
			window,
			line,
			x,
			empty_string_len,
			&window.empty_line,
			UPDATED_LINE_ERASE_FG,
			context,
		);
	}
}

pub struct WindowContentDrawer {}

impl WindowContentDrawer {
	fn new_lines(window: &Window) -> impl Iterator<Item = u16> + '_ {
		window
			.line_manager
			.lines_not_present_in(&window.deleted_content_line_manager)
	}

	fn updated_lines(window: &Window) -> impl Iterator<Item = u16> + '_ {
		window
			.line_manager
			.lines_also_present_in(&window.deleted_content_line_manager)
	}

	fn deleted_lines(window: &Window) -> impl Iterator<Item = u16> + '_ {
		window
			.deleted_content_line_manager
			.lines_not_present_in(&window.line_manager)
	}

	fn missing_lines(window: &Window) -> impl Iterator<Item = u16> + '_ {
		let current_lines: HashSet<u16> = window.line_manager.lines().collect();

		let deleted_lines: HashSet<u16> = window.deleted_content_line_manager.lines().collect();

		(1..window.height - 1)
			.filter(move |line| !current_lines.contains(line) && !deleted_lines.contains(line))
	}

	fn draw_empty_lines(window: &Window, context: &mut impl Context) {
		let lines = Self::deleted_lines(window).chain(Self::missing_lines(window));

		for line in lines {
			// log::info!("%%& drawing DELETED line {line}");
			draw_empty_line_portion(
				window,
				line,
				1,
				window.width - 2,
				&window.empty_line,
				DELETED_LINE_ERASE_FG,
				context,
			);
		}
	}

	fn draw_non_empty_lines(window: &Window, context: &mut impl Context) {
		let lines = Self::updated_lines(window).chain(Self::new_lines(window));

		for line in lines {
			// log::info!("%%& drawing updated line {line}");

			Self::draw_updated_line(window, line, context);
		}
	}

	fn just_draw_display_items(window: &Window, context: &mut impl Context) {
		for line in window.line_manager.lines() {
			for (x, display_item) in window.line_manager.line_display_items(line) {
				if window.darkened {
					display_item.draw_darkened(window, line, *x, context);
				} else {
					display_item.draw(window, line, *x, context);
				}
			}
		}
	}
}

impl DrawLines for WindowContentDrawer {
	fn draw_lines(window: &Window, context: &mut impl Context) {
		if context.needs_to_redraw_buffer() {
			Self::just_draw_display_items(window, context);
		} else {
			Self::draw_non_empty_lines(window, context);
			Self::draw_empty_lines(window, context);
		}
	}
}
