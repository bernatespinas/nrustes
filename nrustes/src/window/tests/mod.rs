use crate::{backends::VoidContext, nrustes_config::NrustesConfig};

use super::{Window, WindowBuilder};

mod draw;
mod print;

struct TestContext {
	context: VoidContext,

	window: Window,
}

impl TestContext {
	pub fn new() -> Self {
		let nrustes_config = NrustesConfig::default();

		let context = VoidContext::new(160, 200, nrustes_config).mock_drawing_is_slow();

		let window: Window = WindowBuilder::full("Test", &context).build();

		Self { context, window }
	}

	fn window_is_empty(&self) {
		assert!(self.window.display_items().is_empty());
	}

	/// Asserts that all the items in `coords` are the only items that exist in
	/// the `Window`.
	/// `coords` is assumed not to contain repeated elements.
	fn items_exist_at_coords(&self, coords: &[(u16, u16)]) {
		let display_items = self.window.display_items();

		assert_eq!(
			display_items.len(),
			coords.len(),
			"Expected {} display items, but got {} instead.",
			coords.len(),
			display_items.len()
		);

		for (y, x) in coords {
			assert!(display_items.contains_key(&(*y, *x)));
		}
	}
}
