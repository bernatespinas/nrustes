use crate::{container::Container, draw::Draw, prelude::Print};

use super::TestContext;

#[test]
fn drawing_sets_needs_draw_to_false() {
	let mut test_context = TestContext::new();

	assert!(test_context.window.needs_draw);

	test_context.window.draw(&mut test_context.context);

	assert!(!test_context.window.needs_draw);
}

#[test]
fn erase_all_keeps_display_items() {
	let mut test_context = TestContext::new();

	test_context.window.print_accent((2, 2), "morning");
	test_context.window.print_accent((4, 2), "afternoon");
	test_context.window.print_accent((6, 2), "evening");
	test_context.window.print_accent((8, 2), "night");

	test_context.items_exist_at_coords(&[(2, 2), (4, 2), (6, 2), (8, 2)]);

	test_context.window.erase_all(&mut test_context.context);

	test_context.items_exist_at_coords(&[(2, 2), (4, 2), (6, 2), (8, 2)]);
}
