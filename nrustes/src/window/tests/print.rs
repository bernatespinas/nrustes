use crate::{
	colors::{BLACK, DARK_BLUE, LIGHT_GREEN},
	context::Context,
	prelude::Print,
	window::DisplayItem,
};

use super::TestContext;

#[test]
fn print() {
	let mut test_context = TestContext::new();

	test_context
		.window
		.print((2, 2), "print_fgbg", LIGHT_GREEN, DARK_BLUE);

	let display_item = test_context.window.display_item((2, 2));

	assert_eq!(
		display_item,
		Some(&DisplayItem::new(
			"print_fgbg".to_string(),
			LIGHT_GREEN,
			DARK_BLUE
		))
	);
}

#[test]
fn print_fg() {
	let mut test_context = TestContext::new();

	test_context
		.window
		.print_fg((2, 2), "print_fg", LIGHT_GREEN);

	let display_item = test_context.window.display_item((2, 2));

	assert_eq!(
		display_item,
		Some(&DisplayItem::new(
			"print_fg".to_string(),
			LIGHT_GREEN,
			BLACK
		))
	);
}

#[test]
fn print_primary() {
	let mut test_context = TestContext::new();

	test_context.window.print_primary((2, 2), "print_primary");

	let display_item = test_context.window.display_item((2, 2));

	assert_eq!(
		display_item,
		Some(&DisplayItem::new(
			"print_primary".to_string(),
			test_context.window.primary_color,
			BLACK
		))
	);

	assert_eq!(
		test_context.window.primary_color,
		test_context.context.primary_color()
	);
}

#[test]
fn print_accent() {
	let mut test_context = TestContext::new();

	test_context.window.print_accent((2, 2), "print_accent");

	let display_item = test_context.window.display_item((2, 2));

	assert_eq!(
		display_item,
		Some(&DisplayItem::new(
			"print_accent".to_string(),
			test_context.window.accent_color,
			BLACK
		))
	);

	assert_eq!(
		test_context.window.accent_color,
		test_context.context.accent_color()
	);
}

#[test]
fn first_test() {
	let mut test_context = TestContext::new();

	test_context.window_is_empty();

	test_context.window.print_primary((0, 0), "one_line");

	test_context.items_exist_at_coords(&vec![(0, 0)]);
}
