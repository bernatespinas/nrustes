use std::path::PathBuf;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let items = (0..200)
		.into_iter()
		.map(|i| format!("Option {i}"))
		.collect();

	let mut scrollable: Scrollable<String> = WindowBuilder::left("Left scrollable", &mut context)
		.with_bottom_text("Back - Exit  d - Delete")
		.scrollable_builder()
		.with_items(items)
		.build();

	let mut win_right: Window = WindowBuilder::right("Right window", &mut context).build();

	win_right.print_accent((3, 1), "Up: arrow key or 8");
	win_right.print_accent((5, 1), "Down: arrow key or 2");
	win_right.print_accent((7, 1), "Previous page: left arrow key or 4");
	win_right.print_accent((9, 1), "Next page: right arrow key or 6");

	win_right.draw_decorations(&mut context);

	loop {
		win_right.print_primary((13, 1), format!("Currently at: {:?}", scrollable.hovered()));
		win_right.draw_content(&mut context);

		match scrollable.handle_input_and_redraw(&mut context).unwrap() {
			Key::Char('d') => scrollable.delete_hovered(),
			Key::Back => break,
			_ => continue,
		};
	}
}
