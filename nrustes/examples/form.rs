use std::path::PathBuf;

use nrustes::form::{FieldType, Form};
use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::{ErrorPopup, HandleInput, Key};
use nrustes::window::WindowBuilder;

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextCrossterm::new(nrustes_config);

	let mut form = Form::new(WindowBuilder::full("Form example", &mut context).build())
		.with_required_field("Name".to_string(), FieldType::String)
		.with_required_field("Age".to_string(), FieldType::U16)
		.with_optional_field("Dumbness".to_string(), FieldType::I32)
		.with_optional_field(
			"Season".to_string(),
			FieldType::Choice(vec![
				"Winter".to_string(),
				"Spring".to_string(),
				"Summer".to_string(),
				"Autumn".to_string(),
			]),
		);

	loop {
		match form.handle_input_and_redraw(&mut context) {
			Ok(Key::Back) => return,
			Err(message) => {
				ErrorPopup::centered_in_container(message, &form, &mut context)
					.show(&mut form, &mut context);
			}
			Ok(Key::Char('s')) => match form.validate() {
				Ok(()) => return,
				Err(message) => {
					ErrorPopup::centered_in_container(message, &form, &mut context)
						.show(&mut form, &mut context);
				}
			},
			_ => {}
		};
	}
}
