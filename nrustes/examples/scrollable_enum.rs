use std::fmt::Display;
use std::path::PathBuf;

use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

#[derive(Debug, EnumIter)]
enum Action {
	CreateGame,
	LoadGame,
	DeleteGame,
	Exit,
}

impl Display for Action {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Action::CreateGame => write!(f, "Create game"),
			Action::LoadGame => write!(f, "Load game"),
			Action::DeleteGame => write!(f, "Delete game"),
			Action::Exit => write!(f, "Exit"),
		}
	}
}

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let mut scrollable: Scrollable<Action> = WindowBuilder::left("Left scrollable", &mut context)
		.scrollable_builder()
		.with_items(Action::iter().collect())
		.build();

	let mut win_right: Window = WindowBuilder::right("Right window", &mut context).build();

	win_right.print_accent((3, 1), "Up: arrow key or 8");
	win_right.print_accent((5, 1), "Down: arrow key or 2");
	win_right.print_accent((7, 1), "Previous page: left arrow key or 4");
	win_right.print_accent((9, 1), "Next page: right arrow key or 6");

	win_right.draw_decorations(&mut context);

	loop {
		win_right.erase_content(&mut context);
		win_right.print_primary((13, 1), format!("Currently at: {:?}", scrollable.hovered()));
		win_right.draw_content(&mut context);

		match scrollable.handle_input_and_redraw(&mut context).unwrap() {
			Key::Select => match scrollable.hovered() {
				Some(Action::CreateGame) => {
					ErrorPopup::centered_in_container("Create game", &scrollable, &context)
						.show(&mut scrollable, &mut context);
				}
				Some(Action::LoadGame) => {
					ErrorPopup::centered_in_container("Load game", &scrollable, &context)
						.show(&mut scrollable, &mut context);
				}
				Some(Action::DeleteGame) => {
					ErrorPopup::centered_in_container("Delete game", &scrollable, &context)
						.show(&mut scrollable, &mut context);
				}
				Some(Action::Exit) => break,
				None => {}
			},
			_ => continue,
		};
	}
}
