use std::{fmt::Display, path::PathBuf};

use nrustes::{nrustes_config::NrustesConfig, prelude::*};

#[derive(Clone, Copy)]
enum Action {
	Select,
}

#[derive(EnumIter)]
enum PopupType {
	ErrorPopupScreen,
	ErrorPopupWindow,
	ProgressPopupScreenOk,
	ProgressPopupWindowErr,
}

impl Display for PopupType {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			PopupType::ErrorPopupScreen => write!(f, "ErrorPopup - centered_in_screen"),
			PopupType::ErrorPopupWindow => write!(f, "ErrorPopup - centered_in_window"),
			PopupType::ProgressPopupScreenOk => {
				write!(f, "ProgressPopup - centered_in_screen - Ok")
			}
			PopupType::ProgressPopupWindowErr => {
				write!(f, "ProgressPopup - centered_in_window - Err")
			}
		}
	}
}

#[derive(Draw, DoubleViewHolder)]
struct MainView {
	#[draw]
	view: DoubleViewState<Scrollable<PopupType>, Window, Action>,
}

impl MainView {
	fn new(context: &impl Context) -> MainView {
		let window: Window = WindowBuilder::right("Window", context).build();

		let scrollable: Scrollable<PopupType> = WindowBuilder::left("Scrollable", context)
			.scrollable_builder()
			.with_items(PopupType::iter().collect())
			.build();

		MainView {
			view: DoubleViewState::new(scrollable, window),
		}
	}
}

impl DoubleView for MainView {
	type Output = PopupType;

	fn initialize(&mut self) {
		self.view
			.secondary_container
			.print_accent((3, 1), "Since I'm using a `View`, `Popup`s know");

		self.view
			.secondary_container
			.print_accent((5, 1), "what to darken, undarken and draw.");

		self.view.main_container.bind_shortcut(
			Key::Select,
			Shortcut::new(Action::Select)
				.with_label("Create popup")
				.permanent(),
		);
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Select => match self.main_container().container().hovered() {
				Some(PopupType::ErrorPopupScreen) => {
					ErrorPopup::centered_in_screen("Centered in screen", context)
						.show(self, context);
				}
				Some(PopupType::ErrorPopupWindow) => {
					ErrorPopup::centered_in_container(
						"Centered in window",
						self.secondary_container().window(),
						context,
					)
					.show(self, context);
				}
				Some(PopupType::ProgressPopupScreenOk) => {
					let sleeping = || -> Result<(), String> {
						std::thread::sleep(std::time::Duration::from_secs(2));

						Ok(())
					};

					let result = ProgressPopup::centered_in_screen("Sleeping", context)
						.process(sleeping, self, context);

					assert_eq!(result, Ok(()));
				}
				Some(PopupType::ProgressPopupWindowErr) => {
					let sleeping = || -> Result<(), String> {
						std::thread::sleep(std::time::Duration::from_secs(2));

						Err("Harcoded error".to_string())
					};

					let result = ProgressPopup::centered_in_container(
						"Sleeping",
						self.main_container(),
						context,
					)
					.process(sleeping, self, context);

					assert_eq!(result, Err("Harcoded error".to_string()));
				}
				None => {}
			},
		};

		self.schedule_draw();

		None
	}
}

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	example(&mut context);
}

pub fn example(context: &mut impl Context) {
	let mut main_view = MainView::new(context);

	main_view.input_loop(context);
}
