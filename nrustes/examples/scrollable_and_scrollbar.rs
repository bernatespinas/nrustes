use std::path::PathBuf;

use nrustes::{nrustes_config::NrustesConfig, prelude::*};

fn main() -> Result<(), String> {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextCrossterm::new(nrustes_config);

	let items = (0..200)
		.into_iter()
		.map(|i| format!("Option {i}"))
		.collect();

	let scrollable: Scrollable<String> = WindowBuilder::left("Scrollable", &mut context)
		.with_bottom_text("Back - Exit  d - Delete")
		.scrollable_builder()
		.with_items(items)
		.build();

	let scrollbar: Scrollbar = WindowBuilder::right("Scrollbar", &mut context)
		.with_bottom_text("asdasd")
		.build();

	let mut view = View {
		view: DoubleViewState::new(scrollable, scrollbar),
	};

	view.input_loop(&mut context);

	Ok(())
}

#[derive(Clone)]
pub enum Action {
	ScrollItems,
}

#[derive(Draw, DoubleViewHolder)]
pub struct View {
	#[draw]
	view: DoubleViewState<Scrollable<String>, Scrollbar, Action>,
}

impl DoubleView for View {
	type Output = ();

	fn initialize(&mut self) {
		self.main_container_mut()
			.bind_movement_shortcuts(Action::ScrollItems);

		self.print_hovered_item_details();

		assert!(self
			.main_container()
			.shortcut_data(&Key::Movement(direction_simple::Direction::Up))
			.is_some());
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::ScrollItems => {
				self.secondary_container_mut().delete_content();

				self.print_hovered_item_details();

				None
			}
		}
	}
}

impl View {
	fn print_hovered_item_details(&mut self) {
		let hovered_index = self.main_container().container().hovered_index().unwrap();

		for _ in 0..200 {
			self.secondary_container_mut()
				.print_accent(2, format!("Item {hovered_index} is hovered."));
		}

		self.secondary_container_mut().schedule_draw();
	}
}
