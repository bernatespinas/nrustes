use std::path::PathBuf;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let mut win: Window = WindowBuilder::full("I ate a lot", &mut context)
		.with_bottom_text("Esc - Exit")
		.build();

	win.draw(&mut context);

	loop {
		match context.input_key() {
			Key::Back => break,
			_ => continue,
		};
	}
}
