use std::fmt::Display;
use std::path::PathBuf;
use std::time::Duration;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

#[derive(EnumIter)]
enum PopupType {
	ErrorPopupScreenComplete,
	ErrorPopupScreenIncomplete,
	ErrorPopupWindow,
	ProgressPopupScreenOk,
	ProgressPopupWindowErr,
}

impl Display for PopupType {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			PopupType::ErrorPopupScreenComplete => {
				write!(f, "ErrorPopup - centered_in_screen - complete")
			}
			PopupType::ErrorPopupScreenIncomplete => {
				write!(f, "ErrorPopup - centered_in_screen - incomplete")
			}
			PopupType::ErrorPopupWindow => write!(f, "ErrorPopup - centered_in_window"),
			PopupType::ProgressPopupScreenOk => {
				write!(f, "ProgressPopup - centered_in_screen - Ok")
			}
			PopupType::ProgressPopupWindowErr => {
				write!(f, "ProgressPopup - centered_in_window - Err")
			}
		}
	}
}

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let mut scrollable: Scrollable<PopupType> = WindowBuilder::left("Popup list", &mut context)
		.with_bottom_text("Esc - Exit  Enter - Create Popup")
		.scrollable_builder()
		.with_items(PopupType::iter().collect())
		.build();

	let mut win_right: Window = WindowBuilder::right("Right window", &mut context).build();

	win_right.print_accent((3, 1), "Blah blah blah. ;)");
	win_right.draw(&mut context);

	loop {
		match scrollable.handle_input_and_redraw(&mut context).unwrap() {
			Key::Select => {
				match scrollable.hovered() {
					Some(PopupType::ErrorPopupScreenComplete) => {
						// This `Popup` is rendered at the middle of the screen and knows how to darken and undarken just one thing. Here, since I chose `&mut win_right`, I have to handle `scrollable` manually: I have to darken and draw it before the popup appears and undarken it after it's been dismissed. I also have to draw `win_right` manually.
						// If I had 3 more `Window`s, for example, I would have to manually handle them too.
						// To avoid having to do all this you can use a view. See the `popups_with_views` example.

						scrollable.darken();
						scrollable.draw(&mut context);

						ErrorPopup::centered_in_screen("centered_in_screen", &context)
							.show(&mut win_right, &mut context);

						scrollable.undarken();
						win_right.draw(&mut context);
					}
					Some(PopupType::ErrorPopupScreenIncomplete) => {
						// This is what happens if I don't take all the steps from the `PopupType::ErrorPopupScreenComplete`'s example.
						ErrorPopup::centered_in_screen("centered_in_screen", &context)
							.show(&mut win_right, &mut context);
					}
					Some(PopupType::ErrorPopupWindow) => {
						scrollable.darken();
						scrollable.draw(&mut context);

						ErrorPopup::centered_in_container(
							"centered_in_window",
							&win_right,
							&context,
						)
						.show(&mut win_right, &mut context);

						scrollable.undarken();
						// `ErrorPopup` darkens and undarkens `win_right`, but doesn't draw it again (in case I do other things that already do it: like what views, for example).
						win_right.draw(&mut context);
					}
					Some(PopupType::ProgressPopupScreenOk) => {
						let sleeping = || -> Result<(), String> {
							std::thread::sleep(Duration::from_secs(2));

							Ok(())
						};

						scrollable.darken();
						scrollable.draw(&mut context);

						let result = ProgressPopup::centered_in_screen("Sleeping", &mut context)
							.process(sleeping, &mut win_right, &mut context);

						assert_eq!(result, Ok(()));

						scrollable.undarken();
						win_right.draw(&mut context);
					}
					Some(PopupType::ProgressPopupWindowErr) => {
						let sleeping = || -> Result<(), String> {
							std::thread::sleep(Duration::from_secs(2));

							Err("Hardcoded error".to_string())
						};

						scrollable.darken();
						scrollable.draw(&mut context);

						let result = ProgressPopup::centered_in_container(
							"Sleeping",
							&win_right,
							&mut context,
						)
						.process(sleeping, &mut win_right, &mut context);

						assert_eq!(result, Err("Hardcoded error".to_string()));

						scrollable.undarken();
						win_right.draw(&mut context);
					}
					None => {}
				};
			}
			Key::Back => break,
			_ => continue,
		};
	}
}
