use std::path::PathBuf;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::create_context_sdl2;
use nrustes::texture_atlas::{LoadTextureAtlas, TextureAtlasLoader};

mod popups_with_views;

fn main() -> Result<(), String> {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	create_context_sdl2!(
		context,
		texture_creator,
		nrustes_config: nrustes_config,
		window_title: "nrustes",
		window_mode: "windowed"
	);

	let texture_atlas_loader = TextureAtlasLoader::new(PathBuf::from("nrustes").join("examples"));

	texture_atlas_loader.load_font(
		"font",
		"font.bmp",
		"font.ron",
		(14, 14),
		&mut context,
		&texture_creator,
	)?;

	context
		.texture_atlas_font
		.set_current_texture("font".to_string());

	popups_with_views::example(&mut context);

	Ok(())
}
