use std::path::PathBuf;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

fn main() -> Result<(), String> {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextCrossterm::new(nrustes_config);

	let mut scrollbar: Scrollbar = WindowBuilder::left("Left scrollable", &mut context)
		.with_bottom_text("Back - Exit")
		.build();

	for i in 0..200 {
		scrollbar.print_accent(2, format!("Item {i}"));
	}

	scrollbar.print_primary(2, "a really really long line really long line really long line really long line really long line really long line really long line really long line really long line");

	scrollbar.print_primary(2, "abecedary bee column dictionary epiphany frontal gullible harmonic intellectual jump knee loom mastodon nominal oppulence partisan query retrospective studying theatrical universal venerable web xanadu yarn zngana");

	scrollbar.print_accent(2, "hahahaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                                                                                                           d d d d d d d d");

	let mut win_right: Window = WindowBuilder::right("Right window", &mut context).build();

	win_right.print_accent((3, 1), "Up: arrow key or 8");
	win_right.print_accent((5, 1), "Down: arrow key or 2");
	win_right.print_accent((7, 1), "Previous page: left arrow key or 4");
	win_right.print_accent((9, 1), "Next page: right arrow key or 6");

	win_right.draw(&mut context);

	loop {
		match scrollbar.handle_input_and_redraw(&mut context).unwrap() {
			Key::Back => break,
			_ => continue,
		};
	}

	Ok(())
}
