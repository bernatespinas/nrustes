use std::path::PathBuf;

use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let mut scrollable: Scrollable<&str> = WindowBuilder::left("Left scrollable", &mut context)
		.with_bottom_text("Back - Exit  Select - Print to window")
		.scrollable_builder()
		.with_items(vec![
			"First option",
			"Second option",
			"Third option",
			"Move with arrow keys or 8 and 2",
			"Use enter and 5 to select",
		])
		.build();

	let mut win_right: Window = WindowBuilder::right("Right window", &mut context).build();
	win_right.draw(&mut context);

	loop {
		match scrollable.handle_input_and_redraw(&mut context).unwrap() {
			Key::Select => {
				win_right.erase_and_delete_drawn_content(&mut context);
				win_right.print_primary((3, 1), scrollable.hovered().unwrap());
				win_right.draw_content(&mut context);
			}
			Key::Back => break,
			_ => continue,
		};
	}
}
