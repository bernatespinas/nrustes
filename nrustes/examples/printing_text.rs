use std::path::PathBuf;

use rgb::RGB8;

use nrustes::colors::BLACK;
use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

enum Color {
	Yellow,
	Orange,
	Red,
	LightGreen,
	LightBlue,
	DarkPurple,
}

impl From<Color> for RGB8 {
	fn from(color: Color) -> RGB8 {
		match color {
			Color::Red => RGB8::new(255, 25, 25),
			Color::Orange => RGB8::new(255, 140, 25),
			Color::Yellow => RGB8::new(255, 255, 25),
			Color::LightGreen => RGB8::new(25, 255, 25),
			Color::LightBlue => RGB8::new(25, 255, 255),
			Color::DarkPurple => RGB8::new(100, 0, 200),
		}
	}
}

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let mut win: Window = WindowBuilder::full("COLORS!!!", &mut context)
		.with_bottom_text("Esc - Exit  r - Refresh")
		.with_colors(Color::Yellow.into(), Color::Orange.into())
		.build();

	win.print_fg((3, 1), "Red velvet", Color::Red.into());
	win.print(
		(5, 1),
		"My eyes hurt",
		Color::LightGreen.into(),
		Color::DarkPurple.into(),
	);

	win.print_accent((9, 1), "Using the accent color.");
	win.print_primary((11, 1), "Using the primary color.");

	win.draw(&mut context);

	context.render_text_container(
		&win,
		15,
		1,
		"Printed directly through context, not stored in the Window.",
		Color::LightBlue.into(),
		BLACK,
	);

	loop {
		match context.input_key() {
			Key::Char('r') => win.draw(&mut context),
			Key::Back => break,
			_ => continue,
		};
	}
}
