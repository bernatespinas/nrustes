use std::fmt::Display;
use std::path::PathBuf;

use rgb::RGB8;

use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use nrustes::colors::BLACK;
use nrustes::nrustes_config::NrustesConfig;
use nrustes::prelude::*;

enum Color {
	Yellow,
	LightGreen,
}

impl From<Color> for RGB8 {
	fn from(color: Color) -> RGB8 {
		match color {
			Color::Yellow => RGB8::new(255, 255, 25),
			Color::LightGreen => RGB8::new(25, 255, 25),
		}
	}
}

#[derive(EnumIter)]
enum ScrollableItem {
	InputKey,
	InputKeyGamify,
	InputString,
}

impl Display for ScrollableItem {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			ScrollableItem::InputKey => write!(f, "input_key"),
			ScrollableItem::InputKeyGamify => write!(f, "input_key + gamify"),
			ScrollableItem::InputString => write!(f, "input_string"),
		}
	}
}

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextTermion::new(nrustes_config);

	let mut scrollable: Scrollable<ScrollableItem> =
		WindowBuilder::left("Left scrollable", &mut context)
			.with_bottom_text("Esc - Exit")
			.scrollable_builder()
			.with_items(ScrollableItem::iter().collect())
			.build();

	let mut win_right: Window = WindowBuilder::right("Right window", &mut context).build();

	win_right.print_accent((3, 1), "input_key");
	win_right.print_primary(
		(5, 1),
		"Waits for a single keypress. Keys mean what they mean.",
	);

	win_right.print_accent((9, 1), "input_key + gamify");
	win_right.print_primary(
		(11, 1),
		"Waits for a single keypress, and treats some differently.",
	);
	win_right.print_primary((13, 1), "For example, 8 means 'up', 2 'down', 5 'enter'...");

	win_right.print_accent((17, 1), "input_string");
	win_right.print_primary((19, 1), "Reads a String until the user presses Enter.");
	win_right.print_primary((21, 1), "Can't be cancelled yet.");

	// Splitting printed strings is in my TODO list. ;)

	win_right.draw(&mut context);

	loop {
		match scrollable.handle_input_and_redraw(&mut context).unwrap() {
			Key::Select => {
				win_right.draw_content(&mut context);

				match scrollable.hovered() {
					Some(ScrollableItem::InputKey) => {
						context.render_text_container(
							&win_right,
							25,
							1,
							"Press a key NOW",
							Color::Yellow.into(),
							BLACK,
						);

						let key = context.input_key();

						context.render_text_container(
							&win_right,
							27,
							1,
							&format!("You pressed {key:?}."),
							Color::LightGreen.into(),
							BLACK,
						);
					}
					Some(ScrollableItem::InputKeyGamify) => {
						context.render_text_container(
							&win_right,
							25,
							1,
							"Press a key NOW",
							Color::Yellow.into(),
							BLACK,
						);

						let key = context.input_key().gamify();

						context.render_text_container(
							&win_right,
							27,
							1,
							&format!("You pressed {key:?}."),
							Color::LightGreen.into(),
							BLACK,
						);
					}
					Some(ScrollableItem::InputString) => {
						let string =
							TextInputModal::new("Spill the tea", scrollable.window(), &context)
								.show(&mut scrollable, &mut context);

						let text = string.map_or("You pressed Esc! ;)".to_string(), |s| {
							format!("You typed '{s}'.")
						});

						context.render_text_container(
							&win_right,
							25,
							1,
							&text,
							Color::LightGreen.into(),
							BLACK,
						);
					}
					None => {}
				};
			}
			Key::Back => break,
			_ => continue,
		};
	}
}
