use std::path::PathBuf;

use rgb::RGB8;

use nrustes::context::Context;
use nrustes::nrustes_config::NrustesConfig;

fn main() {
	let nrustes_config = NrustesConfig::load(
		PathBuf::from("nrustes")
			.join("examples")
			.join("nrustes_config.ron"),
	)
	.unwrap();

	let mut context = nrustes::backends::ContextCrossterm::new(nrustes_config);

	context.render_text(
		2,
		3,
		"We have not cleared the screen.",
		RGB8::new(100, 200, 250),
		RGB8::new(1, 1, 1),
	);

	context.render_text(
		20,
		4,
		"Yeah! We like it like that!",
		RGB8::new(250, 100, 200),
		RGB8::new(1, 1, 1),
	);

	context.render_text(
		45,
		15,
		"Psst! Press any key to clear it!",
		RGB8::new(100, 50, 250),
		RGB8::new(10, 10, 10),
	);

	context.input_key();

	context.clear();
	context.render_text(
		9,
		3,
		"Ahhh, that's what I'm talking about!",
		RGB8::new(100, 50, 250),
		RGB8::new(10, 10, 10),
	);

	context.render_text(27, 3, ":(", RGB8::new(100, 200, 250), RGB8::new(1, 1, 1));

	context.render_text(40, 4, ";_;", RGB8::new(250, 100, 200), RGB8::new(1, 1, 1));

	context.input_key();
}
