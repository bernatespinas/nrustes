use super::TestContext;

#[test]
fn delete_first_item_in_only_page() {
	let mut test_context = TestContext::enumerated(10, 20);

	test_context.assert_status(0, 0, 0);
	test_context.assert_has_items(10);
	test_context.delete_hovered_item(0);
	test_context.assert_status(0, 0, 1);
	test_context.assert_has_items(9);
}

#[test]
fn delete_first_item_in_first_page() {
	let mut test_context = TestContext::enumerated(30, 10);

	test_context.assert_status(0, 0, 0);
	test_context.assert_has_items(30);
	test_context.delete_hovered_item(0);
	test_context.assert_status(0, 0, 1);
	test_context.assert_has_items(29);
}

#[test]
fn delete_first_item_in_last_page() {
	let mut test_context = TestContext::enumerated(30, 10);
	test_context.set_hovered_item_index(20);

	test_context.assert_status(2, 20, 20);
	test_context.assert_has_items(30);
	test_context.delete_hovered_item(20);
	test_context.assert_status(2, 20, 21);
	test_context.assert_has_items(29);
}

#[test]
fn delete_first_item_in_middle_page() {
	let mut test_context = TestContext::enumerated(30, 10);
	test_context.set_hovered_item_index(10);

	test_context.assert_status(1, 10, 10);
	test_context.assert_has_items(30);
	test_context.delete_hovered_item(10);
	test_context.assert_status(1, 10, 11);
	test_context.assert_has_items(29);
}

#[test]
fn delete_last_item_in_only_page() {
	let mut test_context = TestContext::enumerated(10, 20);
	test_context.set_hovered_item_index(9);

	test_context.assert_status(0, 9, 9);
	test_context.assert_has_items(10);
	test_context.delete_hovered_item(9);
	test_context.assert_status(0, 8, 8);
	test_context.assert_has_items(9);
}

#[test]
fn delete_last_item_in_first_page() {
	let mut test_context = TestContext::enumerated(30, 10);
	test_context.set_hovered_item_index(9);

	test_context.assert_status(0, 9, 9);
	test_context.assert_has_items(30);
	test_context.delete_hovered_item(9);
	test_context.assert_status(0, 9, 10);
	test_context.assert_has_items(29);
}

#[test]
fn delete_last_item_in_last_page() {
	let mut test_context = TestContext::enumerated(30, 10);
	test_context.set_hovered_item_index(29);

	test_context.assert_status(2, 29, 29);
	test_context.assert_has_items(30);
	test_context.delete_hovered_item(29);
	test_context.assert_status(2, 28, 28);
	test_context.assert_has_items(29);
}

#[test]
fn delete_last_item_in_middle_page() {
	let mut test_context = TestContext::enumerated(30, 10);
	test_context.set_hovered_item_index(19);

	test_context.assert_status(1, 19, 19);
	test_context.assert_has_items(30);
	test_context.delete_hovered_item(19);
	test_context.assert_status(1, 19, 20);
	test_context.assert_has_items(29);
}

#[test]
fn delete_only_item() {
	let mut test_context = TestContext::enumerated(1, 10);

	test_context.assert_status(0, 0, 0);
	test_context.assert_has_items(1);
	test_context.delete_hovered_item(0);
	test_context.assert_has_no_items();
}

#[test]
fn delete_all_items_in_only_page() {
	let mut test_context = TestContext::enumerated(10, 10);

	for i in 0..10 {
		test_context.assert_status(0, 0, i);
		test_context.assert_has_items(10 - i);
		test_context.delete_hovered_item(i);
	}

	test_context.assert_has_no_items();
}

#[test]
fn delete_only_item_in_last_page() {
	let mut test_context = TestContext::enumerated(21, 10);
	test_context.set_hovered_item_index(20);

	test_context.assert_status(2, 20, 20);
	test_context.assert_has_items(21);
	test_context.delete_hovered_item(20);
	test_context.assert_status(1, 19, 19);
}

#[test]
fn delete_item_which_deletes_last_empty_page() {
	let mut test_context = TestContext::enumerated(21, 10);

	test_context.assert_status(0, 0, 0);
	test_context.assert_has_items(21);
	test_context.delete_hovered_item(0);
	test_context.assert_status(0, 0, 1);
	test_context.assert_has_items(20);
}
