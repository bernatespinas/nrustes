use direction_simple::Direction;

use nrustes::key::Key;

use super::TestContext;

#[test]
fn scroll_down_in_same_page() {
	let mut test_context = TestContext::enumerated(10, 20);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_status(0, 1, 1);
	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_status(0, 2, 2);
}

#[test]
fn scroll_up_in_same_page() {
	let mut test_context = TestContext::enumerated(10, 20);
	test_context.set_hovered_item_index(4);

	test_context.assert_status(0, 4, 4);
	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(0, 3, 3);
	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(0, 2, 2);
}

#[test]
fn scroll_down_to_next_page() {
	let mut test_context = TestContext::enumerated(10, 5);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 4);
	test_context.assert_status(0, 4, 4);
	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_status(1, 5, 5);
}

#[test]
fn scroll_up_to_previous_page() {
	let mut test_context = TestContext::enumerated(40, 10);
	test_context.set_hovered_item_index(20);

	test_context.assert_status(2, 20, 20);
	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(1, 19, 19);
}

#[test]
fn scroll_right_to_next_page() {
	let mut test_context = TestContext::enumerated(60, 10);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key(Key::Movement(Direction::Right));
	test_context.assert_status(1, 10, 10);
	test_context.mock_key(Key::Movement(Direction::Right));
	test_context.assert_status(2, 20, 20);
}

#[test]
fn scroll_left_to_previous_page() {
	let mut test_context = TestContext::enumerated(60, 10);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key(Key::Movement(Direction::Left));
	test_context.assert_status(5, 50, 50);
	test_context.mock_key(Key::Movement(Direction::Left));
	test_context.assert_status(4, 40, 40);
}

#[test]
fn scroll_down_in_same_page_to_first_item() {
	let mut test_context = TestContext::enumerated(10, 20);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 9);
	test_context.assert_status(0, 9, 9);
	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_status(0, 0, 0);
}

#[test]
fn scroll_down_to_first_item_in_first_page() {
	let mut test_context = TestContext::enumerated(30, 10);
	test_context.set_hovered_item_index(29);

	test_context.assert_status(2, 29, 29);
	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_status(0, 0, 0);
}

#[test]
fn scroll_up_in_same_page_to_last_item() {
	let mut test_context = TestContext::enumerated(10, 20);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(0, 9, 9);
	test_context.mock_key_multiple_times(Key::Movement(Direction::Up), 9);
	test_context.assert_status(0, 0, 0);
	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(0, 9, 9);
}

#[test]
fn scroll_up_to_last_item_in_last_page() {
	let mut test_context = TestContext::enumerated(60, 10);

	test_context.assert_status(0, 0, 0);
	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(5, 59, 59);
}

#[test]
fn scroll_in_all_directions_with_just_one_item() {
	let mut test_context = TestContext::enumerated(1, 10);

	test_context.assert_status(0, 0, 0);

	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_status(0, 0, 0);

	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_status(0, 0, 0);

	test_context.mock_key(Key::Movement(Direction::Left));
	test_context.assert_status(0, 0, 0);

	test_context.mock_key(Key::Movement(Direction::Right));
	test_context.assert_status(0, 0, 0);
}

#[test]
fn scroll_in_all_directions_with_no_items() {
	let mut test_context = TestContext::enumerated(0, 10);

	test_context.assert_has_no_items();

	test_context.mock_key(Key::Movement(Direction::Up));
	test_context.assert_has_no_items();

	test_context.mock_key(Key::Movement(Direction::Down));
	test_context.assert_has_no_items();

	test_context.mock_key(Key::Movement(Direction::Left));
	test_context.assert_has_no_items();

	test_context.mock_key(Key::Movement(Direction::Right));
	test_context.assert_has_no_items();
}
