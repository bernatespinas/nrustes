use direction_simple::Direction;

use nrustes::key::Key;

use super::TestContext;

#[test]
fn select_items_from_same_page() {
	let mut test_context = TestContext::enumerated(30, 10);

	test_context.assert_selected_items(Vec::new());
	test_context.select_hovered_item();
	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 3);
	test_context.select_hovered_item();
	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 2);
	test_context.select_hovered_item();

	test_context.assert_selected_items(vec![0, 3, 5]);
}

#[test]
fn select_items_from_different_pages() {
	let mut test_context = TestContext::enumerated(30, 10);

	test_context.assert_status(0, 0, 0);
	test_context.assert_selected_items(Vec::new());

	test_context.select_hovered_item();
	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 9);
	test_context.assert_status(0, 9, 9);
	test_context.select_hovered_item();

	test_context.mock_key(Key::Movement(Direction::Right));
	test_context.assert_status(1, 19, 19);
	test_context.select_hovered_item();

	test_context.assert_selected_items(vec![0, 9, 19]);
}

#[test]
fn select_same_item_multiple_times() {
	let mut test_context = TestContext::enumerated(10, 10);

	test_context.assert_status(0, 0, 0);

	for _ in 0..10 {
		test_context.select_hovered_item();
		test_context.assert_selected_items(vec![0]);
		test_context.select_hovered_item();
		test_context.assert_selected_items(Vec::new());
	}
}

#[test]
fn select_when_there_are_no_items_does_nothing() {
	let mut test_context = TestContext::enumerated(0, 10);

	test_context.assert_has_no_items();
	test_context.assert_selected_items(Vec::new());

	test_context.select_hovered_item();

	test_context.assert_selected_items(Vec::new());
}

#[test]
fn delete_only_selected_item() {
	let mut test_context = TestContext::enumerated(10, 10);

	test_context.assert_status(0, 0, 0);
	test_context.assert_selected_items(Vec::new());
	test_context.select_hovered_item();
	test_context.assert_selected_items(vec![0]);
	test_context.delete_hovered_item(0);
	test_context.assert_status(0, 0, 1);
	test_context.assert_selected_items(Vec::new());
}

#[test]
fn delete_item_below_selected_item_in_only_page() {
	let mut test_context = TestContext::enumerated(10, 10);

	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 4);
	test_context.assert_status(0, 4, 4);
	test_context.select_hovered_item();
	test_context.assert_selected_items(vec![4]);

	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 2);
	test_context.assert_status(0, 6, 6);

	test_context.delete_hovered_item(6);
	test_context.assert_selected_items(vec![4]);
}

#[test]
fn delete_item_above_selected_item_in_only_page() {
	let mut test_context = TestContext::enumerated(10, 10);

	test_context.mock_key_multiple_times(Key::Movement(Direction::Down), 4);
	test_context.assert_status(0, 4, 4);
	test_context.select_hovered_item();
	test_context.assert_selected_items(vec![4]);

	test_context.mock_key_multiple_times(Key::Movement(Direction::Up), 2);
	test_context.assert_status(0, 2, 2);

	test_context.delete_hovered_item(2);
	test_context.assert_selected_items(vec![3]);
}

#[test]
fn delete_item_above_and_below_selected_items_in_only_page() {
	let mut test_context = TestContext::enumerated(10, 10);

	test_context.set_hovered_item_index(3);
	test_context.select_hovered_item();

	test_context.set_hovered_item_index(8);
	test_context.select_hovered_item();

	test_context.set_hovered_item_index(5);
	test_context.assert_selected_items(vec![3, 8]);
	test_context.delete_hovered_item(5);
	test_context.assert_selected_items(vec![3, 7]);
}

#[test]
fn delete_item_above_and_below_selected_items_in_multiple_pages() {
	let mut test_context = TestContext::enumerated(30, 10);

	test_context.set_hovered_item_index(4);
	test_context.assert_status(0, 4, 4);
	test_context.select_hovered_item();
	test_context.set_hovered_item_index(2);
	test_context.assert_status(0, 2, 2);
	test_context.select_hovered_item();

	test_context.set_hovered_item_index(14);
	test_context.assert_status(1, 14, 14);
	test_context.select_hovered_item();
	test_context.assert_selected_items(vec![4, 2, 14]);
	test_context.set_hovered_item_index(12);
	test_context.assert_status(1, 12, 12);
	test_context.select_hovered_item();
	test_context.assert_selected_items(vec![4, 2, 14, 12]);

	test_context.set_hovered_item_index(22);
	test_context.assert_status(2, 22, 22);
	test_context.select_hovered_item();
	test_context.assert_selected_items(vec![4, 2, 14, 12, 22]);

	test_context.set_hovered_item_index(5);
	test_context.delete_hovered_item(5);
	test_context.assert_selected_items(vec![4, 2, 13, 11, 21]);
}
