use nrustes::{
	backends::VoidContext,
	nrustes_config::NrustesConfig,
	prelude::{Container, Context, Draw, HandleInput, Key, Scrollable, WindowBuilder},
	scrollable::ScrollableBuilder,
};

mod delete;
mod scroll;
mod select;

pub struct TestContext {
	pub context: VoidContext,

	pub scrollable: Scrollable<String>,
}

impl TestContext {
	pub fn enumerated(item_count: usize, items_per_page: usize) -> Self {
		let (mut context, scrollable_builder) = TestContext::initialize(item_count, items_per_page);

		let mut scrollable: Scrollable<String> = scrollable_builder.enumerated().build();

		scrollable.update();
		scrollable.draw(&mut context);

		Self {
			context,
			scrollable,
		}
	}

	// pub fn with_no_prefix(item_count: usize, items_per_page: usize) -> Self {
	// 	let (mut context, scrollable_builder) = TestContext::initialize(item_count, items_per_page);

	// 	let mut scrollable: Scrollable<String> = scrollable_builder.with_no_prefix().build();

	// 	scrollable.update();
	// 	scrollable.draw(&mut context);

	// 	Self {
	// 		context,
	// 		scrollable,
	// 	}
	// }

	// pub fn with_prefix(item_count: usize, items_per_page: usize, prefix: char) -> Self {
	// 	let (mut context, scrollable_builder) = TestContext::initialize(item_count, items_per_page);

	// 	let mut scrollable: Scrollable<String> = scrollable_builder.with_prefix(prefix).build();

	// 	scrollable.update();
	// 	scrollable.draw(&mut context);

	// 	Self {
	// 		context,
	// 		scrollable,
	// 	}
	// }

	fn initialize(
		item_count: usize,
		items_per_page: usize,
	) -> (VoidContext, ScrollableBuilder<String>) {
		let nrustes_config = NrustesConfig::default();

		let context = VoidContext::new(160, 200, nrustes_config).mock_drawing_is_slow();

		let items = (0..item_count).map(|i| format!("Item {i}")).collect();
		let height = 3 + items_per_page as u16 * 2 + 2;

		let scrollable_builder = WindowBuilder::new(0, 0, height, 60, &context)
			.with_title("Tested Scrollable")
			.scrollable_builder()
			.with_items(items);

		(context, scrollable_builder)
	}

	/// Asserts that the `Scrollable`'s status matches provided arguments.
	/// - `page_index`: the index of the currently visible page.
	/// - `item_index`: the index of the currently hovered item.
	/// - `item_number`: the number which is displayed in the currently hovered item.
	///   Example: "Item 23", which does not necessarily have to be at index 23, has the number 23.
	fn assert_status(&self, page_index: usize, item_index: usize, item_number: usize) {
		let info = self.scrollable.info();
		println!("{info:?}");

		assert_eq!(
			info.current_page_index, page_index,
			"Expected current page to be {page_index}, but it's {} instead.",
			info.current_page_index
		);

		assert_eq!(
			info.current_item_index,
			Some(item_index),
			"Expected currently hovered item to be at index {item_index}, but it's at {:?} instead.",
			info.current_item_index
		);

		assert_eq!(
			self.scrollable.hovered_index(),
			Some(item_index),
			"Expected currently hovered item to be at index {item_index}, but it's at {:?} instead.",
			self.scrollable.hovered_index()
		);

		assert_eq!(
			self.scrollable.hovered(),
			Some(&format!("Item {item_number}")),
			"Expected currently hovered item to be \"Item {item_number}\", but it's {:?} instead.",
			self.scrollable.hovered()
		);
	}

	fn assert_has_items(&self, items_count: usize) {
		assert_eq!(
			self.scrollable.info().item_count,
			items_count,
			"Expected item count to be {items_count}, but got {} instead.",
			self.scrollable.info().item_count
		)
	}

	fn assert_has_no_items(&self) {
		let info = self.scrollable.info();

		assert_eq!(
			info.item_count, 0,
			"Expected no items to be present, but got {} instead.",
			info.item_count
		);

		assert!(
			info.current_item_index.is_none(),
			"Expected no currently hovered item, but got one at index {:?} instead.",
			info.current_item_index
		);

		assert_eq!(
			info.page_count, 1,
			"Expected only one page, but got {} instead.",
			info.page_count
		);

		assert_eq!(
			info.current_page_index, 0,
			"Expected to be at the first page (index 0), but got page {} instead.",
			info.current_page_index
		);

		assert!(
			self.scrollable.hovered_index().is_none(),
			"Expected no currently hovered item, but got one at index {:?} instead.",
			self.scrollable.hovered_index()
		);

		assert!(
			self.scrollable.hovered().is_none(),
			"Expected no currently hovered item, but got {:?} instead.",
			self.scrollable.hovered()
		);
	}

	fn assert_selected_items(&self, item_numbers: Vec<usize>) {
		let info = self.scrollable.info();

		assert_eq!(
			info.selected_item_count,
			item_numbers.len(),
			"Expected {} selected items, but got {} instead.",
			item_numbers.len(),
			info.selected_item_count
		);

		assert_eq!(
			self.scrollable.selected_indexes(),
			item_numbers,
			"Expected selected indexes to be {item_numbers:?}, but got {:?} instead.",
			self.scrollable.selected_indexes()
		);
	}

	fn mock_key(&mut self, key: Key) {
		self.context.queue_key(key);

		self.scrollable
			.handle_input_and_redraw(&mut self.context)
			.unwrap();
	}

	fn mock_key_multiple_times(&mut self, key: Key, times: usize) {
		for _ in 0..times {
			self.mock_key(key);
		}
	}

	fn set_hovered_item_index(&mut self, item_index: usize) {
		self.scrollable.set_hovered_index(item_index);
		self.scrollable.update();
	}

	/// Deletes the currently hovered item, asserts that it matches the expected
	/// number and returns it.
	fn delete_hovered_item(&mut self, item_number: usize) -> Option<String> {
		let deleted = self.scrollable.delete_hovered();

		assert_eq!(
			deleted,
			Some(format!("Item {item_number}")),
			"Expected deleted item to be \"Item {item_number}\", but got {deleted:?} instead."
		);

		deleted
	}

	fn select_hovered_item(&mut self) {
		self.scrollable.select_hovered();
	}
}
