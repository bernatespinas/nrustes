use nrustes::{
	backends::VoidContext,
	nrustes_config::NrustesConfig,
	prelude::{
		BindShortcut, Container, Context, HandleInput, Key, Scrollable, Scrollbar, Shortcut,
		Shortcuts, Window, WindowBuilder,
	},
};

mod scrollable;
mod scrollbar;
mod window;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum TestAction {
	Delete(usize),
	Scroll,
}

struct TestContext<C: Container, D: Clone> {
	context: VoidContext,

	shortcuts: Shortcuts<C, D>,
}

impl<D> TestContext<Window, D>
where
	D: Clone + Eq + PartialEq + std::fmt::Debug,
{
	fn new() -> Self {
		let nrustes_config = NrustesConfig::default();

		let context = VoidContext::new(160, 200, nrustes_config).mock_drawing_is_slow();

		let container: Window = WindowBuilder::full("test", &context).build();

		let shortcuts: Shortcuts<Window, D> = Shortcuts::from(container);

		Self { context, shortcuts }
	}
}

impl<D> TestContext<Scrollable<String>, D>
where
	D: Clone + Eq + PartialEq + std::fmt::Debug,
{
	fn new(item_count: usize, items_per_page: usize) -> Self {
		// Reuse the logic from `Scrollable`'s `TestContext`.
		let test_context = crate::scrollable::TestContext::enumerated(item_count, items_per_page);

		Self {
			context: test_context.context,
			shortcuts: Shortcuts::from(test_context.scrollable),
		}
	}
}

impl<D> TestContext<Scrollbar, D>
where
	D: Clone + Eq + PartialEq + std::fmt::Debug,
{
	fn new() -> Self {
		let nrustes_config = NrustesConfig::default();

		let context = VoidContext::new(160, 200, nrustes_config).mock_drawing_is_slow();

		let container: Scrollbar = WindowBuilder::full("test", &context).build();

		let shortcuts: Shortcuts<Scrollbar, D> = Shortcuts::from(container);

		Self { context, shortcuts }
	}
}

impl<C, D> TestContext<C, D>
where
	C: Container + HandleInput,
	D: Clone + Eq + PartialEq + std::fmt::Debug,
{
	fn mock_key(&mut self, key: Key, expected_shortcut_data: Option<D>) {
		let shortcut_data = self.shortcuts.shortcut_data(&key).cloned();

		self.context.queue_key(key);

		self.shortcuts.handle_input(&mut self.context).unwrap();

		assert_eq!(
			shortcut_data,
			expected_shortcut_data,
			"Expected shortcut data bound to key {key:?} to be {expected_shortcut_data:?}, but got {shortcut_data:?} instead."
		);
	}

	fn bind_shortcut(&mut self, key: Key, data: D) {
		self.shortcuts.bind_shortcut(key, Shortcut::new(data));
	}

	fn unbind_shortcut(&mut self, key: Key) {
		self.shortcuts.unbind_shortcut(&key);
	}
}

fn unbound_key_returns_no_shortcut_data(
	mut test_context: TestContext<impl Container + HandleInput, TestAction>,
) {
	test_context.mock_key(Key::Char('d'), None);
}

fn bind_and_unbind_a_key(mut test_context: TestContext<impl Container + HandleInput, TestAction>) {
	test_context.mock_key(Key::Char('d'), None);

	for i in 0..10 {
		test_context.bind_shortcut(Key::Char('d'), TestAction::Delete(i));
		test_context.mock_key(Key::Char('d'), Some(TestAction::Delete(i)));

		test_context.unbind_shortcut(Key::Char('d'));
		test_context.mock_key(Key::Char('d'), None);
	}
}

fn binding_same_key_keeps_last_shortcut_data(
	mut test_context: TestContext<impl Container + HandleInput, TestAction>,
) {
	test_context.mock_key(Key::Char('d'), None);

	for i in 0..10 {
		test_context.bind_shortcut(Key::Char('d'), TestAction::Delete(i));
		test_context.mock_key(Key::Char('d'), Some(TestAction::Delete(i)));
	}
}

fn unbiding_unbound_key_does_nothing(
	mut test_context: TestContext<impl Container + HandleInput, TestAction>,
) {
	test_context.mock_key(Key::Char('d'), None);
	test_context.unbind_shortcut(Key::Char('d'));
	test_context.mock_key(Key::Char('d'), None);
}
