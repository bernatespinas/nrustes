use nrustes::prelude::Window;

use super::TestAction;

type TestContext = super::TestContext<Window, TestAction>;

#[test]
fn unbound_key_returns_no_shortcut_data() {
	super::unbound_key_returns_no_shortcut_data(TestContext::new());
}

#[test]
fn bind_and_unbind_a_key() {
	super::bind_and_unbind_a_key(TestContext::new());
}

#[test]
fn binding_same_key_keeps_last_shortcut_data() {
	super::binding_same_key_keeps_last_shortcut_data(TestContext::new());
}

#[test]
fn unbiding_unbound_key_does_nothing() {
	super::unbiding_unbound_key_does_nothing(TestContext::new());
}
