use nrustes::prelude::Scrollbar;

use super::TestAction;

type TestContext = super::TestContext<Scrollbar, TestAction>;

// TODO Add tests about `Scrollable`'s movement keys.

#[test]
fn unbound_key_returns_no_shortcut_data() {
	super::unbound_key_returns_no_shortcut_data(TestContext::new());
}

#[test]
fn bind_and_unbind_a_key() {
	super::bind_and_unbind_a_key(TestContext::new());
}

#[test]
fn binding_same_key_keeps_last_shortcut_data() {
	super::binding_same_key_keeps_last_shortcut_data(TestContext::new());
}

#[test]
fn unbiding_unbound_key_does_nothing() {
	super::unbiding_unbound_key_does_nothing(TestContext::new());
}
