use nrustes::prelude::{direction_simple::Direction, Key, Scrollable};

use super::TestAction;

type TestContext = super::TestContext<Scrollable<String>, TestAction>;

#[test]
fn unbound_key_returns_no_shortcut_data() {
	super::unbound_key_returns_no_shortcut_data(TestContext::new(30, 10));
}

#[test]
fn bind_and_unbind_a_key() {
	super::bind_and_unbind_a_key(TestContext::new(30, 10));
}

#[test]
fn binding_same_key_keeps_last_shortcut_data() {
	super::binding_same_key_keeps_last_shortcut_data(TestContext::new(30, 10));
}

#[test]
fn unbiding_unbound_key_does_nothing() {
	super::unbiding_unbound_key_does_nothing(TestContext::new(30, 10));
}

#[test]
fn scroll_with_movement_shortcuts() {
	let mut test_context = TestContext::new(30, 10);

	test_context
		.shortcuts
		.bind_movement_shortcuts(TestAction::Scroll);

	assert_eq!(
		test_context.shortcuts.container().info().current_item_index,
		Some(0)
	);

	test_context.mock_key(Key::Movement(Direction::Down), Some(TestAction::Scroll));

	assert_eq!(
		test_context.shortcuts.container().info().current_item_index,
		Some(1)
	);
}
