# nrustes

Welcome!

nrustes is a wrapper around other libraries to facilitate the creation of "terminal" programs.

Choose between [crossterm](https://crates.io/crates/crossterm), [termion](https://crates.io/crates/termion) and [SDL2](https://crates.io/crates/sdl2), use the same API and switch between them effortlessly.

First, add `nrustes` to your `Cargo.toml`:

```toml
[dependencies.nrustes]
git = "https://github.com/bernatespinas/nrustes.git"
features = ["with_crossterm"]
```

These are the available features:

- "with_termion"
- "with_crossterm"
- "with_sdl2"
- By default, all of them.

Check the examples to see how everything works!

For more advanced examples check [dastan](https://www.gitlab.com/bernatespinas/dastan), the roguelike I'm also creating using nrustes. `ContextSDL2` is more complicated to create and use, for example.

## Screenshots

Some screenshots of [dastan](https://www.gitlab.com/bernatespinas/dastan) to see how nrustes looks like.

![](pics/dastan.png)

![](pics/inventory.png)

![](pics/popup.png)